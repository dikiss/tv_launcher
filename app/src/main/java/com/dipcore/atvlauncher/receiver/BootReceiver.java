package com.dipcore.atvlauncher.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.dipcore.atvlauncher.event.BootOnBootCompletedEvent;
import org.greenrobot.eventbus.EventBus;

public class BootReceiver extends BroadcastReceiver {
    private static final String TAG = BootReceiver.class.getCanonicalName();
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.i(TAG, "onReceive action: " + intent.getAction());
        EventBus.getDefault().post(new BootOnBootCompletedEvent());
    }
}
