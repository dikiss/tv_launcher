package com.dipcore.atvlauncher.receiver;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;
import com.dipcore.atvlauncher.event.LoaderLoadApplicationsAction;
import org.greenrobot.eventbus.EventBus;

public class PackageIntentReceiver extends BroadcastReceiver {
    private static final String TAG = PackageIntentReceiver.class.getCanonicalName();
    @Override
    public void onReceive(Context context, Intent intent) {
        Log.d(TAG, "onReceive action: " + intent.getAction());
        Log.d(TAG, "onReceive packageName: " + intent.getData().getSchemeSpecificPart());
        EventBus.getDefault().post(new LoaderLoadApplicationsAction());
    }
}
