package com.dipcore.atvlauncher.listener;

import android.content.Context;
import android.content.IntentFilter;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.LauncherApplication;
import com.dipcore.atvlauncher.LauncherConstants;
import com.dipcore.atvlauncher.event.ItemsAdapterAddItemAction;
import com.dipcore.atvlauncher.event.ItemsAdapterRemoveItemAction;
import com.dipcore.atvlauncher.event.ItemsAdapterSetItemsAction;
import com.dipcore.atvlauncher.event.LoaderLoadApplicationsAction;
import com.dipcore.atvlauncher.event.LoaderLoadInputsAction;
import com.dipcore.atvlauncher.event.LoaderLoadWidgetsAction;
import com.dipcore.atvlauncher.event.LoaderOnLoadFinishedEvent;
import com.dipcore.atvlauncher.event.StorageSaveItemsAction;
import com.dipcore.atvlauncher.helper.AppHelper;
import com.dipcore.atvlauncher.helper.ColorHelper;
import com.dipcore.atvlauncher.helper.ExportHelper;
import com.dipcore.atvlauncher.item.widget.WidgetLauncherItemModel;
import com.dipcore.atvlauncher.receiver.PackageIntentReceiver;
import com.dipcore.atvlauncher.widget.LauncherAppWidget.LauncherAppWidgetHost;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherSections;
import com.google.gson.reflect.TypeToken;
import io.paperdb.Paper;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.Iterator;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class LoaderEventListener extends EventListener {
    public static final String APPLICATIONS_FILE_NAME = "apps.json";
    private static final String INPUTS_FILE_NAME = "inputs.json";
    public static final Type ITEM_LIST_TYPE = new C03091().getType();
    private static final String TAG = LoaderEventListener.class.getCanonicalName();
    private static final String WIDGETS_FILE_NAME = "widgets.json";
    private final LauncherAppWidgetHost appWidgetHost;
    private final PackageIntentReceiver applicationIntentReceiver;
    private final Context context;
    private int count = 0;
    private final LauncherSections launcherSections;

    static class C03091 extends TypeToken<ArrayList<LauncherItemModel>> {
        C03091() {
        }
    }

    public LoaderEventListener(Context context) {
        this.context = context;
        this.appWidgetHost = LauncherApplication.getInstance().getAppWidgetHost();
        this.launcherSections = LauncherApplication.getInstance().getLauncherSections();
        this.applicationIntentReceiver = new PackageIntentReceiver();
    }

    public void register() {
        super.register();
        IntentFilter filter = new IntentFilter("android.intent.action.PACKAGE_ADDED");
        filter.addAction("android.intent.action.PACKAGE_REMOVED");
        filter.addAction("android.intent.action.PACKAGE_CHANGED");
        filter.addDataScheme("package");
        this.context.registerReceiver(this.applicationIntentReceiver, filter);
    }

    public void unregister() {
        this.context.unregisterReceiver(this.applicationIntentReceiver);
        super.unregister();
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onEvent(LoaderLoadApplicationsAction event) {
        Log.d(TAG, "LoaderLoadApplicationsAction order = 3");
        this.count++;
        ArrayList<LauncherItemModel> adapterItems = this.launcherSections.getSectionItems(LauncherConstants.AppsSectionUuid);
        ArrayList<LauncherItemModel> installedAppList = AppHelper.getInstalledApplications(this.context);
        ArrayList<LauncherItemModel> list = Paper.book().read(LauncherConstants.AppsSectionUuid);
        if (list == null) {
            Log.d(TAG, "First loading (No db data yet)");
            ArrayList<LauncherItemModel> newList = installedAppList;
            appListPostProcessing(newList);
            this.launcherSections.setSection(LauncherConstants.AppsSectionUuid, this.context.getString(R.string.section_applications), newList);
        } else if (adapterItems == null || event.forceReload) {
            Log.d(TAG, "Normal loading, db exists");
            AppHelper.removeNonExistingItems(list, installedAppList);
            AppHelper.addNewItems(list, installedAppList);
            AppHelper.removeDoubles(list);
            appListPostProcessing(list);
            this.launcherSections.setSection(LauncherConstants.AppsSectionUuid, this.context.getString(R.string.section_applications), list);
        } else {
            Log.d(TAG, "Items already loaded, update app list");
            final ArrayList<LauncherItemModel> toDelete = AppHelper.filterNonExistingItems(adapterItems, installedAppList);
            Log.d(TAG, "LoaderLoadApplicationsAction toDelete = " + toDelete);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Iterator it = toDelete.iterator();
                    while (it.hasNext()) {
                        LauncherItemModel item = (LauncherItemModel) it.next();
                        launcherSections.removeItem(LauncherConstants.AppsSectionUuid, item);
                        EventBus.getDefault().postSticky(new ItemsAdapterRemoveItemAction(LauncherConstants.HomeAdapterUuid, item.getUuid()));
                    }
                }
            });
            final ArrayList<LauncherItemModel> toAdd = AppHelper.filterNonExistingItems(installedAppList, adapterItems);
            Log.d(TAG, "LoaderLoadApplicationsAction toAdd = " + toAdd);
            new Handler(Looper.getMainLooper()).post(new Runnable() {
                @Override
                public void run() {
                    Iterator it = toAdd.iterator();
                    while (it.hasNext()) {
                        LauncherItemModel item = (LauncherItemModel) it.next();
                        if (item.getBackgroundColor() == null && item.getBackgroundImage() == null) {
                            item.setBackgroundColor(ColorHelper.getMatColorString(context));
                        }
                        launcherSections.addItem(LauncherConstants.AppsSectionUuid, item);
                        EventBus.getDefault().postSticky(new ItemsAdapterAddItemAction(LauncherConstants.HomeAdapterUuid, item));
                    }
                }
            });
        }
        int i = this.count - 1;
        this.count = i;
        if (i == 0) {
            EventBus.getDefault().post(new LoaderOnLoadFinishedEvent());
        }
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onEvent(LoaderLoadWidgetsAction event) {
        Log.d(TAG, "LoaderLoadWidgetsAction order = 1");
        this.count++;
        ArrayList<LauncherItemModel> items = this.launcherSections.getSectionItems(LauncherConstants.WidgetSectionUuid);
        if (items == null || event.forceReload) {
            try {
                items = (ArrayList) Paper.book().read(LauncherConstants.WidgetSectionUuid);
            } catch (Exception e) {
                e.printStackTrace();
            }
            if (items == null) {
                items = ExportHelper.getDefaultItems(this.context, WIDGETS_FILE_NAME, ITEM_LIST_TYPE);
            }
            Iterator it = items.iterator();
            while (it.hasNext()) {
                WidgetLauncherItemModel widgetItemModel = (WidgetLauncherItemModel) ((LauncherItemModel) it.next());
                widgetItemModel.setLauncherAppWidgetHost(this.appWidgetHost);
                widgetItemModel.setContext(this.context);
            }
            this.launcherSections.addSection(LauncherConstants.WidgetSectionUuid, items);
        }
        int i = this.count - 1;
        this.count = i;
        if (i == 0) {
            EventBus.getDefault().post(new LoaderOnLoadFinishedEvent());
        }
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onEvent(LoaderLoadInputsAction event) {
        Log.d(TAG, "LoaderLoadInputsAction order = 2");
        this.count++;
        if (this.launcherSections.getSectionItems(LauncherConstants.InputsSectionUuid) == null || event.forceReload) {
            this.launcherSections.addSection(LauncherConstants.InputsSectionUuid, this.context.getString(R.string.section_inputs), (ArrayList) Paper.book().read(LauncherConstants.InputsSectionUuid));
        }
        int i = this.count - 1;
        this.count = i;
        if (i == 0) {
            EventBus.getDefault().post(new LoaderOnLoadFinishedEvent());
        }
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onEvent(LoaderOnLoadFinishedEvent event) {
        EventBus.getDefault().post(new ItemsAdapterSetItemsAction(LauncherConstants.HomeAdapterUuid, this.launcherSections.flatten(true)));
        EventBus.getDefault().post(new StorageSaveItemsAction());
        Log.i(TAG, "LoaderOnLoadFinishedEvent");
    }

    private void appListPostProcessing(ArrayList<LauncherItemModel> list) {
        Iterator it = AppHelper.flatten(list, true).iterator();
        while (it.hasNext()) {
            LauncherItemModel item = (LauncherItemModel) it.next();
            if (item.getBackgroundColor() == null && item.getBackgroundImage() == null) {
                item.setBackgroundColor(ColorHelper.getMatColorString(this.context));
            }
        }
    }
}
