package com.dipcore.atvlauncher.listener;

import android.appwidget.AppWidgetHost;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;
import android.support.v7.widget.GridLayoutManager;
import android.util.Log;
import com.dipcore.atvlauncher.LauncherActivity;
import com.dipcore.atvlauncher.LauncherApplication;
import com.dipcore.atvlauncher.LauncherConstants;
import com.dipcore.atvlauncher.event.StorageSaveItemsAction;
import com.dipcore.atvlauncher.event.WidgetConfigureEvent;
import com.dipcore.atvlauncher.event.WidgetPickEvent;
import com.dipcore.atvlauncher.event.WidgetRemoveEvent;
import com.dipcore.atvlauncher.event.WidgetSetEvent;
import com.dipcore.atvlauncher.event.WidgetSetScaleEvent;
import com.dipcore.atvlauncher.event.WidgetSetTapCoordinatesEvent;
import com.dipcore.atvlauncher.event.WidgetTapEvent;
import com.dipcore.atvlauncher.helper.WidgetHelper;
import com.dipcore.atvlauncher.helper.WidgetHelper.OnConfigureWidgetListener;
import com.dipcore.atvlauncher.helper.WidgetHelper.OnPickWidgetListener;
import com.dipcore.atvlauncher.item.widget.WidgetLauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherAppWidget.LauncherAppWidgetHostView;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherRendererRecyclerViewAdapter;
import com.dipcore.atvlauncher.widget.WidgetView;
import org.greenrobot.eventbus.EventBus;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class WidgetEventListener extends EventListener {
    private static final String TAG = WidgetEventListener.class.getCanonicalName();
    private final AppWidgetHost appWidgetHost = LauncherApplication.getInstance().getAppWidgetHost();
    private final Context context;
    private final GridLayoutManager gridLayoutManager;
    private final LauncherRendererRecyclerViewAdapter recyclerViewAdapter;

    public WidgetEventListener(LauncherActivity activity) {
        this.context = activity;
        this.recyclerViewAdapter = activity.getRecyclerViewAdapter();
        this.gridLayoutManager = activity.getGridLayoutManager();
    }

    @Subscribe
    public void onEvent(WidgetTapEvent event) {
        Log.d(TAG, "WidgetTapEvent position = " + event.position);
        WidgetLauncherItemModel widgetItemModel = (WidgetLauncherItemModel) this.recyclerViewAdapter.getItem(event.position);
        WidgetView widgetView = (WidgetView) this.gridLayoutManager.findViewByPosition(event.position);
        int x = (widgetView.getWidth() * widgetItemModel.getTapX()) / 100;
        int y = (widgetView.getHeight() * widgetItemModel.getTapY()) / 100;
        Log.d(TAG, "WidgetTapEvent x = " + x + " y = " + y);
        WidgetHelper.performClick(x, y, widgetView.getLauncherAppWidgetHostView());
    }

    @Subscribe
    public void onEvent(final WidgetPickEvent event) {
        Log.d(TAG, "WidgetPickEvent position = " + event.position);
        WidgetHelper.pickWidget(this.context, this.appWidgetHost, new OnPickWidgetListener() {
            public void onPickWidget(final int appWidgetId) {
                Log.d(WidgetEventListener.TAG, "WidgetPickEvent appWidgetId = " + appWidgetId);
                if (WidgetHelper.getAppWidgetInfo(WidgetEventListener.this.context, appWidgetId).configure != null) {
                    new Handler(Looper.getMainLooper()).post(new Runnable() {
                        public void run() {
                            WidgetEventListener.this.configureWidget(event.position, appWidgetId, true);
                        }
                    });
                } else {
                    EventBus.getDefault().postSticky(new WidgetSetEvent(event.position, appWidgetId));
                }
            }
        });
    }

    @Subscribe
    public void onEvent(WidgetConfigureEvent event) {
        Log.d(TAG, "WidgetConfigureEvent position = " + event.position);
        WidgetLauncherItemModel widgetItemModel = (WidgetLauncherItemModel) this.recyclerViewAdapter.getItem(event.position);
        if (event.appWidgetId == 0) {
            event.appWidgetId = widgetItemModel.getAppWidgetId();
            configureWidget(event.position, event.appWidgetId, false);
            return;
        }
        configureWidget(event.position, event.appWidgetId, true);
    }

    @Subscribe
    public void onEvent(WidgetSetEvent event) {
        Log.d(TAG, "WidgetSetEvent position = " + event.position + " appWidgetId = " + event.appWidgetId);
        WidgetLauncherItemModel widgetItemModel = (WidgetLauncherItemModel) this.recyclerViewAdapter.getItem(event.position);
        WidgetView widgetView = (WidgetView) this.gridLayoutManager.findViewByPosition(event.position);
        if (widgetItemModel.getAppWidgetId() != 0 && event.reset) {
            widgetItemModel.reset();
            widgetView.removeAppWidgetHostView();
            widgetView.resetBackground();
        }
        LauncherAppWidgetHostView appWidgetHostView = WidgetHelper.createAppWidgetHostView(this.context, widgetItemModel.getLauncherAppWidgetHost(), event.appWidgetId);
        widgetItemModel.setAppWidgetId(event.appWidgetId);
        widgetItemModel.setLauncherAppWidgetHostView(appWidgetHostView);
        widgetView.setAppWidgetHostView(appWidgetHostView);
        this.recyclerViewAdapter.notifyItemChanged(event.position);
        EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.WidgetSectionUuid));
    }

    @Subscribe
    public void onEvent(WidgetRemoveEvent event) {
        Log.d(TAG, "WidgetRemoveEvent position = " + event.position);
        WidgetLauncherItemModel widgetItemModel = (WidgetLauncherItemModel) this.recyclerViewAdapter.getItem(event.position);
        WidgetView widgetView = (WidgetView) this.gridLayoutManager.findViewByPosition(event.position);
        widgetItemModel.getLauncherAppWidgetHost().deleteAppWidgetId(widgetItemModel.getAppWidgetId());
        widgetItemModel.reset();
        widgetView.removeAppWidgetHostView();
        widgetView.resetBackground();
        EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.WidgetSectionUuid));
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onEvent(WidgetSetTapCoordinatesEvent event) {
        Log.d(TAG, "WidgetSetTapCoordinatesEvent position = " + event.position + " x = " + event.f2x + " y = " + event.f3y);
        WidgetLauncherItemModel widgetItemModel = (WidgetLauncherItemModel) this.recyclerViewAdapter.getItem(event.position);
        widgetItemModel.setTapX(event.f2x);
        widgetItemModel.setTapY(event.f3y);
        EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.WidgetSectionUuid));
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(WidgetSetScaleEvent event) {
        Log.d(TAG, "WidgetSetScaleEvent position = " + event.position + " scale = " + event.scale);
        WidgetLauncherItemModel widgetItemModel = (WidgetLauncherItemModel) this.recyclerViewAdapter.getItem(event.position);
        WidgetView widgetView = (WidgetView) this.gridLayoutManager.findViewByPosition(event.position);
        LauncherAppWidgetHostView appWidgetHostView = WidgetHelper.createAppWidgetHostView(this.context, widgetItemModel.getLauncherAppWidgetHost(), widgetItemModel.getAppWidgetId());
        widgetItemModel.setLauncherAppWidgetHostView(appWidgetHostView);
        widgetItemModel.setScaleFactor(event.scale);
        widgetView.setAppWidgetHostView(appWidgetHostView);
        widgetView.setAppWidgetScaleFactor(event.scale);
        this.recyclerViewAdapter.notifyItemChanged(event.position);
        EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.WidgetSectionUuid));
    }

    private void configureWidget(final int position, int appWidgetId, final boolean reset) {
        Log.d(TAG, "WidgetConfigureEvent appWidgetId = " + appWidgetId);
        WidgetHelper.configureWidget(this.context, appWidgetId, new OnConfigureWidgetListener() {
            @Override
            public void onConfigureWidget(final int appWidgetId) {
                new Handler(Looper.getMainLooper()).post(new Runnable() {
                    public void run() {
                        EventBus.getDefault().postSticky(new WidgetSetEvent(position, appWidgetId, reset));
                    }
                });
            }
        });
    }
}
