package com.dipcore.atvlauncher.listener;

import android.util.Log;
import com.dipcore.atvlauncher.LauncherActivity;
import com.dipcore.atvlauncher.LauncherApplication;
import com.dipcore.atvlauncher.LauncherConstants;
import com.dipcore.atvlauncher.event.StorageSaveItemsAction;
import com.dipcore.atvlauncher.event.StorageSaveItemsSyncAction;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherSections;
import io.paperdb.Paper;
import java.util.ArrayList;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class StorageEventListener extends EventListener {
    private static final String TAG = WidgetEventListener.class.getCanonicalName();
    private final LauncherSections launcherSections = LauncherApplication.getInstance().getLauncherSections();

    public StorageEventListener(LauncherActivity activity) {
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onEvent(StorageSaveItemsAction event) {
        Log.d(TAG, "StorageSaveItemsAction type = " + event.sectionUuid);
        processSaveItemsEvent(event.sectionUuid);
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(StorageSaveItemsSyncAction event) {
        Log.d(TAG, "StorageSaveItemsSyncAction type = " + event.sectionUuid);
        processSaveItemsEvent(event.sectionUuid);
    }

    private void processSaveItemsEvent(String sectionUuid) {
        int obj = -1;
        switch (sectionUuid.hashCode()) {
            case 1006515758:
                if (sectionUuid.equals(LauncherConstants.AppsSectionUuid)) {
                    obj = 2;
                    break;
                }
                break;
            case 1483280004:
                if (sectionUuid.equals(LauncherConstants.InputsSectionUuid)) {
                    obj = 1;
                    break;
                }
                break;
            case 2089104405:
                if (sectionUuid.equals(LauncherConstants.WidgetSectionUuid)) {
                    obj = 0;
                    break;
                }
                break;
        }
        switch (obj) {
            case 0:
                storeWidgets();
                return;
            case 1:
                storeInputs();
                return;
            case 2:
                storeApps();
                return;
            default:
                storeWidgets();
                storeInputs();
                storeApps();
                return;
        }
    }

    private void storeWidgets() {
        ArrayList<LauncherItemModel> items = this.launcherSections.getSectionItems(LauncherConstants.WidgetSectionUuid);
        if (items != null) {
            Paper.book().write(LauncherConstants.WidgetSectionUuid, items);
        }
    }

    private void storeInputs() {
        ArrayList<LauncherItemModel> items = this.launcherSections.getSectionItems(LauncherConstants.InputsSectionUuid);
        if (items != null) {
            Paper.book().write(LauncherConstants.InputsSectionUuid, items);
        }
    }

    private void storeApps() {
        ArrayList<LauncherItemModel> items = this.launcherSections.getSectionItems(LauncherConstants.AppsSectionUuid);
        if (items != null) {
            Paper.book().write(LauncherConstants.AppsSectionUuid, items);
        }
    }
}
