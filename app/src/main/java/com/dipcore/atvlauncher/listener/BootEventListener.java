package com.dipcore.atvlauncher.listener;

import android.app.Activity;
import android.content.Context;
import android.content.IntentFilter;
import android.util.Log;
import android.view.View;
import android.view.animation.AccelerateInterpolator;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.LauncherActivity;
import com.dipcore.atvlauncher.event.BootOnBootCompletedEvent;
import com.dipcore.atvlauncher.helper.ShellHelper;
import com.dipcore.atvlauncher.helper.ShellHelper.OnBootResultReceived;
import com.dipcore.atvlauncher.receiver.BootReceiver;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherRendererRecyclerViewAdapter;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class BootEventListener extends EventListener {
    private static final String TAG = BootEventListener.class.getCanonicalName();
    private boolean bootCompleted = false;
    private final BootReceiver bootReceiver;
    private final Context context;
    private final LauncherRendererRecyclerViewAdapter recyclerViewAdapter;

    public BootEventListener(LauncherActivity activity) {
        this.context = activity;
        this.recyclerViewAdapter = activity.getRecyclerViewAdapter();
        this.bootReceiver = new BootReceiver();
        initShellBootDetector();
    }

    public void register() {
        super.register();
        this.context.registerReceiver(this.bootReceiver, new IntentFilter("android.intent.action.BOOT_COMPLETED"));
    }

    public void unregister() {
        try {
            this.context.unregisterReceiver(this.bootReceiver);
        } catch (Exception e) {
            Log.w(TAG, "problem while unregister bootReceiver", e);
        }
        super.unregister();
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(BootOnBootCompletedEvent event) {
        Log.i(TAG, "BootOnBootCompletedEvent");
        hideSplashScreen();
    }

    private void initShellBootDetector() {
        if (!this.bootCompleted) {
            ShellHelper.bootComplete(500, 300, new OnBootResultReceived() {
                @Override
                public void onBootComplete() {
                    BootEventListener.this.hideSplashScreen();
                }
                @Override
                public void onError() {
                }
            });
        }
    }

    private void hideSplashScreen() {
        final View v = ((Activity) this.context).findViewById(R.id.splash_screen);
        if (!this.bootCompleted) {
            this.bootCompleted = true;
            v.postDelayed(new Runnable() {
                @Override
                public void run() {
                    BootEventListener.this.recyclerViewAdapter.notifyDataSetChanged();
                    Animation animation = new AlphaAnimation(1.0f, 0.0f);
                    animation.setDuration(500);
                    animation.setInterpolator(new AccelerateInterpolator());
                    animation.setAnimationListener(new AnimationListener() {
                        @Override
                        public void onAnimationStart(Animation animation) {
                        }
                        @Override
                        public void onAnimationEnd(Animation animation) {
                            v.setVisibility(View.GONE);
                        }
                        @Override
                        public void onAnimationRepeat(Animation animation) {
                        }
                    });
                    v.startAnimation(animation);
                }
            }, 1500);
        }
    }
}
