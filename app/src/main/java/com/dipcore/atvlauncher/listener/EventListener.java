package com.dipcore.atvlauncher.listener;

import org.greenrobot.eventbus.EventBus;

public class EventListener {
    public void register() {
        EventBus.getDefault().register(this);
    }

    public void unregister() {
        EventBus.getDefault().unregister(this);
    }
}
