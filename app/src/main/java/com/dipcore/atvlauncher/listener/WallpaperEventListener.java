package com.dipcore.atvlauncher.listener;

import android.app.WallpaperManager;
import android.content.Context;
import android.util.Log;
import com.dipcore.atvlauncher.event.WallpaperSetAction;
import com.dipcore.imagepicker.util.BitmapHelper;
import java.io.IOException;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class WallpaperEventListener extends EventListener {
    private static final String TAG = WallpaperEventListener.class.getCanonicalName();
    private Context context;

    public WallpaperEventListener(Context context) {
        this.context = context;
    }

    @Subscribe(threadMode = ThreadMode.ASYNC)
    public void onEvent(WallpaperSetAction event) {
        try {
            WallpaperManager.getInstance(this.context).setBitmap(BitmapHelper.resize(event.path == null ? BitmapHelper.getPath(this.context, event.uri) : event.path, BitmapHelper.getScreenWidth(), BitmapHelper.getScreenHeight()));
        } catch (IOException e) {
            Log.e(TAG, "Cannot set image as wallpaper", e);
        }
    }
}
