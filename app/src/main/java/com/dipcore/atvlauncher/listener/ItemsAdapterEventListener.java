package com.dipcore.atvlauncher.listener;

import android.content.Context;
import android.util.Log;
import com.dipcore.atvlauncher.event.ItemsAdapterAddItemAction;
import com.dipcore.atvlauncher.event.ItemsAdapterAddItemsAction;
import com.dipcore.atvlauncher.event.ItemsAdapterMoveItemAction;
import com.dipcore.atvlauncher.event.ItemsAdapterRefreshItemAction;
import com.dipcore.atvlauncher.event.ItemsAdapterRemoveItemAction;
import com.dipcore.atvlauncher.event.ItemsAdapterSetItemsAction;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherRendererRecyclerViewAdapter;
import org.greenrobot.eventbus.Subscribe;
import org.greenrobot.eventbus.ThreadMode;

public class ItemsAdapterEventListener extends EventListener {
    private static final String TAG = ItemsAdapterEventListener.class.getCanonicalName();
    private Context context;
    private LauncherRendererRecyclerViewAdapter recyclerViewAdapter;

    public ItemsAdapterEventListener(Context context) {
        this.context = context;
    }

    public LauncherRendererRecyclerViewAdapter getRecyclerViewAdapter() {
        return this.recyclerViewAdapter;
    }

    public void setRecyclerViewAdapter(LauncherRendererRecyclerViewAdapter recyclerViewAdapter) {
        this.recyclerViewAdapter = recyclerViewAdapter;
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ItemsAdapterSetItemsAction event) {
        Log.d(TAG, "ItemsAdapterSetItemsAction");
        if (this.recyclerViewAdapter != null && this.recyclerViewAdapter.getUuid().equals(event.adapterUuid)) {
            this.recyclerViewAdapter.setItems(event.items);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ItemsAdapterAddItemsAction event) {
        Log.d(TAG, "ItemsAdapterAddItemsAction items = " + event.items);
        if (this.recyclerViewAdapter != null && this.recyclerViewAdapter.getUuid().equals(event.adapterUuid)) {
            this.recyclerViewAdapter.addItems(event.items);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ItemsAdapterRemoveItemAction event) {
        Log.d(TAG, "ItemsAdapterRemoveItemAction item = " + event.uuid);
        if (this.recyclerViewAdapter != null && this.recyclerViewAdapter.getUuid().equals(event.adapterUuid)) {
            this.recyclerViewAdapter.removeItem(event.uuid);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ItemsAdapterRefreshItemAction event) {
        Log.d(TAG, "ItemsAdapterRefreshItemAction item = " + event.uuid);
        if (this.recyclerViewAdapter != null && this.recyclerViewAdapter.getUuid().equals(event.adapterUuid)) {
            this.recyclerViewAdapter.refreshItem(event.uuid);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ItemsAdapterMoveItemAction event) {
        Log.d(TAG, "ItemsAdapterMoveItemAction fromUuid = " + event.fromUuid + " toUuid = " + event.toUuid);
        if (this.recyclerViewAdapter != null && this.recyclerViewAdapter.getUuid().equals(event.adapterUuid)) {
            this.recyclerViewAdapter.moveItem(event.fromUuid, event.toUuid);
        }
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    public void onEvent(ItemsAdapterAddItemAction event) {
        Log.d(TAG, "ItemsAdapterAddItemAction item.uuid = " + event.item.getUuid());
        if (this.recyclerViewAdapter != null && this.recyclerViewAdapter.getUuid().equals(event.adapterUuid)) {
            if (event.position > -1) {
                this.recyclerViewAdapter.addItem(event.position, event.item);
            } else {
                this.recyclerViewAdapter.addItem(event.item);
            }
        }
    }
}
