package com.dipcore.atvlauncher.cache;

import android.graphics.Bitmap;
import android.util.Log;

public class ImageCache {
    private static final String TAG = ImageCache.class.getCanonicalName();
    private static ImageCache cache;
    private LruCache imagesWarehouse;

    public static ImageCache getInstance() {
        if (cache == null) {
            cache = new ImageCache();
        }
        return cache;
    }

    public void initializeCache() {
        int cacheSize = ((int) (Runtime.getRuntime().maxMemory() / 1024)) / 8;
        Log.d(TAG, "Initialize cache, size = " + cacheSize);
        this.imagesWarehouse = new LruCache(cacheSize) {
            protected int sizeOf(String key, Bitmap value) {
                return (value.getRowBytes() * value.getHeight()) / 1024;
            }
        };
    }

    public void put(String key, Bitmap value) {
        if (this.imagesWarehouse != null && this.imagesWarehouse.get(key) == null) {
            this.imagesWarehouse.put(key, value);
        }
    }

    public Bitmap get(String key) {
        if (key != null) {
            return (Bitmap) this.imagesWarehouse.get(key);
        }
        return null;
    }

    public void remove(String key) {
        this.imagesWarehouse.remove(key);
    }

    public void clear() {
        if (this.imagesWarehouse != null) {
            this.imagesWarehouse.evictAll();
        }
    }
}
