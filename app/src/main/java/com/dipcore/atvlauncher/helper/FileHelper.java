package com.dipcore.atvlauncher.helper;

import android.content.Context;
import android.database.Cursor;
import android.net.Uri;
import android.support.annotation.Nullable;
import android.util.Log;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;

public class FileHelper {
    public static void mkdir(String path) {
        File f = new File(path);
        if (!f.isDirectory()) {
            f.mkdirs();
        }
    }

    public static void stringToFile(String path, String content) {
        try {
            File file = new File(path);
            FileOutputStream fop = new FileOutputStream(file);
            if (!file.exists()) {
                file.mkdirs();
            }
            fop.write(content.getBytes());
            fop.flush();
            fop.close();
        } catch (IOException e) {
            Log.d("Exception", "File write failed: " + e.toString());
        }
    }

    @Nullable
    public static String fileToString(String path) {
        try {
            return inputStreamToString(new FileInputStream(new File(path)));
        } catch (Exception e) {
            Log.d("Exception", "File read failed: " + e.toString());
            return null;
        }
    }

    @Nullable
    public static String inputStreamToString(InputStream inputStream) {
        try {
            byte[] bytes = new byte[inputStream.available()];
            inputStream.read(bytes, 0, bytes.length);
            String json = new String(bytes);
            inputStream.close();
            return json;
        } catch (IOException e) {
            Log.d("Exception", "File read failed: " + e.toString());
            return null;
        }
    }

    public static String getFileName(Context context, Uri uri) {
        String result = null;
        if (uri.getScheme().equals("content")) {
            Cursor cursor = context.getContentResolver().query(uri, null, null, null, null);
            if (cursor != null) {
                try {
                    if (cursor.moveToFirst()) {
                        result = cursor.getString(cursor.getColumnIndex("_display_name"));
                    }
                } catch (Throwable th) {
                    cursor.close();
                }
            }
            cursor.close();
        }
        if (result != null) {
            return result;
        }
        result = uri.getPath();
        int cut = result.lastIndexOf(47);
        if (cut != -1) {
            return result.substring(cut + 1);
        }
        return result;
    }

    public static String getFileExtension(File file) {
        return getFileExtension(file.getName());
    }

    public static String getFileExtension(String name) {
        try {
            return name.substring(name.lastIndexOf(".") + 1);
        } catch (Exception e) {
            return "";
        }
    }
}
