package com.dipcore.atvlauncher.helper;

import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;

public class ViewHelper {
    private static String TAG = ViewHelper.class.getCanonicalName();

    public static ArrayList<View> findViews(int x, int y, View parent) {
        ArrayList<View> result = new ArrayList<>();
        Iterator it = getAllChildren(parent).iterator();
        while (it.hasNext()) {
            View view = (View) it.next();
            Log.d(TAG, "Found view: " + view.getId());
            Log.d(TAG, "Found view: " + view.getClass().getCanonicalName());
            if (isPointInsideView((float) x, (float) y, view)) {
                result.add(view);
            }
        }
        return result;
    }

    public static ArrayList<View> getAllChildren(View view) {
        ArrayList<View> result = new ArrayList<>();
        result.add(view);
        if (view instanceof ViewGroup) {
            ViewGroup viewGroup = (ViewGroup) view;
            int childCount = viewGroup.getChildCount();
            for (int i = 0; i < childCount; i++) {
                result.addAll(getAllChildren(viewGroup.getChildAt(i)));
            }
        }
        return result;
    }

    public static boolean isPointInsideView(float x, float y, View view) {
        int[] location = new int[2];
        view.getLocationOnScreen(location);
        int viewX = location[0];
        int viewY = location[1];
        if (x <= ((float) viewX) || x >= ((float) (view.getWidth() + viewX)) || y <= ((float) viewY) || y >= ((float) (view.getHeight() + viewY))) {
            return false;
        }
        return true;
    }

    public static void focusView(View view) {
        view.setFocusableInTouchMode(true);
        view.requestFocus();
        view.setFocusableInTouchMode(false);
    }
}
