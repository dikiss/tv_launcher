package com.dipcore.atvlauncher.helper;

import android.content.Context;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.provider.MediaStore.Images.Media;
import java.io.File;
import java.io.FileOutputStream;
import java.util.UUID;

public class CacheHelper {
    public static String storeFile(Context context, Uri uri) {
        Exception e;
        File imageFile = new File(context.getCacheDir(), UUID.randomUUID().toString() + "." + FileHelper.getFileExtension(FileHelper.getFileName(context, uri)));
        try {
            FileOutputStream fos = new FileOutputStream(imageFile);
            FileOutputStream fileOutputStream;
            try {
                Media.getBitmap(context.getContentResolver(), uri).compress(CompressFormat.PNG, 100, fos);
                fos.close();
                fileOutputStream = fos;
            } catch (Exception e2) {
                e = e2;
                fileOutputStream = fos;
                e.printStackTrace();
                return imageFile.getAbsolutePath();
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return imageFile.getAbsolutePath();
        }
        return imageFile.getAbsolutePath();
    }
}
