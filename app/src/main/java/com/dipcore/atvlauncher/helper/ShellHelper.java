package com.dipcore.atvlauncher.helper;

import android.os.Handler;
import android.util.Log;
import java.io.BufferedReader;
import java.io.InputStreamReader;

public class ShellHelper {
    private static String TAG = ShellHelper.class.getCanonicalName();

    public interface OnBootResultReceived {
        void onBootComplete();

        void onError();
    }

    public static String run(String command) {
        StringBuffer output = new StringBuffer();
        try {
            Process process = Runtime.getRuntime().exec(command);
            process.waitFor();
            BufferedReader reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
            char[] buffer = new char[4096];
            while (true) {
                int read = reader.read(buffer);
                if (read <= 0) {
                    break;
                }
                output.append(buffer, 0, read);
            }
            reader.close();
            process.waitFor();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return output.toString();
    }

    public static void bootComplete(final int delay, final int maxRepeatCount, final OnBootResultReceived onBootResultReceived) {
        final Handler handler = new Handler();
        handler.postDelayed(new Runnable() {
            int count = maxRepeatCount;

            public void run() {
                String result = ShellHelper.run("getprop sys.boot_completed");
                Log.d(ShellHelper.TAG, "getprop sys.boot_completed = " + result);
                if (result.replace("\n", "").equals("1")) {
                    onBootResultReceived.onBootComplete();
                    return;
                }
                int i = this.count - 1;
                this.count = i;
                if (i > 0) {
                    handler.postDelayed(this, (long) delay);
                } else {
                    onBootResultReceived.onError();
                }
                Log.d(ShellHelper.TAG, "count = " + this.count);
            }
        }, (long) delay);
    }
}
