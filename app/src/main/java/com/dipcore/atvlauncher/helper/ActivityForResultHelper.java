package com.dipcore.atvlauncher.helper;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Fragment;
import android.app.FragmentManager;
import android.content.Context;
import android.content.Intent;
import java.util.UUID;

public class ActivityForResultHelper {
    private static String TAG = ActivityForResultHelper.class.getName();
    private static Context context;

    @SuppressLint({"ValidFragment"})
    public static class FragmentForResult extends Fragment {
        OnActivityResultListener onActivityResultListener;

        public FragmentForResult() {
            this(null);
        }

        public FragmentForResult(OnActivityResultListener onActivityResultListener) {
            this.onActivityResultListener = onActivityResultListener;
        }
        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent data) {
            if (this.onActivityResultListener != null) {
                this.onActivityResultListener.onActivityResult(requestCode, resultCode, data);
            }
            super.onActivityResult(requestCode, resultCode, data);
            getActivity().getFragmentManager().beginTransaction().remove(this).commit();
        }
    }

    public interface OnActivityResultListener {
        void onActivityResult(int i, int i2, Intent intent);
    }

    public static void start(Context context, Intent in, int requestCode, OnActivityResultListener onActivityResultListener) {
        context = context;
        Fragment aux = new FragmentForResult(onActivityResultListener);
        FragmentManager fm = ((Activity) context).getFragmentManager();
        fm.beginTransaction().add(aux, UUID.randomUUID().toString()).commit();
        fm.executePendingTransactions();
        try {
            aux.startActivityForResult(in, requestCode);
        } catch (Exception e) {
            if (onActivityResultListener != null) {
                onActivityResultListener.onActivityResult(requestCode, -1, null);
            }
        }
    }
}
