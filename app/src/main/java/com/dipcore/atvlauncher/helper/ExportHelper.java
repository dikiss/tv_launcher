package com.dipcore.atvlauncher.helper;

import android.content.Context;
import android.graphics.Bitmap.CompressFormat;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore.Images.Media;
import com.dipcore.atvlauncher.settings.SettingsItemModelDeserializer;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherItemModel;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Type;
import java.util.ArrayList;
import java.util.UUID;

public class ExportHelper {
    private static String PATH = (Environment.getExternalStorageDirectory().toString() + "/ATV Launcher");
    private static String IMAGE_PATH = (PATH + "/images");
    private final Context context;

    public ExportHelper(Context context) {
        this.context = context;
        FileHelper.mkdir(PATH);
        FileHelper.mkdir(IMAGE_PATH);
    }

    public <T> T getObject(String filename, Type type) {
        T object = null;
        try {
            object = new Gson().fromJson(FileHelper.fileToString(PATH + File.separator + filename), type);
        } catch (Exception e) {
        }
        if (object == null) {
            return getDefaultObject(filename, type);
        }
        return object;
    }

    public <T> T getDefaultObject(String filename, Type type) {
        try {
            return new Gson().fromJson(FileHelper.inputStreamToString(this.context.getResources().openRawResource(this.context.getResources().getIdentifier(filename.replaceFirst("[.][^.]+$", ""), "raw", this.context.getPackageName()))), type);
        } catch (Exception e) {
            return null;
        }
    }

    public static void putObject(String filename, Object object) {
        String path = PATH + File.separator + filename;
        GsonBuilder gsonBuilder = new GsonBuilder();
        gsonBuilder.excludeFieldsWithModifiers(128);
        gsonBuilder.setPrettyPrinting();
        FileHelper.stringToFile(path, gsonBuilder.create().toJson(object));
    }

    public static ArrayList<LauncherItemModel> getItems(Context context, String filename, Type type) {
        ArrayList<LauncherItemModel> entries = new ArrayList<>();
        try {
            String json = FileHelper.fileToString(PATH + File.separator + filename);
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(LauncherItemModel.class, new SettingsItemModelDeserializer(context, type));
            entries = gsonBuilder.create().fromJson(json, type);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (entries == null) {
            return getDefaultItems(context, filename, type);
        }
        return entries;
    }

    public static ArrayList<LauncherItemModel> getDefaultItems(Context context, String filename, Type type) {
        try {
            String json = FileHelper.inputStreamToString(context.getResources().openRawResource(context.getResources().getIdentifier(filename.replaceFirst("[.][^.]+$", ""), "raw", context.getPackageName())));
            GsonBuilder gsonBuilder = new GsonBuilder();
            gsonBuilder.registerTypeAdapter(LauncherItemModel.class, new SettingsItemModelDeserializer(context, type));
            return (ArrayList<LauncherItemModel>) gsonBuilder.create().fromJson(json, type);
        } catch (Exception e) {
            return null;
        }
    }

    public static String storeImage(Context context, Uri uri) {
        FileOutputStream fileOutputStream;
        Exception e;
        File imageFile = new File(IMAGE_PATH, UUID.randomUUID().toString() + "." + FileHelper.getFileExtension(FileHelper.getFileName(context, uri)));
        try {
            FileOutputStream fos = new FileOutputStream(imageFile);
            try {
                Media.getBitmap(context.getContentResolver(), uri).compress(CompressFormat.PNG, 100, fos);
                fos.close();
                fileOutputStream = fos;
            } catch (Exception e2) {
                e = e2;
                fileOutputStream = fos;
                e.printStackTrace();
                return imageFile.getAbsolutePath();
            }
        } catch (Exception e3) {
            e = e3;
            e.printStackTrace();
            return imageFile.getAbsolutePath();
        }
        return imageFile.getAbsolutePath();
    }
}
