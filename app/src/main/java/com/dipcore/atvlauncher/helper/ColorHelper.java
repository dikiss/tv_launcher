package com.dipcore.atvlauncher.helper;

import android.content.Context;
import android.content.res.TypedArray;

public class ColorHelper {
    public static String toString(int color) {
        return String.format("#%08X", color & -1);
    }

    public static String getMatColorString(Context context) {
        return "#" + Integer.toHexString(getMatColor(context));
    }

    public static int getMatColor(Context context) {
        int arrayId = context.getResources().getIdentifier("bg_colors", "array", context.getPackageName());
        if (arrayId == 0) {
            return -16777216;
        }
        TypedArray colors = context.getResources().obtainTypedArray(arrayId);
        int returnColor = colors.getColor((int) (Math.random() * ((double) colors.length())), -16777216);
        colors.recycle();
        return returnColor;
    }
}
