package com.dipcore.atvlauncher.helper;

import android.appwidget.AppWidgetHost;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Context;
import android.content.Intent;
import android.os.Parcelable;
import android.view.View;
import android.widget.LinearLayout.LayoutParams;
import com.dipcore.atvlauncher.helper.ActivityForResultHelper.OnActivityResultListener;
import com.dipcore.atvlauncher.widget.LauncherAppWidget.LauncherAppWidgetHost;
import com.dipcore.atvlauncher.widget.LauncherAppWidget.LauncherAppWidgetHostView;
import java.util.ArrayList;
import java.util.Iterator;

public class WidgetHelper {
    public static int REQUEST_CONFIGURE_APPWIDGET = 10001;
    public static int REQUEST_PICK_APPWIDGET = 10000;

    public interface OnConfigureWidgetListener {
        void onConfigureWidget(int i);
    }

    public interface OnPickWidgetListener {
        void onPickWidget(int i);
    }

    public static AppWidgetProviderInfo getAppWidgetInfo(Context context, int appWidgetId) {
        return AppWidgetManager.getInstance(context).getAppWidgetInfo(appWidgetId);
    }

    public static void pickWidget(Context context, final AppWidgetHost appWidgetHost, final OnPickWidgetListener onPickWidgetListener) {
        final int appWidgetId = appWidgetHost.allocateAppWidgetId();
        Intent pickIntent = new Intent("android.appwidget.action.APPWIDGET_PICK");
        pickIntent.putExtra("appWidgetId", appWidgetId);
        addEmptyData(pickIntent);
        ActivityForResultHelper.start(context, pickIntent, REQUEST_PICK_APPWIDGET, new OnActivityResultListener() {
            @Override
            public void onActivityResult(int requestCode, int resultCode, Intent data) {
                if (resultCode == -1) {
                    if (requestCode == WidgetHelper.REQUEST_PICK_APPWIDGET && onPickWidgetListener != null) {
                        onPickWidgetListener.onPickWidget(appWidgetId);
                    }
                } else if (resultCode == 0 && data != null) {
                    int appWidgetId = data.getIntExtra("appWidgetId", 0);
                    if (appWidgetId != 0) {
                        appWidgetHost.deleteAppWidgetId(appWidgetId);
                    }
                }
            }
        });
    }

    public static void configureWidget(Context context, final int appWidgetId, final OnConfigureWidgetListener onConfigureWidgetListener) {
        AppWidgetProviderInfo appWidgetInfo = AppWidgetManager.getInstance(context).getAppWidgetInfo(appWidgetId);
        Intent configureIntent = new Intent("android.appwidget.action.APPWIDGET_CONFIGURE");
        configureIntent.setComponent(appWidgetInfo.configure);
        configureIntent.putExtra("appWidgetId", appWidgetId);
        addEmptyData(configureIntent);
        ActivityForResultHelper.start(context, configureIntent, REQUEST_CONFIGURE_APPWIDGET, new OnActivityResultListener() {
            @Override
            public void onActivityResult(int requestCode, int resultCode, Intent data) {
                if (resultCode == -1) {
                    if (requestCode == WidgetHelper.REQUEST_CONFIGURE_APPWIDGET && onConfigureWidgetListener != null) {
                        onConfigureWidgetListener.onConfigureWidget(appWidgetId);
                    }
                } else if (resultCode == 0 && data != null) {
                }
            }
        });
    }

    public static LauncherAppWidgetHostView createAppWidgetHostView(Context context, LauncherAppWidgetHost appWidgetHost, int appWidgetId) {
        AppWidgetProviderInfo appWidgetInfo = AppWidgetManager.getInstance(context).getAppWidgetInfo(appWidgetId);
        LauncherAppWidgetHostView appWidgetHostView = (LauncherAppWidgetHostView) appWidgetHost.createView(context, appWidgetId, appWidgetInfo);
        appWidgetHostView.setAppWidget(appWidgetId, appWidgetInfo);
        appWidgetHostView.setLayoutParams(new LayoutParams(-1, -1, 17.0f));
        return appWidgetHostView;
    }

    public static void performClick(int x, int y, View parent) {
        int[] location = new int[2];
        parent.getLocationOnScreen(location);
        Iterator it = ViewHelper.findViews(location[0] + x, location[1] + y, parent).iterator();
        while (it.hasNext()) {
            ((View) it.next()).performClick();
        }
    }

    private static void addEmptyData(Intent pickIntent) {
        pickIntent.putParcelableArrayListExtra("customInfo", new ArrayList<Parcelable>());
        pickIntent.putParcelableArrayListExtra("customExtras", new ArrayList<Parcelable>());
    }
}
