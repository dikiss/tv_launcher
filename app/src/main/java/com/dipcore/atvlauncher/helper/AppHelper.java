package com.dipcore.atvlauncher.helper;

import android.content.Context;
import android.content.Intent;
import android.content.pm.ApplicationInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.res.Resources;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Build.VERSION;
import com.dipcore.atvlauncher.item.app.AppLauncherItemModel;
import com.dipcore.atvlauncher.item.folder.FolderLauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherItemModel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class AppHelper {
    public static ArrayList<LauncherItemModel> filterNonExistingItems(ArrayList<LauncherItemModel> list1, ArrayList<LauncherItemModel> list2) {
        ArrayList<LauncherItemModel> listA = flatten(list1);
        ArrayList<LauncherItemModel> listB = flatten(list2);
        ArrayList<LauncherItemModel> entries = new ArrayList();
        if (list1 != null) {
            Iterator it = listA.iterator();
            while (it.hasNext()) {
                LauncherItemModel item = (LauncherItemModel) it.next();
                if (!contains(listB, item)) {
                    entries.add(item);
                }
            }
        }
        return entries;
    }

    public static ArrayList<LauncherItemModel> flatten(ArrayList<LauncherItemModel> list, boolean includeFolders) {
        ArrayList<LauncherItemModel> result = new ArrayList();
        Iterator it = list.iterator();
        while (it.hasNext()) {
            LauncherItemModel item = (LauncherItemModel) it.next();
            if (item instanceof FolderLauncherItemModel) {
                if (includeFolders) {
                    result.add(item);
                }
                result.addAll(flatten(((FolderLauncherItemModel) item).getItems()));
            } else if (item instanceof AppLauncherItemModel) {
                result.add(item);
            } else {
                throw new IllegalArgumentException("Input must be an array of ApplicationLauncherItemModel or nested arrays of ApplicationLauncherItemModel(FolderLauncherItemModel) ");
            }
        }
        return result;
    }

    public static ArrayList<LauncherItemModel> flatten(ArrayList<LauncherItemModel> list) {
        return flatten(list, false);
    }

    public static void removeDoubles(ArrayList<LauncherItemModel> list) {
        Iterator<LauncherItemModel> iterator = list.iterator();
        while (iterator.hasNext()) {
            LauncherItemModel item = (LauncherItemModel) iterator.next();
            if (item instanceof FolderLauncherItemModel) {
                removeDoubles(((FolderLauncherItemModel) item).getItems());
            }
            if ((item instanceof AppLauncherItemModel) && appItemCount(item, flatten(list)) > 1) {
                iterator.remove();
            }
        }
    }

    public static void removeNonExistingItems(ArrayList<LauncherItemModel> list, ArrayList<LauncherItemModel> flatExistingAppList) {
        Iterator<LauncherItemModel> iterator = list.iterator();
        while (iterator.hasNext()) {
            LauncherItemModel item = (LauncherItemModel) iterator.next();
            if (item instanceof FolderLauncherItemModel) {
                removeNonExistingItems(((FolderLauncherItemModel) item).getItems(), flatExistingAppList);
            } else if (!(item instanceof AppLauncherItemModel)) {
                throw new IllegalArgumentException("Input must be an array of ApplicationLauncherItemModel or nested arrays of ApplicationLauncherItemModel(FolderLauncherItemModel) ");
            } else if (!containsItem(item, flatExistingAppList)) {
                iterator.remove();
            }
        }
    }

    public static void addNewItems(ArrayList<LauncherItemModel> list, ArrayList<LauncherItemModel> flatExistingAppList) {
        ArrayList<LauncherItemModel> flatItems = flatten(list);
        Iterator<LauncherItemModel> iterator = flatExistingAppList.iterator();
        while (iterator.hasNext()) {
            LauncherItemModel item = (LauncherItemModel) iterator.next();
            if (!containsItem(item, flatItems)) {
                list.add(item);
            }
        }
    }

    public static int appItemCount(LauncherItemModel item, ArrayList<LauncherItemModel> list) {
        int count = 0;
        Iterator it = list.iterator();
        while (it.hasNext()) {
            LauncherItemModel i = (LauncherItemModel) it.next();
            if (i instanceof AppLauncherItemModel) {
                if (((AppLauncherItemModel) i).getPackageName().equals(((AppLauncherItemModel) item).getPackageName())) {
                    count++;
                }
            }
        }
        return count;
    }

    public static boolean containsItem(LauncherItemModel item, ArrayList<LauncherItemModel> list) {
        Iterator it = list.iterator();
        while (it.hasNext()) {
            LauncherItemModel i = (LauncherItemModel) it.next();
            if (i instanceof AppLauncherItemModel) {
                if (((AppLauncherItemModel) i).getPackageName().equals(((AppLauncherItemModel) item).getPackageName())) {
                    return true;
                }
            }
        }
        return false;
    }

    public static ArrayList<LauncherItemModel> merge(ArrayList<LauncherItemModel> list1, ArrayList<LauncherItemModel> list2) {
        Iterator it;
        LauncherItemModel item;
        ArrayList<LauncherItemModel> entries = new ArrayList<>();
        if (list1 != null) {
            it = list1.iterator();
            while (it.hasNext()) {
                item = (LauncherItemModel) it.next();
                if (contains(list2, item)) {
                    entries.add(item);
                }
            }
        }
        it = list2.iterator();
        while (it.hasNext()) {
            item = (LauncherItemModel) it.next();
            if (!contains(entries, item)) {
                entries.add(item);
            }
        }
        return entries;
    }

    public static boolean contains(ArrayList<LauncherItemModel> list, LauncherItemModel item) {
        if (item instanceof FolderLauncherItemModel) {
            return true;
        }
        if (item instanceof AppLauncherItemModel) {
            Iterator it = list.iterator();
            while (it.hasNext()) {
                LauncherItemModel i = (LauncherItemModel) it.next();
                if (i instanceof AppLauncherItemModel) {
                    if (((AppLauncherItemModel) i).getPackageName().equals(((AppLauncherItemModel) item).getPackageName())) {
                        return true;
                    }
                }
            }
        }
        return false;
    }

    public static ArrayList<LauncherItemModel> getInstalledApplications(Context context) {
        PackageManager mPackageManager = context.getPackageManager();
        ArrayList<LauncherItemModel> entries = new ArrayList<>();
        try {
            List<ApplicationInfo> apps = mPackageManager.getInstalledApplications(0);
            if (apps == null) {
                apps = new ArrayList<>();
            }
            for (ApplicationInfo app : apps) {
                String packageName = app.packageName;
                if (mPackageManager.getLaunchIntentForPackage(packageName) != null || (VERSION.SDK_INT >= 21 && mPackageManager.getLeanbackLaunchIntentForPackage(packageName) != null)) {
                    String appName = app.loadLabel(mPackageManager).toString();
                    AppLauncherItemModel item = new AppLauncherItemModel();
                    item.setTitle(appName);
                    item.setPackageName(packageName);
                    entries.add(item);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return entries;
    }

    public static ApplicationInfo getApplicationInfo(Context context, String packageName) {
        try {
            return context.getPackageManager().getApplicationInfo(packageName, 0);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void uninstall(Context context, String packageName) {
        Intent intent = new Intent("android.intent.action.DELETE");
        intent.setData(Uri.parse("package:" + packageName));
        intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
        context.startActivity(intent);
    }

    public static boolean isSystemApp(Context context, String packageName) {
        return (getApplicationInfo(context, packageName).flags & 1) != 0;
    }

    public static Drawable getIcon(Context context, String packageName) {
        try {
            PackageManager packageManager = context.getPackageManager();
            return packageManager.getApplicationInfo(packageName, 0).loadIcon(packageManager);
        } catch (NameNotFoundException e) {
            return null;
        }
    }

    public static Drawable getIconWithDensity(Context context, String packageName, int density) {
        try {
            PackageManager packageManager = context.getPackageManager();
            ApplicationInfo applicationInfo = packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA);
            Resources res = packageManager.getResourcesForApplication(applicationInfo);
            if (VERSION.SDK_INT >= 21) {
                return res.getDrawableForDensity(applicationInfo.icon, density, null);
            }
            return res.getDrawableForDensity(applicationInfo.icon, density);
        } catch (NameNotFoundException e) {
            return null;
        }
    }

    public static Drawable getBanner(Context context, String packageName) {
        try {
            PackageManager packageManager = context.getPackageManager();
            return packageManager.getApplicationInfo(packageName, 0).loadBanner(packageManager);
        } catch (NameNotFoundException e) {
            return null;
        }
    }

    public static String getName(Context context, String packageName) {
        try {
            PackageManager packageManager = context.getPackageManager();
            return (String) packageManager.getApplicationLabel(packageManager.getApplicationInfo(packageName, PackageManager.GET_META_DATA));
        } catch (NameNotFoundException e) {
            return null;
        }
    }

    public static void launch(Context context, String packageName) {
        Intent launchIntent = null;
        if (VERSION.SDK_INT >= 21) {
            launchIntent = context.getPackageManager().getLeanbackLaunchIntentForPackage(packageName);
        }
        if (launchIntent == null) {
            launchIntent = context.getPackageManager().getLaunchIntentForPackage(packageName);
        }
        if (launchIntent != null) {
            launchIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            context.startActivity(launchIntent);
        }
    }
}
