package com.dipcore.atvlauncher.helper;

import android.content.Context;

public class ResourceHelper {
    public static String getStringResourceByName(Context context, String name) {
        return context.getString(context.getResources().getIdentifier(name, "string", context.getPackageName()));
    }
}
