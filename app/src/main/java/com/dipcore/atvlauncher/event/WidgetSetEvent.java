package com.dipcore.atvlauncher.event;

public class WidgetSetEvent {
    public int appWidgetId;
    public int position;
    public boolean reset = true;

    public WidgetSetEvent(int position, int appWidgetId) {
        this.position = position;
        this.appWidgetId = appWidgetId;
    }

    public WidgetSetEvent(int position, int appWidgetId, boolean reset) {
        this.position = position;
        this.appWidgetId = appWidgetId;
        this.reset = reset;
    }
}
