package com.dipcore.atvlauncher.event;

public class LoaderLoadApplicationsAction {
    public boolean forceReload = false;

    public LoaderLoadApplicationsAction() {
    }

    public LoaderLoadApplicationsAction(boolean forceReload) {
        this.forceReload = forceReload;
    }
}
