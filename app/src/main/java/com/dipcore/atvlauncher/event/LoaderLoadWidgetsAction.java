package com.dipcore.atvlauncher.event;

public class LoaderLoadWidgetsAction {
    public boolean forceReload = false;

    public LoaderLoadWidgetsAction() {
    }

    public LoaderLoadWidgetsAction(boolean forceReload) {
        this.forceReload = forceReload;
    }
}
