package com.dipcore.atvlauncher.event;

public class StorageSaveItemsAction {
    public String sectionUuid;

    public StorageSaveItemsAction() {
        this.sectionUuid = "save-all";
    }

    public StorageSaveItemsAction(String sectionUuid) {
        this.sectionUuid = sectionUuid;
    }
}
