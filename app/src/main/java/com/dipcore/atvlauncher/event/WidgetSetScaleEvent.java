package com.dipcore.atvlauncher.event;

public class WidgetSetScaleEvent {
    public int position;
    public int scale;

    public WidgetSetScaleEvent(int position, int scale) {
        this.position = position;
        this.scale = scale;
    }
}
