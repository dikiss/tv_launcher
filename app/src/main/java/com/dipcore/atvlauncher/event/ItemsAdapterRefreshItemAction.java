package com.dipcore.atvlauncher.event;

public class ItemsAdapterRefreshItemAction {
    public String adapterUuid;
    public String uuid;

    public ItemsAdapterRefreshItemAction(String adapterUuid, String uuid) {
        this.adapterUuid = adapterUuid;
        this.uuid = uuid;
    }
}
