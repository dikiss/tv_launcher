package com.dipcore.atvlauncher.event;

public class ItemsAdapterRemoveItemAction {
    public String adapterUuid;
    public String uuid;

    public ItemsAdapterRemoveItemAction(String adapterUuid, String uuid) {
        this.adapterUuid = adapterUuid;
        this.uuid = uuid;
    }
}
