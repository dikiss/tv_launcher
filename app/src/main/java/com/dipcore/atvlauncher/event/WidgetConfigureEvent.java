package com.dipcore.atvlauncher.event;

public class WidgetConfigureEvent {
    public int appWidgetId = 0;
    public int position;

    public WidgetConfigureEvent(int position) {
        this.position = position;
    }

    public WidgetConfigureEvent(int position, int appWidgetId) {
        this.position = position;
        this.appWidgetId = appWidgetId;
    }
}
