package com.dipcore.atvlauncher.event;

public class ItemsAdapterMoveItemAction {
    public String adapterUuid;
    public String fromUuid;
    public String toUuid;

    public ItemsAdapterMoveItemAction(String adapterUuid, String fromUuid, String toUuid) {
        this.adapterUuid = adapterUuid;
        this.fromUuid = fromUuid;
        this.toUuid = toUuid;
    }
}
