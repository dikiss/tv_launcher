package com.dipcore.atvlauncher.event;

public class WidgetSetTapCoordinatesEvent {
    public int position;
    public int f2x;
    public int f3y;

    public WidgetSetTapCoordinatesEvent(int position, int x, int y) {
        this.position = position;
        this.f2x = x;
        this.f3y = y;
    }
}
