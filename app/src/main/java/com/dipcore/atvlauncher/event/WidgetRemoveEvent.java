package com.dipcore.atvlauncher.event;

public class WidgetRemoveEvent {
    public int position;

    public WidgetRemoveEvent(int position) {
        this.position = position;
    }
}
