package com.dipcore.atvlauncher.event;

public class LoaderLoadInputsAction {
    public boolean forceReload = false;

    public LoaderLoadInputsAction() {
    }

    public LoaderLoadInputsAction(boolean forceReload) {
        this.forceReload = forceReload;
    }
}
