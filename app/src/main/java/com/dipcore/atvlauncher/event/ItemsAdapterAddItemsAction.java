package com.dipcore.atvlauncher.event;

import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherItemModel;
import java.util.ArrayList;

public class ItemsAdapterAddItemsAction {
    public String adapterUuid;
    public ArrayList<LauncherItemModel> items;

    public ItemsAdapterAddItemsAction(String adapterUuid, ArrayList<LauncherItemModel> items) {
        this.adapterUuid = adapterUuid;
        this.items = items;
    }
}
