package com.dipcore.atvlauncher.event;

import com.dipcore.atvlauncher.widget.RendererRecyclerView.ItemModel;
import java.util.ArrayList;

public class ItemsAdapterSetItemsAction {
    public String adapterUuid;
    public ArrayList<ItemModel> items;

    public ItemsAdapterSetItemsAction(String adapterUuid, ArrayList<ItemModel> items) {
        this.adapterUuid = adapterUuid;
        this.items = items;
    }
}
