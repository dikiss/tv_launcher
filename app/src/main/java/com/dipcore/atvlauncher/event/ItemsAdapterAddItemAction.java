package com.dipcore.atvlauncher.event;

import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherItemModel;

public class ItemsAdapterAddItemAction {
    public String adapterUuid;
    public LauncherItemModel item;
    public int position;

    public ItemsAdapterAddItemAction(String adapterUuid, int position, LauncherItemModel item) {
        this.adapterUuid = adapterUuid;
        this.position = position;
        this.item = item;
    }

    public ItemsAdapterAddItemAction(String adapterUuid, LauncherItemModel item) {
        this(adapterUuid, -1, item);
    }
}
