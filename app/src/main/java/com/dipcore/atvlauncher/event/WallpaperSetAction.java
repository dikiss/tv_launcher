package com.dipcore.atvlauncher.event;

import android.net.Uri;

public class WallpaperSetAction {
    public String path;
    public Uri uri;

    public WallpaperSetAction(String path) {
        this.path = path;
    }

    public WallpaperSetAction(Uri uri) {
        this.uri = uri;
    }
}
