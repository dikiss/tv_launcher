package com.dipcore.atvlauncher.event;

public class WidgetTapEvent {
    public int position;

    public WidgetTapEvent(int position) {
        this.position = position;
    }
}
