package com.dipcore.atvlauncher.appwidget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetManager;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.preference.PreferenceManager;
import android.widget.RemoteViews;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.helper.AppHelper;

public class AppWidgetFavoriteProvider extends AppWidgetProvider {
    private Context context;
    @Override
    public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
        this.context = context;
        for (int widgetId : appWidgetIds) {
            RemoteViews remoteViews = new RemoteViews(context.getPackageName(), R.layout.app_widget_favorite);
            String packageName = getApplicationPackageName(widgetId);
            if (packageName != null) {
                if (isShowTitle(widgetId)) {
                    remoteViews.setViewVisibility(R.id.title_text_view, 0);
                    remoteViews.setTextViewText(R.id.title_text_view, AppHelper.getName(context, packageName));
                } else {
                    remoteViews.setViewVisibility(R.id.title_text_view, 8);
                }
                if (isShowIcon(widgetId)) {
                    Bitmap icon = ((BitmapDrawable) AppHelper.getIconWithDensity(context, packageName, 480)).getBitmap();
                    remoteViews.setViewVisibility(R.id.icon_image_view, 0);
                    remoteViews.setImageViewBitmap(R.id.icon_image_view, icon);
                } else {
                    remoteViews.setViewVisibility(R.id.icon_image_view, 8);
                }
                remoteViews.setOnClickPendingIntent(R.id.container_relative_layout, PendingIntent.getActivity(context, 0, context.getPackageManager().getLaunchIntentForPackage(packageName), 0));
                appWidgetManager.updateAppWidget(widgetId, remoteViews);
            }
        }
    }

    private Boolean isShowTitle(int appWidgetId) {
        return (PreferenceManager.getDefaultSharedPreferences(this.context).getBoolean("app-widget-favorite-show-title-" + appWidgetId, true));
    }

    private Boolean isShowIcon(int appWidgetId) {
        return (PreferenceManager.getDefaultSharedPreferences(this.context).getBoolean("app-widget-favorite-show-icon-" + appWidgetId, true));
    }

    private String getApplicationPackageName(int appWidgetId) {
        return PreferenceManager.getDefaultSharedPreferences(this.context).getString("app-widget-favorite-package-name-" + appWidgetId, null);
    }
}
