package com.dipcore.atvlauncher.appwidget;

import android.app.Activity;
import android.content.Intent;
import android.content.SharedPreferences.Editor;
import android.os.Bundle;
import android.preference.PreferenceManager;
import android.support.annotation.NonNull;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.dialog.DialogSelectApplication;
import com.dipcore.atvlauncher.dialog.DialogSelectApplication.OnItemClickedListener;
import com.dipcore.atvlauncher.helper.AppHelper;
import com.dipcore.atvlauncher.item.app.AppLauncherItemModel;
import com.dipcore.menuitems.CheckBox;
import com.dipcore.menuitems.Text;
import com.dipcore.smoothcheckbox.SmoothCheckBox;
import com.dipcore.smoothcheckbox.SmoothCheckBox.OnCheckedChangeListener;

public class AppWidgetFavoriteConfigure extends Activity {
    public String PACKAGE_NAME_PREF = "";
    private CheckBox displayIconCheckBox;
    OnCheckedChangeListener displayIconCheckBoxOnCheckedChangeListener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(SmoothCheckBox checkBox, boolean isChecked) {
            AppWidgetFavoriteConfigure.this.setShowIcon(isChecked);
        }
    };
    private CheckBox displayTitleCheckBox;
    OnCheckedChangeListener displayTitleCheckBoxOnCheckedChangeListener = new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(SmoothCheckBox checkBox, boolean isChecked) {
            AppWidgetFavoriteConfigure.this.setShowTitle(isChecked);
        }
    };
    private Button doneButton;
    OnClickListener doneButtonOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            int[] ids = new int[]{AppWidgetFavoriteConfigure.this.mAppWidgetId};
            Intent updateIntent = new Intent();
            updateIntent.setAction("android.appwidget.action.APPWIDGET_UPDATE");
            updateIntent.putExtra("appWidgetIds", ids);
            AppWidgetFavoriteConfigure.this.sendBroadcast(updateIntent);
            Intent resultValue = new Intent();
            resultValue.putExtra("appWidgetId", AppWidgetFavoriteConfigure.this.mAppWidgetId);
            AppWidgetFavoriteConfigure.this.setResult(-1, resultValue);
            AppWidgetFavoriteConfigure.this.finish();
        }
    };
    private int mAppWidgetId;
    private Text selectApplicationText;
    OnClickListener selectApplicationTextOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View v) {
            final DialogSelectApplication dialog = new DialogSelectApplication(AppWidgetFavoriteConfigure.this);
            dialog.setOnItemClickedListener(new OnItemClickedListener() {
                @Override
                public void onItemClicked(@NonNull AppLauncherItemModel model, View view) {
                    AppWidgetFavoriteConfigure.this.setApplicationPackageName(model.getPackageName());
                    AppWidgetFavoriteConfigure.this.initValues();
                    dialog.dismiss();
                }
            });
            dialog.show();
        }
    };
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.app_widget_favorite_configure);
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            this.mAppWidgetId = extras.getInt("appWidgetId", 0);
        }
        if (this.mAppWidgetId == 0) {
            finish();
            return;
        }
        Intent cancelResultValue = new Intent();
        cancelResultValue.putExtra("appWidgetId", this.mAppWidgetId);
        setResult(0, cancelResultValue);
        initViews();
        initCallbacks();
        initValues();
    }

    private void initViews() {
        this.selectApplicationText = findViewById(R.id.select_application_text);
        this.displayIconCheckBox = findViewById(R.id.display_icon_check_box);
        this.displayTitleCheckBox = findViewById(R.id.display_title_text_item);
        this.doneButton = findViewById(R.id.done_button);
    }

    private void initValues() {
        String packageName = getApplicationPackageName();
        String selectApplicationString = getString(R.string.app_widget_configure_select_application_detail);
        if (packageName == null) {
            selectApplicationString = selectApplicationString + " " + getString(R.string.app_widget_configure_select_application_detail_none);
        } else {
            selectApplicationString = selectApplicationString + " " + AppHelper.getName(this, packageName);
        }
        this.selectApplicationText.setDetailText(selectApplicationString);
        this.displayIconCheckBox.setChecked(isShowIcon(), false);
        this.displayTitleCheckBox.setChecked(isShowTitle(), false);
    }

    private void initCallbacks() {
        this.displayIconCheckBox.setOnCheckedChangeListener(this.displayIconCheckBoxOnCheckedChangeListener);
        this.displayTitleCheckBox.setOnCheckedChangeListener(this.displayTitleCheckBoxOnCheckedChangeListener);
        this.selectApplicationText.setOnClickListener(this.selectApplicationTextOnClickListener);
        this.doneButton.setOnClickListener(this.doneButtonOnClickListener);
    }

    private String getApplicationPackageName() {
        return PreferenceManager.getDefaultSharedPreferences(this).getString("app-widget-favorite-package-name-" + this.mAppWidgetId, null);
    }

    private void setApplicationPackageName(String name) {
        Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
        edit.putString("app-widget-favorite-package-name-" + this.mAppWidgetId, name);
        edit.commit();
    }

    private Boolean isShowIcon() {
        return (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("app-widget-favorite-show-icon-" + this.mAppWidgetId, true));
    }

    private void setShowIcon(Boolean flag) {
        Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
        edit.putBoolean("app-widget-favorite-show-icon-" + this.mAppWidgetId, flag);
        edit.commit();
    }

    private Boolean isShowTitle() {
        return (PreferenceManager.getDefaultSharedPreferences(this).getBoolean("app-widget-favorite-show-title-" + this.mAppWidgetId, true));
    }

    private void setShowTitle(Boolean flag) {
        Editor edit = PreferenceManager.getDefaultSharedPreferences(this).edit();
        edit.putBoolean("app-widget-favorite-show-title-" + this.mAppWidgetId, flag);
        edit.commit();
    }
}
