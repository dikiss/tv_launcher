package com.dipcore.atvlauncher;

import android.app.Activity;
import android.support.v7.widget.GridLayoutManager;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherRendererRecyclerViewAdapter;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherSections;
import com.dipcore.atvlauncher.widget.RecyclerView;

public class ItemsActivity extends Activity {
    private GridLayoutManager gridLayoutManager;
    private LauncherSections launcherSections;
    private RecyclerView recyclerView;
    private LauncherRendererRecyclerViewAdapter recyclerViewAdapter;

    public LauncherRendererRecyclerViewAdapter getRecyclerViewAdapter() {
        return this.recyclerViewAdapter;
    }

    public void setRecyclerViewAdapter(LauncherRendererRecyclerViewAdapter recyclerViewAdapter) {
        this.recyclerViewAdapter = recyclerViewAdapter;
    }

    public RecyclerView getRecyclerView() {
        return this.recyclerView;
    }

    public void setRecyclerView(RecyclerView recyclerView) {
        this.recyclerView = recyclerView;
    }

    public GridLayoutManager getGridLayoutManager() {
        return this.gridLayoutManager;
    }

    public void setGridLayoutManager(GridLayoutManager gridLayoutManager) {
        this.gridLayoutManager = gridLayoutManager;
    }

    public LauncherSections getLauncherSections() {
        return this.launcherSections;
    }

    public void setLauncherSections(LauncherSections launcherSections) {
        this.launcherSections = launcherSections;
    }

    public void moveItem(String fromUuid, String toUuid) {
        this.recyclerViewAdapter.moveItem(fromUuid, toUuid);
        this.launcherSections.moveItem(fromUuid, toUuid);
    }
}
