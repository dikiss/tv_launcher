package com.dipcore.atvlauncher;

public class LauncherConstants {
    public static final int APPWIDGET_HOST_ID = 9000019;
    public static final String AppsSectionUuid = "58a38b88-5089-4cf9-9888-8cf6395a7684";
    public static final String FolderAdapterUuid = "5e584a1d-5940-466a-9963-79a3d391979d";
    public static final String HomeAdapterUuid = "6731ef21-61b9-4789-b7ab-78dc56ae4187";
    public static final String InputsSectionUuid = "172855d6-89ed-475b-81aa-e61abda42f27";
    public static final String WidgetSectionUuid = "e91dd30d-a3ce-44df-a309-ad096d100f09";
}
