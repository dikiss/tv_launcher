package com.dipcore.atvlauncher;

import android.os.Bundle;
import android.support.v7.widget.GridLayoutManager;
import android.util.DisplayMetrics;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;
import com.dipcore.atvlauncher.decorator.EqualSpacesItemDecoration;
import com.dipcore.atvlauncher.event.ItemsAdapterRefreshItemAction;
import com.dipcore.atvlauncher.event.StorageSaveItemsAction;
import com.dipcore.atvlauncher.helper.CompatUtils;
import com.dipcore.atvlauncher.item.app.AppItemListener;
import com.dipcore.atvlauncher.item.app.AppViewRenderer;
import com.dipcore.atvlauncher.item.folder.FolderItemListener;
import com.dipcore.atvlauncher.item.folder.FolderLauncherItemModel;
import com.dipcore.atvlauncher.item.folder.FolderViewRenderer;
import com.dipcore.atvlauncher.listener.ItemsAdapterEventListener;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherRendererRecyclerViewAdapter;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherSections;
import com.dipcore.atvlauncher.widget.RecyclerView;
import org.greenrobot.eventbus.EventBus;

public class FolderActivity extends ItemsActivity {
    private TextView emptyFolderTextView;
    private FolderLauncherItemModel folderItem;
    private ItemsAdapterEventListener itemsAdapterEventListener;
    private RecyclerView recyclerView;
    private EditText titleEditText;
    private String uuid;
    private TextView itemMemory;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_folder);
        this.itemsAdapterEventListener = new ItemsAdapterEventListener(this);
        this.uuid = getIntent().getStringExtra("ITEM_UUID");
        this.folderItem = (FolderLauncherItemModel) LauncherApplication.getInstance().getLauncherSections().getItemByUuid(this.uuid);
        LauncherSections launcherSections = new LauncherSections();
        launcherSections.addSection(this.folderItem.getUuid(), this.folderItem.getItems());
        setLauncherSections(launcherSections);
        initWindow();
        initViews();
        initRecyclerView();
    }
    @Override
    protected void onStart() {
        super.onStart();
        this.itemsAdapterEventListener.register();
        this.recyclerView.requestFocus(130);
    }
    @Override
    protected void onStop() {
        this.itemsAdapterEventListener.unregister();
        updateTitle();
        super.onStop();
    }

    private void initWindow() {
        DisplayMetrics metrics = getResources().getDisplayMetrics();
        getWindow().setLayout((int) (((double) metrics.widthPixels) * 0.85d), (int) (((double) metrics.heightPixels) * 0.85d));
    }

    private void initViews() {
        this.recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        this.emptyFolderTextView = (TextView) findViewById(R.id.empty_folder_text_view);
        this.titleEditText = (EditText) findViewById(R.id.title_edit_text);
        this.titleEditText.setText(this.folderItem.getTitle());
        if (this.folderItem.getItems().size() > 0) {
            this.recyclerView.setVisibility(View.VISIBLE);
            this.emptyFolderTextView.setVisibility(View.GONE);
            return;
        }
        this.recyclerView.setVisibility(View.GONE);
        this.emptyFolderTextView.setVisibility(View.VISIBLE);
    }

    private void initRecyclerView() {
        LauncherRendererRecyclerViewAdapter recyclerViewAdapter = new LauncherRendererRecyclerViewAdapter();
        recyclerViewAdapter.setUuid(LauncherConstants.FolderAdapterUuid);
        recyclerViewAdapter.registerRenderer(new AppViewRenderer(3, this, new AppItemListener(this)));
        recyclerViewAdapter.registerRenderer(new FolderViewRenderer(4, this, new FolderItemListener(this)));
        recyclerViewAdapter.setItems(getLauncherSections().flatten(true));
        setRecyclerViewAdapter(recyclerViewAdapter);
        this.itemsAdapterEventListener.setRecyclerViewAdapter(recyclerViewAdapter);
        setGridLayoutManager(new GridLayoutManager(this, 5));
        this.recyclerView.setLayoutManager(getGridLayoutManager());
        this.recyclerView.setAdapter(recyclerViewAdapter);
        this.recyclerView.addItemDecoration(new EqualSpacesItemDecoration(CompatUtils.dp2px(this, 5.0f)));
        setRecyclerView(this.recyclerView);
    }

    private void updateTitle() {
        if (!this.folderItem.getTitle().equals(this.titleEditText.getText().toString())) {
            this.folderItem.setTitle(this.titleEditText.getText().toString());
            EventBus.getDefault().post(new ItemsAdapterRefreshItemAction(LauncherConstants.HomeAdapterUuid, this.uuid));
            EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.AppsSectionUuid));
        }
    }
}
