package com.dipcore.atvlauncher.decorator;

import android.graphics.Rect;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.ItemDecoration;
import android.support.v7.widget.RecyclerView.State;
import android.view.View;

public class EqualSpacesItemDecoration extends ItemDecoration {
    private final int mSpace;

    public EqualSpacesItemDecoration(int space) {
        this.mSpace = space;
    }
    @Override
    public void getItemOffsets(Rect outRect, View view, RecyclerView parent, State state) {
        if (parent.getLayoutManager() instanceof GridLayoutManager) {
            outRect.left = (int) Math.floor((double) (((float) this.mSpace) / 2.0f));
            outRect.right = (int) Math.floor((double) (((float) this.mSpace) / 2.0f));
            outRect.bottom = (int) Math.floor((double) (((float) this.mSpace) / 2.0f));
            outRect.top = (int) Math.floor((double) (((float) this.mSpace) / 2.0f));
            return;
        }
        throw new UnsupportedClassVersionError("Unsupported LayoutManager");
    }
}
