package com.dipcore.atvlauncher.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.SeekBar.OnSeekBarChangeListener;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.helper.WidgetHelper;
import com.dipcore.atvlauncher.item.widget.WidgetLauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherAppWidget.LauncherAppWidgetHostView;
import com.dipcore.menuitems.SeekBar;

public class DialogWidgetScaleFactor extends Dialog {
    private LinearLayout appWidgetContainer;
    private Button applyButton;
    private View.OnClickListener applyButtonOnClickListener;
    private Button cancelButton;
    private View.OnClickListener cancelButtonOnClickListener;
    private LauncherAppWidgetHostView launcherAppWidgetHostView;
    private OnSeekBarChangeListener onSeekBarChangeListener;
    public int scale;
    private SeekBar scaleSeekBar;
    private int widgetHeight;
    private WidgetLauncherItemModel widgetItemModel;
    private int widgetWidth;

    public DialogWidgetScaleFactor(Context context) {
        this(context, 0);
    }

    public DialogWidgetScaleFactor(Context context, int themeResId) {
        super(context, themeResId);
        this.onSeekBarChangeListener = new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(android.widget.SeekBar seekBar, int i, boolean b) {
                if (DialogWidgetScaleFactor.this.launcherAppWidgetHostView != null) {
                    DialogWidgetScaleFactor.this.launcherAppWidgetHostView.setScale(i);
                }
            }
            @Override
            public void onStartTrackingTouch(android.widget.SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(android.widget.SeekBar seekBar) {
            }
        };
        this.cancelButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogWidgetScaleFactor.this.dismiss();
            }
        };
        this.applyButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogWidgetScaleFactor.this.scale = DialogWidgetScaleFactor.this.launcherAppWidgetHostView.getScale();
                DialogWidgetScaleFactor.this.dismiss();
            }
        };
        init();
        initCallbacks();
    }

    public void setWidgetItemModel(WidgetLauncherItemModel widgetItemModel) {
        this.widgetItemModel = widgetItemModel;
        setScale(widgetItemModel.getScaleFactor());
    }

    public void setWidgetHeight(int widgetHeight) {
        this.widgetHeight = widgetHeight;
    }

    public void setWidgetWidth(int widgetWidth) {
        this.widgetWidth = widgetWidth;
    }
    @Override
    public void show() {
        this.launcherAppWidgetHostView = WidgetHelper.createAppWidgetHostView(getContext(), this.widgetItemModel.getLauncherAppWidgetHost(), this.widgetItemModel.getAppWidgetId());
        this.launcherAppWidgetHostView.setScale(this.scale);
        this.launcherAppWidgetHostView.setBackgroundColor(0x40000000);
        this.appWidgetContainer.addView(this.launcherAppWidgetHostView);
        LayoutParams params = this.launcherAppWidgetHostView.getLayoutParams();
        params.width = this.widgetWidth;
        params.height = this.widgetHeight;
        this.launcherAppWidgetHostView.setLayoutParams(params);
        super.show();
    }

    public void setScale(int scale) {
        this.scale = scale;
        this.scaleSeekBar.setValue(scale);
    }

    public int getScale() {
        return this.scale;
    }

    private void init() {
        requestWindowFeature(1);
        setContentView(R.layout.dialog_widget_scale_widget);
        this.appWidgetContainer = findViewById(R.id.app_widget_container);
        this.scaleSeekBar = findViewById(R.id.scale_seek_bar);
        this.cancelButton = findViewById(R.id.cancel_button);
        this.applyButton = findViewById(R.id.apply_button);
    }

    private void initCallbacks() {
        this.scaleSeekBar.setOnSeekBarChangeListener(this.onSeekBarChangeListener);
        this.cancelButton.setOnClickListener(this.cancelButtonOnClickListener);
        this.applyButton.setOnClickListener(this.applyButtonOnClickListener);
    }
}
