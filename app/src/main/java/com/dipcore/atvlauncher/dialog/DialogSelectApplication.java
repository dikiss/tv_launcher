package com.dipcore.atvlauncher.dialog;

import android.app.Dialog;
import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.helper.AppHelper;
import com.dipcore.atvlauncher.item.app.AppLauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherItemModel;
import java.util.ArrayList;

public class DialogSelectApplication extends Dialog {
    private AppListAdapter appListAdapter;
    private OnItemClickedListener onItemClickedListener;
    private RecyclerView recyclerView;

    public interface OnItemClickedListener {
        void onItemClicked(@NonNull AppLauncherItemModel appLauncherItemModel, View view);
    }

    private class AppListAdapter extends Adapter<AppListAdapter.ViewHolder> {
        private Context context;
        private ArrayList<LauncherItemModel> items;

        public class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
            public TextView detailTextView;
            public ImageView imageView;
            public TextView titleTextView;

            public ViewHolder(ViewGroup view) {
                super(view);
                this.imageView = view.findViewById(R.id.image_view);
                this.titleTextView = view.findViewById(R.id.title_text_view);
                this.detailTextView = view.findViewById(R.id.detail_text_view);
            }
        }

        public AppListAdapter(Context context, ArrayList<LauncherItemModel> items) {
            this.context = context;
            this.items = items;
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder((ViewGroup) LayoutInflater.from(parent.getContext()).inflate(R.layout.dialog_select_application_item, parent, false));
        }
        @Override
        public void onBindViewHolder(ViewHolder holder, int position) {
            final AppLauncherItemModel model = (AppLauncherItemModel) this.items.get(position);
            holder.detailTextView.setText(model.getPackageName());
            holder.titleTextView.setText(model.getTitle());
            holder.imageView.setImageDrawable(AppHelper.getIcon(this.context, model.getPackageName()));
            holder.itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (DialogSelectApplication.this.onItemClickedListener != null) {
                        DialogSelectApplication.this.onItemClickedListener.onItemClicked(model, v);
                    }
                }
            });
        }
        @Override
        public int getItemCount() {
            return this.items.size();
        }
    }

    public DialogSelectApplication(Context context) {
        this(context, 0);
    }

    public DialogSelectApplication(Context context, int themeResId) {
        super(context, themeResId);
        init();
    }

    public void setOnItemClickedListener(OnItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }

    private void init() {
        requestWindowFeature(1);
        setContentView(R.layout.dialog_select_application);
        getWindow().setLayout(-2, (int) (((double) getContext().getResources().getDisplayMetrics().heightPixels) * 0.85d));
        this.appListAdapter = new AppListAdapter(getContext(), AppHelper.getInstalledApplications(getContext()));
        this.recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        this.recyclerView.setAdapter(this.appListAdapter);
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
    }
}
