package com.dipcore.atvlauncher.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.dipcore.atvlauncher.R;

public class DialogConfirmation extends Dialog {
    private Button applyButton;
    private View.OnClickListener applyButtonOnClickListener;
    private Button cancelButton;
    private View.OnClickListener cancelButtonOnClickListener;
    private TextView contentTextView;
    private OnNegativeButtonClickedListener onNegativeButtonClickedListener;
    private OnPositiveButtonClickedListener onPositiveButtonClickedListener;
    private TextView titleTextView;

    public interface OnNegativeButtonClickedListener {
        void onClick(View view);
    }

    public interface OnPositiveButtonClickedListener {
        void onClick(View view);
    }

    public DialogConfirmation(Context context) {
        this(context, 0);
    }

    public DialogConfirmation(Context context, int themeResId) {
        super(context, themeResId);
        this.cancelButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (onNegativeButtonClickedListener != null) {
                    onNegativeButtonClickedListener.onClick(view);
                }
            }
        };
        this.applyButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (onPositiveButtonClickedListener != null) {
                    onPositiveButtonClickedListener.onClick(view);
                }
            }
        };
        init();
        initCallbacks();
    }

    public void setTitle(String title) {
        this.titleTextView.setText(title);
    }

    public void setContent(String content) {
        this.contentTextView.setText(content);
    }

    public void setTitle(int resId) {
        this.titleTextView.setText(resId);
    }

    public void setContent(int resId) {
        this.contentTextView.setText(resId);
    }
    @Override
    public void show() {
        super.show();
        this.cancelButton.requestFocus();
    }

    public void setOnPositiveButtonClickedListener(OnPositiveButtonClickedListener listener) {
        this.onPositiveButtonClickedListener = listener;
    }

    public void setOnNegativeButtonClickedListener(OnNegativeButtonClickedListener listener) {
        this.onNegativeButtonClickedListener = listener;
    }

    private void init() {
        requestWindowFeature(1);
        setContentView(R.layout.dialog_confirmation);
        this.titleTextView = (TextView) findViewById(R.id.title_text_view);
        this.contentTextView = (TextView) findViewById(R.id.content_text_view);
        this.cancelButton = (Button) findViewById(R.id.cancel_button);
        this.applyButton = (Button) findViewById(R.id.apply_button);
    }

    private void initCallbacks() {
        this.cancelButton.setOnClickListener(this.cancelButtonOnClickListener);
        this.applyButton.setOnClickListener(this.applyButtonOnClickListener);
    }
}
