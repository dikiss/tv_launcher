package com.dipcore.atvlauncher.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.TextView;
import com.dipcore.atvlauncher.R;

public class DialogNotification extends Dialog {
    private TextView contentTextView;
    private Button okButton;
    private View.OnClickListener okButtonOnClickListener;
    private OnButtonClickedListener onButtonClickedListener;
    private TextView titleTextView;

    public interface OnButtonClickedListener {
        void onClick(View view);
    }

    public DialogNotification(Context context) {
        this(context, 0);
    }

    public DialogNotification(Context context, int themeResId) {
        super(context, themeResId);
        this.okButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (onButtonClickedListener != null) {
                    onButtonClickedListener.onClick(view);
                }
            }
        };
        init();
        initCallbacks();
    }

    public void setTitle(String title) {
        this.titleTextView.setText(title);
    }

    public void setContent(String content) {
        this.contentTextView.setText(content);
    }

    public void setTitle(int resId) {
        this.titleTextView.setText(resId);
    }

    public void setContent(int resId) {
        this.contentTextView.setText(resId);
    }

    public void show() {
        super.show();
        this.okButton.requestFocus();
    }

    public void setOnButtonClickedListener(OnButtonClickedListener listener) {
        this.onButtonClickedListener = listener;
    }

    private void init() {
        requestWindowFeature(1);
        setContentView(R.layout.dialog_notification);
        this.titleTextView = (TextView) findViewById(R.id.title_text_view);
        this.contentTextView = (TextView) findViewById(R.id.content_text_view);
        this.okButton = (Button) findViewById(R.id.ok_button);
    }

    private void initCallbacks() {
        this.okButton.setOnClickListener(this.okButtonOnClickListener);
    }
}
