package com.dipcore.atvlauncher.dialog;

import android.app.Dialog;
import android.content.Context;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.LauncherApplication;
import com.dipcore.atvlauncher.LauncherConstants;
import com.dipcore.atvlauncher.event.ItemsAdapterAddItemAction;
import com.dipcore.atvlauncher.event.StorageSaveItemsAction;
import com.dipcore.atvlauncher.helper.ColorHelper;
import com.dipcore.atvlauncher.item.folder.FolderLauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherSections;
import org.greenrobot.eventbus.EventBus;

public class DialogCreateFolder extends Dialog {
    private Button applyButton;
    private View.OnClickListener applyButtonOnClickListener;
    private Button cancelButton;
    private View.OnClickListener cancelButtonOnClickListener;
    private TextView folderNameTextView;
    private LauncherSections launcherSections;

    public DialogCreateFolder(Context context) {
        this(context, 0);
    }

    public DialogCreateFolder(Context context, int themeResId) {
        super(context, themeResId);
        this.cancelButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
            }
        };
        this.applyButtonOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                String name = folderNameTextView.getText().toString();
                if (name != null) {
                    FolderLauncherItemModel folderLauncherItemModel = new FolderLauncherItemModel();
                    folderLauncherItemModel.setTitle(name);
                    folderLauncherItemModel.setBackgroundColor(ColorHelper.getMatColorString(getContext()));
                    launcherSections.addItem(LauncherConstants.AppsSectionUuid, folderLauncherItemModel);
                    EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.AppsSectionUuid));
                    EventBus.getDefault().post(new ItemsAdapterAddItemAction(LauncherConstants.HomeAdapterUuid, folderLauncherItemModel));
                    dismiss();
                }
            }
        };
        this.launcherSections = LauncherApplication.getInstance().getLauncherSections();
        init();
        initCallbacks();
    }

    private void init() {
        requestWindowFeature(1);
        setContentView(R.layout.dialog_create_folder);
        this.folderNameTextView = findViewById(R.id.folder_name_text_view);
        this.cancelButton = findViewById(R.id.cancel_button);
        this.applyButton = findViewById(R.id.apply_button);
    }

    private void initCallbacks() {
        this.cancelButton.setOnClickListener(this.cancelButtonOnClickListener);
        this.applyButton.setOnClickListener(this.applyButtonOnClickListener);
    }
}
