package com.dipcore.atvlauncher;

import android.app.Application;
import android.content.Context;
import android.text.TextUtils;
import android.util.Log;
import com.dipcore.atvlauncher.listener.WallpaperEventListener;
import com.dipcore.atvlauncher.widget.LauncherAppWidget.LauncherAppWidgetHost;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherSections;
import io.paperdb.Paper;
import net.hockeyapp.android.CrashManager;
import net.hockeyapp.android.utils.Util;

public class LauncherApplication extends Application {
    private static final String TAG = LauncherApplication.class.getCanonicalName();
    private static LauncherApplication instance;
    private LauncherAppWidgetHost appWidgetHost;
    LauncherSections launcherSections = new LauncherSections();
    WallpaperEventListener wallpaperEventListener = new WallpaperEventListener(this);
    @Override
    public void onCreate() {
        super.onCreate();
        registerListeners();
        Paper.init(getApplicationContext());
        CrashManager.register(this);
        this.appWidgetHost = new LauncherAppWidgetHost(this, LauncherConstants.APPWIDGET_HOST_ID);
        this.appWidgetHost.startListening();
        instance = this;
    }
    @Override
    public void onTerminate() {
        try {
            this.appWidgetHost.stopListening();
        } catch (NullPointerException ex) {
            Log.w(TAG, "problem while stopping LauncherAppWidgetHost during Launcher destruction", ex);
        }
        this.appWidgetHost = null;
        unregisterListeners();
        super.onTerminate();
    }

    public static LauncherApplication getInstance() {
        return instance;
    }

    public LauncherSections getLauncherSections() {
        return this.launcherSections;
    }

    public LauncherAppWidgetHost getAppWidgetHost() {
        return this.appWidgetHost;
    }

    private void registerListeners() {
        this.wallpaperEventListener.register();
    }

    private void unregisterListeners() {
        this.wallpaperEventListener.unregister();
    }
}
