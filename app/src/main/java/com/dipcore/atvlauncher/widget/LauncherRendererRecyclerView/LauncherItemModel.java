package com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView;

import android.content.Context;
import com.dipcore.atvlauncher.widget.RendererRecyclerView.ItemModel;

public interface LauncherItemModel extends ItemModel {
    public static final boolean INVISIBLE = false;
    public static final boolean VISIBLE = true;

    String getBackgroundColor();

    String getBackgroundImage();

    Context getContext();

    String getSectionUuid();

    String getUuid();

    boolean isVisible();

    void setBackgroundColor(String str);

    void setBackgroundImage(String str);

    void setContext(Context context);

    void setSectionUuid(String str);

    void setUuid(String str);

    void setVisible(boolean z);
}
