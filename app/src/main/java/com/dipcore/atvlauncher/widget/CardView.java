package com.dipcore.atvlauncher.widget;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Bitmap.Config;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Style;
import android.graphics.Rect;
import android.graphics.RectF;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.annotation.ColorRes;
import android.support.annotation.DrawableRes;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.ViewCompat;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView;
import android.widget.ImageView.ScaleType;
import android.widget.RelativeLayout.LayoutParams;
import com.dipcore.atvlauncher.cache.ImageCache;
import com.dipcore.atvlauncher.helper.CompatUtils;
import com.dipcore.atvlauncher.widget.RatioLayout.RatioRelativeLayout;
import com.makeramen.roundedimageview.RoundedImageView;

public class CardView extends RatioRelativeLayout {
    private static final int ANIMATION_DURATION = 150;
    private static final float DEFAULT_CORNER_RADIUS = 3.0f;
    private static final String DEFAULT_OVERLAY_LAYER_COLOR = "#30000000";
    private static final String DEFAULT_SHADOW_COLOR = "#20000000";
    private static final float DEFAULT_SHADOW_RADIUS = 5.0f;
    private static final String TAG = CardView.class.getCanonicalName();
    private int backgroundColor;
    private String backgroundImage;
    private RoundedImageView backgroundRoundedImageView;
    private float cornerRadius;
    private boolean forceInvalidateShadow;
    private ImageCache imageCache;
    private String itemUuid;
    private int overlayLayerColor;
    private RoundedImageView overlayRoundedImageView;
    private int shadowColor;
    private ImageView shadowImageView;
    private float shadowRadius;

    public CardView(Context context) {
        this(context, null);
    }

    public CardView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CardView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.cornerRadius = DEFAULT_CORNER_RADIUS;
        this.shadowRadius = DEFAULT_SHADOW_RADIUS;
        this.overlayLayerColor = Color.parseColor(DEFAULT_OVERLAY_LAYER_COLOR);
        this.shadowColor = Color.parseColor(DEFAULT_SHADOW_COLOR);
        this.forceInvalidateShadow = false;
        init();
    }

    protected void onAttachedToWindow() {
        super.onAttachedToWindow();
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
    }

    protected void onFocusChanged(boolean gainFocus, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
        if (gainFocus) {
            focusIn();
        } else {
            focusOut();
        }
    }

    protected void onSizeChanged(int w, int h, int oldw, int oldh) {
        super.onSizeChanged(w, h, oldw, oldh);
        if (w > 0 && h > 0) {
            if (getBackground() == null || this.forceInvalidateShadow) {
                this.forceInvalidateShadow = false;
                generateShadowLayer(w, h);
            }
        }
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        super.onLayout(changed, left, top, right, bottom);
        if (this.forceInvalidateShadow) {
            this.forceInvalidateShadow = false;
            generateShadowLayer(right - left, bottom - top);
        }
    }

    public String getItemUuid() {
        return this.itemUuid;
    }

    public void setItemUuid(String itemUuid) {
        this.itemUuid = itemUuid;
    }

    public void setCornerRadius(float radius) {
        this.cornerRadius = (float) CompatUtils.dp2px(getContext(), radius);
        this.backgroundRoundedImageView.setCornerRadius(this.cornerRadius);
        if (this.overlayRoundedImageView != null) {
            this.overlayRoundedImageView.setCornerRadius(this.cornerRadius);
        }
    }

    public void useOverlayLayer(boolean flag) {
        if (flag && this.overlayRoundedImageView == null) {
            this.overlayRoundedImageView = new RoundedImageView(getContext());
            initRoundedView(this.overlayRoundedImageView);
            setOverlayColor(this.overlayLayerColor);
            this.overlayRoundedImageView.setCornerRadius(this.cornerRadius);
            addView(this.overlayRoundedImageView);
        }
        if (!flag && this.overlayRoundedImageView != null) {
            removeView(this.overlayRoundedImageView);
            this.overlayRoundedImageView = null;
        }
    }

    public void setOverlayColor(int color) {
        ShapeDrawable shapeDrawable = new ShapeDrawable();
        shapeDrawable.getPaint().setColor(color);
        setOverlayImageDrawable(shapeDrawable);
    }

    public void setOverlayImageDrawable(Drawable drawable) {
        this.overlayRoundedImageView.setImageDrawable(drawable);
    }

    public void setOverlayImageResource(@DrawableRes int resId) {
        this.overlayRoundedImageView.setImageResource(resId);
    }

    public void useShadowLayer(boolean flag) {
        if (flag && this.shadowImageView == null) {
            this.shadowImageView = new ImageView(getContext());
            this.shadowImageView.setLayoutParams(new LayoutParams(-1, -1));
            this.shadowImageView.setAlpha(0.5f);
            addView(this.shadowImageView, 0);
            int p = ((int) this.shadowRadius) * 2;
            this.backgroundRoundedImageView.setPadding(p, p, p, p);
            if (this.overlayRoundedImageView != null) {
                this.overlayRoundedImageView.setPadding(p, p, p, p);
            }
        }
        if (!flag && this.shadowImageView != null) {
            removeView(this.shadowImageView);
            this.shadowImageView = null;
        }
    }

    public void setShadowColor(String color) {
        this.shadowColor = Color.parseColor(color);
        invalidateShadow();
    }

    public void setShadowColor(int color) {
        this.shadowColor = color;
        invalidateShadow();
    }

    public void setShadowColorResource(@ColorRes int resId) {
        this.shadowColor = ContextCompat.getColor(getContext(), resId);
        invalidateShadow();
    }

    public void setShadowRadius(float dp) {
        this.shadowRadius = (float) CompatUtils.dp2px(getContext(), dp);
        invalidateShadow();
    }

    public void invalidateShadow() {
        this.forceInvalidateShadow = true;
        requestLayout();
        invalidate();
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        if (this.backgroundImage == null && backgroundColor == 0) {
            this.backgroundRoundedImageView.setVisibility(View.GONE);
        }
        if (backgroundColor != 0) {
            ShapeDrawable shapeDrawable = new ShapeDrawable();
            shapeDrawable.getPaint().setColor(backgroundColor);
            this.backgroundRoundedImageView.setImageDrawable(shapeDrawable);
            this.backgroundRoundedImageView.setVisibility(View.VISIBLE);
        }
    }

    public void setBackgroundColor(String color) {
        setBackgroundColor(Color.parseColor(color));
    }

    public void setBackgroundImageDrawable(Drawable drawable) {
        this.backgroundRoundedImageView.setImageDrawable(drawable);
    }

    public void setBackgroundImageResource(@DrawableRes int resId) {
        this.backgroundRoundedImageView.setImageResource(resId);
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
        if (backgroundImage == null && this.backgroundColor == 0) {
            this.backgroundRoundedImageView.setVisibility(View.GONE);
        }
        if (backgroundImage != null) {
            setBackgroundImageDrawable(Drawable.createFromPath(backgroundImage));
            this.backgroundRoundedImageView.setVisibility(View.VISIBLE);
        }
    }

    public void resetBackground() {
        this.backgroundImage = null;
        this.backgroundColor = 0;
        this.backgroundRoundedImageView.setVisibility(View.GONE);
    }

    private void init() {
        setDuplicateParentStateEnabled(true);
        setClickable(true);
        setFocusable(true);
        this.imageCache = ImageCache.getInstance();
        this.backgroundRoundedImageView = new RoundedImageView(getContext());
        initRoundedView(this.backgroundRoundedImageView);
        addView(this.backgroundRoundedImageView);
    }

    private void initRoundedView(RoundedImageView view) {
        view.setLayoutParams(new ViewGroup.LayoutParams(-1, -1));
        view.setScaleType(ScaleType.CENTER_CROP);
        view.setOval(false);
        view.setTileModeX(TileMode.CLAMP);
        view.setTileModeY(TileMode.CLAMP);
    }

    private void focusOut() {
        animate().scaleX(1.0f).scaleY(1.0f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
        if (this.overlayRoundedImageView != null) {
            this.overlayRoundedImageView.animate().alpha(1.0f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
        }
        if (this.shadowImageView != null) {
            this.shadowImageView.animate().alpha(0.5f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
        }
        ViewCompat.setElevation(this, 0.0f);
    }

    private void focusIn() {
        animate().scaleX(1.2f).scaleY(1.2f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
        if (this.overlayRoundedImageView != null) {
            this.overlayRoundedImageView.animate().alpha(0.0f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
        }
        if (this.shadowImageView != null) {
            this.shadowImageView.animate().alpha(1.0f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
        }
        ViewCompat.setElevation(this, 1.0f);
    }

    private Bitmap createShadowBitmap(int shadowWidth, int shadowHeight, float cornerRadius, float shadowRadius, float dx, float dy, int shadowColor, int fillColor) {
        Bitmap output = Bitmap.createBitmap(shadowWidth, shadowHeight, Config.ALPHA_8);
        Canvas canvas = new Canvas(output);
        RectF shadowRect = new RectF(shadowRadius, shadowRadius, ((float) shadowWidth) - shadowRadius, ((float) shadowHeight) - shadowRadius);
        if (dy > 0.0f) {
            shadowRect.top += dy;
            shadowRect.bottom -= dy;
        } else if (dy < 0.0f) {
            shadowRect.top += Math.abs(dy);
            shadowRect.bottom -= Math.abs(dy);
        }
        if (dx > 0.0f) {
            shadowRect.left += dx;
            shadowRect.right -= dx;
        } else if (dx < 0.0f) {
            shadowRect.left += Math.abs(dx);
            shadowRect.right -= Math.abs(dx);
        }
        Paint shadowPaint = new Paint();
        shadowPaint.setAntiAlias(true);
        shadowPaint.setColor(fillColor);
        shadowPaint.setStyle(Style.FILL);
        if (!isInEditMode()) {
            shadowPaint.setShadowLayer(shadowRadius, dx, dy, shadowColor);
        }
        canvas.drawRoundRect(shadowRect, cornerRadius, cornerRadius, shadowPaint);
        return output;
    }

    private void generateShadowLayer(int w, int h) {
        if (this.shadowImageView != null) {
            String shadowBitmapKey = "card_view_shadow" + String.valueOf(w) + "x" + String.valueOf(h);
            Bitmap shadowBitmap = this.imageCache.get(shadowBitmapKey);
            if (shadowBitmap == null) {
                Log.d(TAG, "Create shadow bitmap");
                shadowBitmap = createShadowBitmap(w, h, this.cornerRadius, this.shadowRadius, 0.0f, 0.0f, this.shadowColor, 0);
                this.imageCache.put(shadowBitmapKey, shadowBitmap);
            }
            this.shadowImageView.setImageDrawable(new BitmapDrawable(getResources(), shadowBitmap));
            this.shadowImageView.setScaleType(ScaleType.FIT_XY);
            int p = ((int) this.shadowRadius) * 2;
            this.backgroundRoundedImageView.setPadding(p, p, p, p);
            if (this.overlayRoundedImageView != null) {
                this.overlayRoundedImageView.setPadding(p, p, p, p);
            }
        }
    }
}
