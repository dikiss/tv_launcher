package com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView;

import com.dipcore.atvlauncher.item.folder.FolderLauncherItemModel;
import com.dipcore.atvlauncher.widget.RendererRecyclerView.ItemModel;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;

public class LauncherSections {
    ArrayList<LauncherSection> launcherSections = new ArrayList();

    public void addSection(LauncherSection launcherSection) {
        this.launcherSections.add(launcherSection);
    }

    public void addSection(String uuid, ArrayList<LauncherItemModel> items) {
        addSection(new LauncherSection(uuid, (ArrayList) items));
    }

    public void addSection(String uuid, String title, ArrayList<LauncherItemModel> items) {
        addSection(new LauncherSection(uuid, title, items));
    }

    public LauncherSection getSection(String uuid) {
        Iterator it = this.launcherSections.iterator();
        while (it.hasNext()) {
            LauncherSection launcherSection = (LauncherSection) it.next();
            if (launcherSection.getUuid().equals(uuid)) {
                return launcherSection;
            }
        }
        return null;
    }

    public void setSection(String uuid, String title, ArrayList<LauncherItemModel> items) {
        removeSection(uuid);
        addSection(new LauncherSection(uuid, title, items));
    }

    public void removeSection(String uuid) {
        this.launcherSections.remove(getSection(uuid));
    }

    public void removeItem(String sectionUuid, LauncherItemModel item) {
        getSection(sectionUuid).removeItem(item);
    }

    public void removeItem(String sectionUuid, String uuid) {
        getSection(sectionUuid).removeItem(uuid);
    }

    public void removeItemAbsolute(String uuid, int position) {
        getSection(uuid).removeItem(position - getSectionContentOffset(uuid));
    }

    public void addItem(String sectionUuid, LauncherItemModel item) {
        getSection(sectionUuid).addItem(item);
    }

    public void addItem(String uuid, int position, LauncherItemModel item) {
        getSection(uuid).addItem(position, item);
    }

    public void moveItem(String fromUuid, String toUuid) {
        LauncherItemModel fromItem = getItemByUuid(fromUuid);
        LauncherItemModel toItem = getItemByUuid(toUuid);
        String fromSectionUuid = fromItem.getSectionUuid();
        String toSectionUuid = toItem.getSectionUuid();
        int fromPosition = getSection(fromItem.getSectionUuid()).getPosition(fromUuid);
        int toPosition = getSection(fromItem.getSectionUuid()).getPosition(toUuid);
        if (fromSectionUuid.equals(toSectionUuid)) {
            getSection(fromSectionUuid).moveItem(fromPosition, toPosition);
            return;
        }
        getSection(fromSectionUuid).removeItem(fromPosition);
        getSection(toSectionUuid).addItem(toPosition, toItem);
    }

    public void moveItem(String uuid, int from, int to) {
        getSection(uuid).moveItem(from, to);
    }

    public void moveItem(String uuid, int from, int to, boolean visibility) {
        getSection(uuid).moveItem(from, to, visibility);
    }

    public void moveItemAbsolute(String uuid, int from, int to, boolean visibility) {
        int sectionOffset = getSectionContentOffset(uuid);
        moveItem(uuid, from - sectionOffset, to - sectionOffset, visibility);
    }

    public ArrayList<LauncherSection> getLauncherSections() {
        return this.launcherSections;
    }

    public ArrayList<LauncherItemModel> getSectionItems(String uuid) {
        if (getSection(uuid) != null) {
            return getSection(uuid).getItems();
        }
        return null;
    }

    public int getSectionSize(String uuid) {
        return getSection(uuid).getSize();
    }

    public int getSectionOffset(String uuid) {
        int offset = 0;
        Iterator it = this.launcherSections.iterator();
        while (it.hasNext()) {
            LauncherSection launcherSection = (LauncherSection) it.next();
            if (launcherSection.getUuid().equals(uuid)) {
                return offset;
            }
            offset += launcherSection.getSize();
        }
        return -1;
    }

    public int getSectionContentOffset(String uuid) {
        int offset = getSectionOffset(uuid);
        if (getSection(uuid).getTitle() != null) {
            return offset + 1;
        }
        return offset;
    }

    public void setItemVisibility(String uuid, int position, boolean visibility) {
        getSection(uuid).setItemVisibility(position, visibility);
    }

    public int getCountBefore(String uuid, int position, boolean visibility) {
        return getSection(uuid).getCountBefore(position, visibility);
    }

    public ArrayList<ItemModel> flatten() {
        ArrayList<ItemModel> result = new ArrayList();
        Iterator it = this.launcherSections.iterator();
        while (it.hasNext()) {
            LauncherSection launcherSection = (LauncherSection) it.next();
            if (launcherSection.getTitle() != null) {
                result.add(launcherSection.getTitle());
            }
            result.addAll(launcherSection.getItems());
        }
        return result;
    }

    public ArrayList<ItemModel> flatten(boolean visibility) {
        ArrayList<ItemModel> result = new ArrayList();
        Iterator it = this.launcherSections.iterator();
        while (it.hasNext()) {
            LauncherSection launcherSection = (LauncherSection) it.next();
            if (launcherSection.getTitle() != null) {
                result.add(launcherSection.getTitle());
            }
            result.addAll(launcherSection.getItems(visibility));
        }
        return result;
    }

    public void sort() {
        Collections.sort(this.launcherSections);
    }

    public LauncherItemModel getItemByUuid(String uuid) {
        Iterator it = this.launcherSections.iterator();
        while (it.hasNext()) {
            LauncherItemModel item = ((LauncherSection) it.next()).getItem(uuid);
            if (item != null) {
                return item;
            }
        }
        return null;
    }

    public void removeItemByUuid(String uuid) {
        Iterator it = this.launcherSections.iterator();
        while (it.hasNext()) {
            ((LauncherSection) it.next()).removeItem(uuid);
        }
    }

    public ArrayList<FolderLauncherItemModel> getFolders() {
        ArrayList<FolderLauncherItemModel> result = new ArrayList();
        Iterator it = flatten().iterator();
        while (it.hasNext()) {
            ItemModel item = (ItemModel) it.next();
            if (item instanceof FolderLauncherItemModel) {
                result.add((FolderLauncherItemModel) item);
            }
        }
        return result;
    }

    public FolderLauncherItemModel getFolderByItemUuid(String uuid) {
        Iterator it = getFolders().iterator();
        while (it.hasNext()) {
            FolderLauncherItemModel folder = (FolderLauncherItemModel) it.next();
            Iterator it2 = folder.getItems().iterator();
            while (it2.hasNext()) {
                if (((LauncherItemModel) it2.next()).getUuid().equals(uuid)) {
                    return folder;
                }
            }
        }
        return null;
    }
}
