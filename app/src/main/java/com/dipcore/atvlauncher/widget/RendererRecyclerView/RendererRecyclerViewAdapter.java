package com.dipcore.atvlauncher.widget.RendererRecyclerView;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;

public class RendererRecyclerViewAdapter extends Adapter {
    @NonNull
    protected final ArrayList<ItemModel> mItems = new ArrayList<>();
    @NonNull
    protected final ArrayList<ViewRenderer> mRenderers = new ArrayList<>();
    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Iterator it = this.mRenderers.iterator();
        while (it.hasNext()) {
            ViewRenderer renderer = (ViewRenderer) it.next();
            if (renderer.isSupportType(viewType)) {
                return renderer.createViewHolder(parent);
            }
        }
        throw new RuntimeException("Not supported Item View Type: " + viewType);
    }

    public void registerRenderer(@NonNull ViewRenderer renderer) {
        this.mRenderers.add(renderer);
    }
    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemModel item = getItem(position);
        Iterator it = this.mRenderers.iterator();
        while (it.hasNext()) {
            ViewRenderer renderer = (ViewRenderer) it.next();
            if (renderer.isSupportType(item.getType())) {
                renderer.bindView(item, holder, position);
                return;
            }
        }
        throw new RuntimeException("Not supported View Holder: " + holder);
    }
    @Override
    public int getItemViewType(int position) {
        return getItem(position).getType();
    }

    @NonNull
    ItemModel getItem(int position) {
        return (ItemModel) this.mItems.get(position);
    }
    @Override
    public int getItemCount() {
        return this.mItems.size();
    }

    public ArrayList<ItemModel> getItems() {
        return this.mItems;
    }

    public void setItems(@NonNull ArrayList<ItemModel> items) {
        this.mItems.clear();
        this.mItems.addAll(items);
    }
}
