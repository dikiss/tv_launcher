package com.dipcore.atvlauncher.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.net.wifi.WifiInfo;
import android.net.wifi.WifiManager;
import android.os.StrictMode;
import android.os.StrictMode.ThreadPolicy.Builder;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageView;
import com.dipcore.atvlauncher.R;
import java.net.InetAddress;

public class NetworkIconView extends ImageView {
    private static final long ONLINE_CHECKING_MIN_INTERVAL = 900000;
    private static final String TAG = NetworkIconView.class.getCanonicalName();
    private int activeNetworkType;
    private BroadcastReceiver connReceiver;
    private Context context;
    private boolean isConnected;
    private boolean isOnline;
    private long isOnlineLastCheckingTime;
    private BroadcastReceiver rssiReceiver;

    public NetworkIconView(Context context) {
        this(context, null);
    }

    public NetworkIconView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NetworkIconView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.isOnline = false;
        this.isOnlineLastCheckingTime = 0;
        this.rssiReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(NetworkIconView.TAG, "rssiReceiver ");
                NetworkIconView.this.checkConnectionType();
                if (NetworkIconView.this.isConnected && NetworkIconView.this.activeNetworkType == 1) {
                    WifiInfo wifiInfo = ((WifiManager) context.getSystemService(Context.WIFI_SERVICE)).getConnectionInfo();
                    if (wifiInfo.getBSSID() != null) {
                        int signalLevel = WifiManager.calculateSignalLevel(wifiInfo.getRssi(), 4) + 1;
                        NetworkIconView.this.setImageResource(context.getResources().getIdentifier((NetworkIconView.this.isOnline ? "ic_home_wifi_" : "ic_home_wifi_disconnect_") + signalLevel, "drawable", context.getPackageName()));
                        Log.d(NetworkIconView.TAG, "rssiReceiver signalLevel " + signalLevel);
                        Log.d(NetworkIconView.TAG, "rssiReceiver isOnline " + NetworkIconView.this.isOnline);
                    }
                }
            }
        };
        this.connReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(NetworkIconView.TAG, "connReceiver ");
                NetworkIconView.this.checkConnectionType();
            }
        };
        this.context = context;
    }
    @Override
    protected void onAttachedToWindow() {
        this.context.registerReceiver(this.rssiReceiver, new IntentFilter("android.net.wifi.RSSI_CHANGED"));
        this.context.registerReceiver(this.connReceiver, new IntentFilter("android.net.conn.CONNECTIVITY_CHANGE"));
        StrictMode.setThreadPolicy(new Builder().permitAll().build());
        checkConnectionType();
        super.onAttachedToWindow();
    }
    @Override
    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.context.unregisterReceiver(this.rssiReceiver);
        this.context.unregisterReceiver(this.connReceiver);
    }

    private void checkConnectionType() {
        if (!isInEditMode()) {
            NetworkInfo activeNetworkInfo = ((ConnectivityManager) this.context.getSystemService(Context.CONNECTIVITY_SERVICE)).getActiveNetworkInfo();
            boolean z = activeNetworkInfo != null && activeNetworkInfo.isConnectedOrConnecting();
            this.isConnected = z;
            if (this.isConnected) {
                this.isOnline = isOnline();
                Log.d(TAG, "isOnline " + this.isOnline);
                this.activeNetworkType = activeNetworkInfo.getType();
                Log.d(TAG, "activeNetworkType " + this.activeNetworkType);
                switch (this.activeNetworkType) {
                    case 1:
                        setImageResource(this.isOnline ? R.drawable.ic_home_wifi_4 : R.drawable.ic_home_wifi_disconnect_4);
                        return;
                    case 9:
                        setImageResource(this.isOnline ? R.drawable.ic_home_ethernet_connected : R.drawable.ic_home_ethernet_disconnected);
                        return;
                    default:
                        return;
                }
            }
            setImageResource(R.drawable.ic_home_ethernet_disconnected);
        }
    }

    private boolean isOnline() {
        if (System.currentTimeMillis() - this.isOnlineLastCheckingTime <= ONLINE_CHECKING_MIN_INTERVAL) {
            return this.isOnline;
        }
        Log.d(TAG, "Check online status");
        this.isOnlineLastCheckingTime = System.currentTimeMillis();
        try {
            InetAddress.getByName("google.com").isReachable(300);
            return true;
        } catch (Exception e) {
            return false;
        }
    }
}
