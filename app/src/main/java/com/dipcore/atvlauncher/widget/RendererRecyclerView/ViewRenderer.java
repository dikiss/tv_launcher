package com.dipcore.atvlauncher.widget.RendererRecyclerView;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;

public abstract class ViewRenderer<M extends ItemModel, VH extends ViewHolder> {
    @NonNull
    private final Context mContext;
    private final int mViewType;

    public abstract void bindView(@NonNull M m, @NonNull VH vh, @NonNull int i);

    @NonNull
    public abstract VH createViewHolder(@Nullable ViewGroup viewGroup);

    public ViewRenderer(int viewType, @NonNull Context context) {
        this.mViewType = viewType;
        this.mContext = context;
    }

    public boolean isSupportType(int viewType) {
        return this.mViewType == viewType;
    }

    @NonNull
    public Context getContext() {
        return this.mContext;
    }
}
