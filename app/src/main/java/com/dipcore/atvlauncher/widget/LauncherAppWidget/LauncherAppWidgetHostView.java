package com.dipcore.atvlauncher.widget.LauncherAppWidget;

import android.appwidget.AppWidgetHostView;
import android.appwidget.AppWidgetProviderInfo;
import android.content.Context;
import android.content.res.Resources;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout.LayoutParams;
import android.widget.RemoteViews;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.helper.CheckLongPressHelper;

public class LauncherAppWidgetHostView extends AppWidgetHostView {
    int height = 0;
    private Context mContext;
    private LayoutInflater mInflater;
    private CheckLongPressHelper mLongPressHelper;
    private int mPreviousOrientation;
    private Runnable onLayoutRunnable = new Runnable() {
        @Override
        public void run() {
            LauncherAppWidgetHostView.this.updateAppWidget(new RemoteViews(LauncherAppWidgetHostView.this.getAppWidgetInfo().provider.getPackageName(), 0));
        }
    };
    private final int paddingBottom;
    private final int paddingLeft;
    private final int paddingRight;
    private final int paddingTop;
    private LayoutParams params;
    private int scale = 0;
    private View view;
    int width = 0;

    public LauncherAppWidgetHostView(Context context) {
        super(context);
        this.mContext = context;
        this.mLongPressHelper = new CheckLongPressHelper(this);
        this.mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        Resources r = getContext().getResources();
        this.paddingLeft = r.getDimensionPixelSize(R.dimen.app_widget_padding_left);
        this.paddingRight = r.getDimensionPixelSize(R.dimen.app_widget_padding_right);
        this.paddingTop = r.getDimensionPixelSize(R.dimen.app_widget_padding_top);
        this.paddingBottom = r.getDimensionPixelSize(R.dimen.app_widget_padding_bottom);
    }

    protected View getErrorView() {
        return this.mInflater.inflate(R.layout.app_widget_error, this, false);
    }

    public void updateAppWidget(RemoteViews remoteViews) {
        this.mPreviousOrientation = this.mContext.getResources().getConfiguration().orientation;
        super.updateAppWidget(remoteViews);
    }

    public boolean orientationChangedSincedInflation() {
        if (this.mPreviousOrientation != this.mContext.getResources().getConfiguration().orientation) {
            return true;
        }
        return false;
    }

    public boolean onInterceptTouchEvent(MotionEvent ev) {
        if (this.mLongPressHelper.hasPerformedLongPress()) {
            this.mLongPressHelper.cancelLongPress();
            return true;
        }
        switch (ev.getAction()) {
            case 0:
                this.mLongPressHelper.postCheckForLongPress();
                break;
            case 1:
            case 3:
                this.mLongPressHelper.cancelLongPress();
                break;
        }
        return false;
    }

    public void cancelLongPress() {
        super.cancelLongPress();
        this.mLongPressHelper.cancelLongPress();
    }

    public int getDescendantFocusability() {
        return ViewGroup.FOCUS_BLOCK_DESCENDANTS;
    }
    @Override
    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        try {
            this.width = ((right - left) - this.paddingLeft) - this.paddingRight;
            this.height = ((bottom - top) - this.paddingTop) - this.paddingBottom;
            scaleAppWidgetView();
            super.onLayout(changed, left, top, right, bottom);
        } catch (RuntimeException e) {
            post(this.onLayoutRunnable);
        }
    }

    public int getScale() {
        return this.scale;
    }

    public void setScale(int scale) {
        this.scale = scale;
        scaleAppWidgetView();
        super.forceLayout();
    }

    private void scaleAppWidgetView() {
        if (this.width != 0 && this.height != 0 && this.scale > 0 && this.view != null) {
            int scaledHeight;
            float scale = ((float) this.scale) / 100.0f;
            int scaledWidth = (int) (((float) this.width) / scale);
            if (this.params.height == -1) {
                scaledHeight = (int) (((float) this.height) / scale);
            } else {
                scaledHeight = this.params.height;
            }
            this.view.setScaleX(scale);
            this.view.setScaleY(scale);
            this.view.setLayoutParams(new LayoutParams(scaledWidth, scaledHeight, 17));
        }
    }

    protected void prepareView(View view) {
        super.prepareView(view);
        this.view = view;
        this.params = (LayoutParams) view.getLayoutParams();
    }

    public void setAppWidget(int appWidgetId, AppWidgetProviderInfo info) {
        super.setAppWidget(appWidgetId, info);
        setPadding(this.paddingLeft, this.paddingTop, this.paddingRight, this.paddingBottom);
    }
}
