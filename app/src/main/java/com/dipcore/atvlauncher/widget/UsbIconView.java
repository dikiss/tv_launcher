package com.dipcore.atvlauncher.widget;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import com.dipcore.atvlauncher.R;

public class UsbIconView extends ImageView {
    private static final String TAG = UsbIconView.class.getCanonicalName();
    private final Context context;
    private BroadcastReceiver hardwareUsbReceiver;
    private BroadcastReceiver usbReceiver;

    public UsbIconView(Context context) {
        this(context, null);
    }

    public UsbIconView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public UsbIconView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.usbReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(UsbIconView.TAG, "usbReceiver action " + intent.getAction());
                String action = intent.getAction();
                int i = -1;
                switch (action.hashCode()) {
                    case -1514214344:
                        if (action.equals("android.intent.action.MEDIA_MOUNTED")) {
                            i = 1;
                            break;
                        }
                        break;
                    case 257177710:
                        if (action.equals("android.intent.action.MEDIA_NOFS")) {
                            i = 3;
                            break;
                        }
                        break;
                    case 1431947322:
                        if (action.equals("android.intent.action.MEDIA_UNMOUNTABLE")) {
                            i = 2;
                            break;
                        }
                        break;
                    case 1964681210:
                        if (action.equals("android.intent.action.MEDIA_CHECKING")) {
                            i = 0;
                            break;
                        }
                        break;
                }
                switch (i) {
                    case 0:
                        UsbIconView.this.setImageResource(R.drawable.ic_home_usb_checking);
                        UsbIconView.this.setVisibility(View.VISIBLE);
                        return;
                    case 1:
                        UsbIconView.this.setImageResource(R.drawable.ic_home_usb_mounted);
                        UsbIconView.this.setVisibility(View.VISIBLE);
                        return;
                    case 2:
                    case 3:
                        UsbIconView.this.setImageResource(R.drawable.ic_home_usb_error);
                        UsbIconView.this.setVisibility(View.VISIBLE);
                        return;
                    default:
                        UsbIconView.this.setVisibility(View.GONE);
                        return;
                }
            }
        };
        this.hardwareUsbReceiver = new BroadcastReceiver() {
            @Override
            public void onReceive(Context context, Intent intent) {
                Log.d(UsbIconView.TAG, "hardwareUsbReceiver action " + intent.getAction());
                if (intent.getAction().equals("android.hardware.usb.action.USB_DEVICE_ATTACHED")) {
                    UsbIconView.this.setImageResource(R.drawable.ic_home_usb_connected);
                    UsbIconView.this.setVisibility(View.VISIBLE);
                }
                if (intent.getAction().equals("android.hardware.usb.action.USB_DEVICE_DETACHED")) {
                    UsbIconView.this.setVisibility(View.GONE);
                }
            }
        };
        this.context = context;
    }

    protected void onAttachedToWindow() {
        IntentFilter intentFilter = new IntentFilter();
        intentFilter.addAction("android.intent.action.MEDIA_CHECKING");
        intentFilter.addAction("android.intent.action.MEDIA_MOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTABLE");
        intentFilter.addAction("android.intent.action.MEDIA_NOFS");
        intentFilter.addAction("android.intent.action.MEDIA_EJECT");
        intentFilter.addAction("android.intent.action.MEDIA_UNMOUNTED");
        intentFilter.addAction("android.intent.action.MEDIA_REMOVED");
        intentFilter.addAction("android.intent.action.MEDIA_BAD_REMOVAL");
        intentFilter.addDataScheme("file");
        this.context.registerReceiver(this.usbReceiver, intentFilter);
        IntentFilter intentFilter2 = new IntentFilter();
        intentFilter2.addAction("android.hardware.usb.action.USB_DEVICE_ATTACHED");
        intentFilter2.addAction("android.hardware.usb.action.USB_DEVICE_DETACHED");
        this.context.registerReceiver(this.hardwareUsbReceiver, intentFilter2);
        setVisibility(View.GONE);
        super.onAttachedToWindow();
    }

    protected void onDetachedFromWindow() {
        super.onDetachedFromWindow();
        this.context.unregisterReceiver(this.usbReceiver);
        this.context.unregisterReceiver(this.hardwareUsbReceiver);
    }
}
