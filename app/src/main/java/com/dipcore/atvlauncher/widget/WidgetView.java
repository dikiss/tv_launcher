package com.dipcore.atvlauncher.widget;

import android.content.Context;
import android.graphics.Color;
import android.graphics.Rect;
import android.graphics.Shader.TileMode;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.ShapeDrawable;
import android.support.annotation.DrawableRes;
import android.util.AttributeSet;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.animation.DecelerateInterpolator;
import android.widget.ImageView.ScaleType;
import com.dipcore.atvlauncher.helper.CompatUtils;
import com.dipcore.atvlauncher.widget.LauncherAppWidget.LauncherAppWidgetHostView;
import com.dipcore.atvlauncher.widget.RatioLayout.RatioRelativeLayout;
import com.makeramen.roundedimageview.RoundedImageView;

public class WidgetView extends RatioRelativeLayout {
    private static final int ANIMATION_DURATION = 150;
    private int backgroundColor;
    private String backgroundImage;
    private RoundedImageView backgroundRoundedImageView;
    private int cornerRadius;
    private int height;
    private LauncherAppWidgetHostView launcherAppWidgetHostView;
    private RoundedImageView overlayRoundedImageView;
    private int width;

    public WidgetView(Context context) {
        this(context, null);
    }

    public WidgetView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public WidgetView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        init();
        initViews();
    }
    @Override
    protected void onFocusChanged(boolean gainFocus, int direction, Rect previouslyFocusedRect) {
        super.onFocusChanged(gainFocus, direction, previouslyFocusedRect);
        if (gainFocus) {
            focusIn();
        } else {
            focusOut();
        }
    }

    public LauncherAppWidgetHostView getLauncherAppWidgetHostView() {
        return this.launcherAppWidgetHostView;
    }

    public void setAppWidgetHostView(LauncherAppWidgetHostView launcherAppWidgetHostView) {
        if (launcherAppWidgetHostView.getParent() != null) {
            ((ViewGroup) launcherAppWidgetHostView.getParent()).removeView(launcherAppWidgetHostView);
        }
        removeView(this.launcherAppWidgetHostView);
        this.launcherAppWidgetHostView = launcherAppWidgetHostView;
        addView(this.launcherAppWidgetHostView);
        this.overlayRoundedImageView.setVisibility(View.GONE);
    }

    public void removeAppWidgetHostView() {
        removeView(this.launcherAppWidgetHostView);
        this.launcherAppWidgetHostView = null;
        this.overlayRoundedImageView.setVisibility(View.VISIBLE);
    }

    public void setAppWidgetScaleFactor(int scaleFactor) {
        this.launcherAppWidgetHostView.setScale(scaleFactor);
    }

    public void setCornerRadius(float radius) {
        this.cornerRadius = CompatUtils.dp2px(getContext(), radius);
        if (this.overlayRoundedImageView != null) {
            this.overlayRoundedImageView.setCornerRadius((float) this.cornerRadius);
        }
        if (this.backgroundRoundedImageView != null) {
            this.backgroundRoundedImageView.setCornerRadius((float) this.cornerRadius);
        }
    }

    public void setOverlayColor(int color) {
        ShapeDrawable shapeDrawable = new ShapeDrawable();
        shapeDrawable.getPaint().setColor(color);
        setOverlayImageDrawable(shapeDrawable);
    }

    public void setOverlayImageDrawable(Drawable drawable) {
        this.overlayRoundedImageView.setImageDrawable(drawable);
    }

    public void setOverlayImageResource(@DrawableRes int resId) {
        this.overlayRoundedImageView.setImageResource(resId);
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
        if (this.backgroundImage == null && backgroundColor == 0) {
            this.backgroundRoundedImageView.setVisibility(View.GONE);
        }
        if (backgroundColor != 0) {
            ShapeDrawable shapeDrawable = new ShapeDrawable();
            shapeDrawable.getPaint().setColor(backgroundColor);
            this.backgroundRoundedImageView.setImageDrawable(shapeDrawable);
            this.backgroundRoundedImageView.setVisibility(View.VISIBLE);
        }
    }

    public void setBackgroundColor(String color) {
        if (color != null) {
            setBackgroundColor(Color.parseColor(color));
        } else {
            setBackgroundColor(0);
        }
    }

    public void setBackground(Drawable drawable) {
        this.backgroundRoundedImageView.setImageDrawable(drawable);
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
        if (backgroundImage == null && this.backgroundColor == 0) {
            this.backgroundRoundedImageView.setVisibility(View.GONE);
        }
        if (backgroundImage != null) {
            setBackground(Drawable.createFromPath(backgroundImage));
            this.backgroundRoundedImageView.setVisibility(View.VISIBLE);
        }
    }

    public void resetBackground() {
        this.backgroundImage = null;
        this.backgroundColor = 0;
        this.backgroundRoundedImageView.setVisibility(View.GONE);
    }

    private void init() {
        setFocusable(true);
        setDuplicateParentStateEnabled(true);
    }

    private void initViews() {
        this.overlayRoundedImageView = new RoundedImageView(getContext());
        this.overlayRoundedImageView.setLayoutParams(new LayoutParams(-1, -1));
        this.overlayRoundedImageView.setScaleType(ScaleType.CENTER_CROP);
        this.overlayRoundedImageView.setOval(false);
        this.overlayRoundedImageView.setTileModeX(TileMode.CLAMP);
        this.overlayRoundedImageView.setTileModeY(TileMode.CLAMP);
        this.overlayRoundedImageView.setAlpha(0.0f);
        this.backgroundRoundedImageView = new RoundedImageView(getContext());
        this.backgroundRoundedImageView.setLayoutParams(new LayoutParams(-1, -1));
        this.backgroundRoundedImageView.setScaleType(ScaleType.CENTER_CROP);
        this.backgroundRoundedImageView.setOval(false);
        this.backgroundRoundedImageView.setTileModeX(TileMode.CLAMP);
        this.backgroundRoundedImageView.setTileModeY(TileMode.CLAMP);
        this.backgroundRoundedImageView.setVisibility(View.GONE);
        addView(this.overlayRoundedImageView);
        addView(this.backgroundRoundedImageView);
    }

    private void focusOut() {
        animate().scaleX(1.0f).scaleY(1.0f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
        if (this.overlayRoundedImageView != null) {
            this.overlayRoundedImageView.animate().alpha(0.0f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
            this.overlayRoundedImageView.setAlpha(0.0f);
        }
    }

    private void focusIn() {
        animate().scaleX(1.1f).scaleY(1.1f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
        if (this.overlayRoundedImageView != null) {
            this.overlayRoundedImageView.setAlpha(1.0f);
            this.overlayRoundedImageView.animate().alpha(1.0f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
        }
    }
}
