package com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView;

import com.dipcore.atvlauncher.item.folder.FolderLauncherItemModel;
import com.dipcore.atvlauncher.item.title.TitleLauncherItemModel;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.UUID;

public class LauncherSection implements Comparable<LauncherSection> {
    private static final String TAG = LauncherSection.class.getCanonicalName();
    ArrayList<LauncherItemModel> items;
    int order;
    TitleLauncherItemModel title;
    String uuid;

    public LauncherSection() {
        this.order = 0;
        init(null, null);
    }

    public LauncherSection(String uuid, ArrayList<LauncherItemModel> items) {
        this.order = 0;
        this.uuid = uuid;
        init(null, items);
    }

    public LauncherSection(String uuid, String titleString, ArrayList<LauncherItemModel> items) {
        this.order = 0;
        this.uuid = uuid;
        TitleLauncherItemModel title = new TitleLauncherItemModel();
        title.setTitle(titleString);
        init(title, items);
    }

    public LauncherSection(TitleLauncherItemModel title, ArrayList<LauncherItemModel> items) {
        this.order = 0;
        this.uuid = UUID.randomUUID().toString();
        init(title, items);
    }

    public int compareTo(LauncherSection o) {
        return (int) Math.signum((float) (getOrder() - o.getOrder()));
    }

    public String toString() {
        return "[LauncherSection order = " + this.order + ", title = " + this.title + ", size = " + this.items.size() + "]";
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void setTitle(String titleString) {
        TitleLauncherItemModel title = new TitleLauncherItemModel();
        title.setTitle(titleString);
        setTitle(title);
    }

    public void setTitle(TitleLauncherItemModel title) {
        this.title = title;
    }

    public TitleLauncherItemModel getTitle() {
        return this.title;
    }

    public void setItems(ArrayList<LauncherItemModel> items) {
        Iterator it = items.iterator();
        while (it.hasNext()) {
            ((LauncherItemModel) it.next()).setSectionUuid(getUuid());
        }
        this.items = items;
    }

    public int getSize() {
        if (this.title != null) {
            return this.items.size() + 1;
        }
        return this.items.size();
    }

    public int getOrder() {
        return this.order;
    }

    public void setOrder(int order) {
        this.order = order;
    }

    public void setItemVisibility(int position, boolean visible) {
        ((LauncherItemModel) this.items.get(position)).setVisible(visible);
    }

    public boolean getItemVisibility(int position) {
        return ((LauncherItemModel) this.items.get(position)).isVisible();
    }

    public ArrayList<LauncherItemModel> getItems() {
        return this.items;
    }

    public ArrayList<LauncherItemModel> getItems(boolean visible) {
        return filterByVisibility(this.items, visible);
    }

    public LauncherItemModel getItem(String uuid) {
        Iterator it = this.items.iterator();
        while (it.hasNext()) {
            LauncherItemModel item = (LauncherItemModel) it.next();
            if (item instanceof FolderLauncherItemModel) {
                LauncherItemModel fItem = ((FolderLauncherItemModel) item).getItem(uuid);
                if (fItem != null) {
                    return fItem;
                }
            }
            if (item.getUuid().equals(uuid)) {
                return item;
            }
        }
        return null;
    }

    public void removeItem(String uuid) {
        this.items.remove(getItem(uuid));
    }

    public LauncherItemModel removeItem(int position) {
        return (LauncherItemModel) this.items.remove(position);
    }

    public void removeItem(LauncherItemModel item) {
        this.items.remove(item);
    }

    public void addItem(LauncherItemModel item) {
        item.setSectionUuid(getUuid());
        this.items.add(item);
    }

    public void addItem(int position, LauncherItemModel item) {
        item.setSectionUuid(getUuid());
        this.items.add(position, item);
    }

    public void moveItem(int from, int to) {
        this.items.add(to, this.items.remove(from));
    }

    public void moveItem(int from, int to, boolean visibility) {
        moveItem(getRealPosition(from, visibility), getRealPosition(to, visibility));
    }

    public int getPosition(String uuid) {
        return this.items.indexOf(getItem(uuid));
    }

    public int getRealPosition(int position, boolean visibility) {
        int count = -1;
        for (int i = 0; i < this.items.size(); i++) {
            if (((LauncherItemModel) this.items.get(i)).isVisible() == visibility) {
                count++;
            }
            if (count == position) {
                return i;
            }
        }
        return 0;
    }

    public int getCountBefore(int position, boolean visibility) {
        int res = 0;
        for (int i = 0; i < position; i++) {
            if (((LauncherItemModel) this.items.get(i)).isVisible() == visibility) {
                res++;
            }
        }
        return res;
    }

    private void init(TitleLauncherItemModel title, ArrayList<LauncherItemModel> items) {
        this.title = title;
        if (items == null) {
            this.items = new ArrayList();
        } else {
            this.items = items;
        }
        Iterator it = items.iterator();
        while (it.hasNext()) {
            ((LauncherItemModel) it.next()).setSectionUuid(getUuid());
        }
    }

    private ArrayList<LauncherItemModel> filterByVisibility(ArrayList<LauncherItemModel> items, boolean visible) {
        ArrayList<LauncherItemModel> result = new ArrayList();
        Iterator it = items.iterator();
        while (it.hasNext()) {
            LauncherItemModel item = (LauncherItemModel) it.next();
            if ((item.isVisible() && visible) || !(item.isVisible() || visible)) {
                result.add(item);
            }
        }
        return result;
    }
}
