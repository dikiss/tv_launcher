package com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.ViewHolder;
import com.dipcore.atvlauncher.widget.RendererRecyclerView.ItemModel;
import com.dipcore.atvlauncher.widget.RendererRecyclerView.RendererRecyclerViewAdapter;
import com.dipcore.atvlauncher.widget.RendererRecyclerView.ViewRenderer;
import java.util.ArrayList;
import java.util.Iterator;

public class LauncherRendererRecyclerViewAdapter extends RendererRecyclerViewAdapter {
    private static final String TAG = LauncherRendererRecyclerViewAdapter.class.getCanonicalName();
    private String uuid;

    public void onBindViewHolder(ViewHolder holder, int position) {
        LauncherItemModel item = getItem(position);
        Iterator it = this.mRenderers.iterator();
        while (it.hasNext()) {
            ViewRenderer renderer = (ViewRenderer) it.next();
            if (renderer.isSupportType(item.getType())) {
                renderer.bindView(item, holder, position);
                return;
            }
        }
        throw new RuntimeException("Not supported View Holder: " + holder);
    }

    public int getItemViewType(int position) {
        return getItem(position).getType();
    }

    @NonNull
    public LauncherItemModel getItem(int position) {
        return (LauncherItemModel) getItems().get(position);
    }

    public int getItemCount() {
        return getItems().size();
    }

    public int getItemPosition(String uuid) {
        return getItemPosition(uuid, getItems());
    }

    public int getItemPosition(String uuid, ArrayList<ItemModel> items) {
        int position = 0;
        Iterator it = items.iterator();
        while (it.hasNext()) {
            if (((LauncherItemModel) ((ItemModel) it.next())).getUuid().equals(uuid)) {
                return position;
            }
            position++;
        }
        return -1;
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public void moveItem(String fromUuid, String toUuid) {
        int fromPosition = getItemPosition(fromUuid);
        int toPosition = getItemPosition(toUuid);
        if (fromPosition > -1 && toPosition > -1 && fromPosition != toPosition) {
            moveItem(fromPosition, toPosition);
        }
    }

    public void moveItem(int fromPosition, int toPosition) {
        ArrayList<ItemModel> items = getItems();
        items.add(toPosition, items.remove(fromPosition));
        notifyItemMoved(fromPosition, toPosition);
    }

    public void refreshItem(String uuid) {
        notifyItemChanged(getItemPosition(uuid));
    }

    public void removeItem(String uuid) {
        int position = getItemPosition(uuid);
        if (position > -1) {
            getItems().remove(position);
            notifyItemRemoved(position);
        }
    }

    public void addItem(ItemModel item) {
        getItems().add(item);
        notifyItemInserted(getItems().size());
    }

    public void addItem(int position, ItemModel item) {
        if (position > getItems().size() - 1) {
            position = getItems().size() - 1;
        }
        getItems().add(position, item);
        notifyItemInserted(position);
    }

    public void addItems(ArrayList<LauncherItemModel> itemsToInsert) {
        int positionStart = getItems().size();
        int itemCount = itemsToInsert.size();
        getItems().addAll(itemsToInsert);
        notifyItemRangeInserted(positionStart, itemCount);
    }
}
