package com.dipcore.atvlauncher.widget.RatioLayout;

import android.view.View;
import android.view.View.MeasureSpec;

public final class RatioLayoutDelegate<TARGET extends View & RatioMeasureDelegate> {
    private float mDatumHeight = 0.0f;
    private float mDatumWidth = 0.0f;
    private int mHeightMeasureSpec;
    private RatioDatumMode mRatioDatumMode;
    private final TARGET mRatioMeasureDelegate;
    private int mWidthMeasureSpec;

    public RatioLayoutDelegate(TARGET target) {
        this.mRatioMeasureDelegate = target;
    }

    public void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        this.mWidthMeasureSpec = widthMeasureSpec;
        this.mHeightMeasureSpec = heightMeasureSpec;
        if (this.mRatioDatumMode != null && this.mDatumWidth != 0.0f && this.mDatumHeight != 0.0f) {
            ((RatioMeasureDelegate) this.mRatioMeasureDelegate).setDelegateMeasuredDimension(View.getDefaultSize(0, this.mWidthMeasureSpec), View.getDefaultSize(0, this.mHeightMeasureSpec));
            int measuredWidth = this.mRatioMeasureDelegate.getMeasuredWidth();
            int measuredHeight = this.mRatioMeasureDelegate.getMeasuredHeight();
            if (this.mRatioDatumMode == RatioDatumMode.DATUM_WIDTH) {
                measuredHeight = (int) ((((float) measuredWidth) / this.mDatumWidth) * this.mDatumHeight);
            } else {
                measuredWidth = (int) ((((float) measuredHeight) / this.mDatumHeight) * this.mDatumWidth);
            }
            this.mWidthMeasureSpec = MeasureSpec.makeMeasureSpec(measuredWidth, MeasureSpec.EXACTLY);
            this.mHeightMeasureSpec = MeasureSpec.makeMeasureSpec(measuredHeight, MeasureSpec.EXACTLY);
        }
    }

    public int getWidthMeasureSpec() {
        return this.mWidthMeasureSpec;
    }

    public int getHeightMeasureSpec() {
        return this.mHeightMeasureSpec;
    }

    public void setRatio(RatioDatumMode mode, float datumWidth, float datumHeight) {
        if (mode == null) {
            throw new IllegalArgumentException("RatioDatumMode == null");
        }
        this.mRatioDatumMode = mode;
        this.mDatumWidth = datumWidth;
        this.mDatumHeight = datumHeight;
        this.mRatioMeasureDelegate.requestLayout();
    }
}
