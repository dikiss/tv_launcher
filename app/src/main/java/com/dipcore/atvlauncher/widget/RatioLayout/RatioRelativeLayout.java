package com.dipcore.atvlauncher.widget.RatioLayout;

import android.content.Context;
import android.util.AttributeSet;
import android.widget.RelativeLayout;

public class RatioRelativeLayout extends RelativeLayout implements RatioMeasureDelegate {
    private RatioLayoutDelegate mRatioLayoutDelegate;

    public RatioRelativeLayout(Context context) {
        this(context, null);
    }

    public RatioRelativeLayout(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public RatioRelativeLayout(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        this.mRatioLayoutDelegate = new RatioLayoutDelegate(this);
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        if (this.mRatioLayoutDelegate != null) {
            this.mRatioLayoutDelegate.onMeasure(widthMeasureSpec, heightMeasureSpec);
            widthMeasureSpec = this.mRatioLayoutDelegate.getWidthMeasureSpec();
            heightMeasureSpec = this.mRatioLayoutDelegate.getHeightMeasureSpec();
        }
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
    }

    public void setDelegateMeasuredDimension(int measuredWidth, int measuredHeight) {
        setMeasuredDimension(measuredWidth, measuredHeight);
    }

    public void setRatio(RatioDatumMode mode, float datumWidth, float datumHeight) {
        if (this.mRatioLayoutDelegate != null) {
            this.mRatioLayoutDelegate.setRatio(mode, datumWidth, datumHeight);
        }
    }
}
