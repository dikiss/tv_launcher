package com.dipcore.atvlauncher.widget.RatioLayout;

public interface RatioMeasureDelegate {
    void setDelegateMeasuredDimension(int i, int i2);

    void setRatio(RatioDatumMode ratioDatumMode, float f, float f2);
}
