package com.dipcore.atvlauncher.menu;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.item.app.AppLauncherItemModel;
import com.dipcore.atvlauncher.item.app.AppViewHolder;
import com.dipcore.atvlauncher.menu.MenuBackground.OnBackgroundColorSelectedListener;
import com.dipcore.atvlauncher.menu.MenuBackground.OnBackgroundImageSelectedListener;
import com.dipcore.atvlauncher.menu.MenuBackground.OnBackgroundRemoveListener;
import com.dipcore.atvlauncher.widget.CardView;
import com.dipcore.menuitems.CheckBox;
import com.dipcore.menuitems.Text;
import com.dipcore.sidebar.OnDismissListener;
import com.dipcore.sidebar.Sidebar;
import com.dipcore.smoothcheckbox.SmoothCheckBox;
import com.dipcore.smoothcheckbox.SmoothCheckBox.OnCheckedChangeListener;

public class MenuAppConfigure extends Menu {
    private Text changeBackgroundTextItem;
    private OnClickListener changeBackgroundTextItemOnClickListener;
    private final Context context;
    private CheckBox displayIconCheckBox;
    private OnCheckedChangeListener displayIconCheckBoxOnCheckedChangeListener;
    private CheckBox displayTitleCheckBox;
    private OnCheckedChangeListener displayTitleCheckBoxOnCheckedChangeListener;
    private AppViewHolder holder;
    private AppLauncherItemModel model;

    public MenuAppConfigure(Context context) {
        this(context, null);
    }

    public MenuAppConfigure(Context context, Sidebar parent) {
        super(context, parent);
        this.changeBackgroundTextItemOnClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuBackground menu = new MenuBackground(MenuAppConfigure.this.context, MenuAppConfigure.this);
                menu.setBackgroundColor(MenuAppConfigure.this.model.getBackgroundColor());
                menu.setBackgroundImage(MenuAppConfigure.this.model.getBackgroundImage());
                menu.show();
                menu.setOnBackgroundColorSelectedListener(new OnBackgroundColorSelectedListener() {
                    @Override
                    public void onBackgroundColorSelected(int color) {
                        CardView view = (CardView) MenuAppConfigure.this.holder.itemView;
                        MenuAppConfigure.this.model.setBackgroundColor(color);
                        view.setBackgroundColor(color);
                        view.invalidate();
                    }
                });
                menu.setOnBackgroundImageSelectedListener(new OnBackgroundImageSelectedListener() {
                    @Override
                    public void onBackgroundImageSelected(String image) {
                        CardView view = (CardView) MenuAppConfigure.this.holder.itemView;
                        MenuAppConfigure.this.model.setBackgroundImage(image);
                        view.setBackgroundImage(image);
                        view.invalidate();
                    }
                });
                menu.setOnBackgroundRemoveListener(new OnBackgroundRemoveListener() {
                    @Override
                    public void onBackgroundRemove() {
                        CardView view = (CardView) MenuAppConfigure.this.holder.itemView;
                        MenuAppConfigure.this.model.resetBackground();
                        MenuAppConfigure.this.model.setShowIcon(true);
                        MenuAppConfigure.this.model.setShowTitle(true);
                        view.resetBackground();
                        view.invalidate();
                    }
                });
                menu.setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(Sidebar sidebar) {
                    }
                });
            }
        };
        this.displayIconCheckBoxOnCheckedChangeListener = new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SmoothCheckBox checkBox, boolean isChecked) {
                if (MenuAppConfigure.this.holder != null && MenuAppConfigure.this.model != null) {
                    CardView view = (CardView) MenuAppConfigure.this.holder.itemView;
                    MenuAppConfigure.this.model.setShowIcon(isChecked);
                    MenuAppConfigure.this.holder.iconImageView.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                    view.invalidate();
                }
            }
        };
        this.displayTitleCheckBoxOnCheckedChangeListener = new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(SmoothCheckBox checkBox, boolean isChecked) {
                if (MenuAppConfigure.this.holder != null && MenuAppConfigure.this.model != null) {
                    CardView view = (CardView) MenuAppConfigure.this.holder.itemView;
                    MenuAppConfigure.this.model.setShowTitle(isChecked);
                    MenuAppConfigure.this.holder.titleTextView.setVisibility(isChecked ? View.VISIBLE : View.GONE);
                    view.invalidate();
                }
            }
        };
        this.context = context;
        init();
    }

    public void setModel(AppLauncherItemModel model) {
        this.model = model;
        initItemsVisibility();
    }

    public void setHolder(AppViewHolder holder) {
        this.holder = holder;
    }

    private void init() {
        setContent(R.layout.menu_app_configure);
        setTitle((int) R.string.menu_app_configure_title);
        initItems();
        initCallbacks();
    }

    private void initItems() {
        this.displayIconCheckBox = (CheckBox) findViewById(R.id.display_icon_check_box);
        this.displayTitleCheckBox = (CheckBox) findViewById(R.id.display_title_text_item);
        this.changeBackgroundTextItem = (Text) findViewById(R.id.change_background_text_item);
    }

    private void initItemsVisibility() {
        this.displayIconCheckBox.setChecked(this.model.isShowIcon(), false);
        this.displayTitleCheckBox.setChecked(this.model.isShowTitle(), false);
    }

    private void initCallbacks() {
        this.displayIconCheckBox.setOnCheckedChangeListener(this.displayIconCheckBoxOnCheckedChangeListener);
        this.displayTitleCheckBox.setOnCheckedChangeListener(this.displayTitleCheckBoxOnCheckedChangeListener);
        this.changeBackgroundTextItem.setOnClickListener(this.changeBackgroundTextItemOnClickListener);
    }
}
