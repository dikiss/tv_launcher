package com.dipcore.atvlauncher.menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.graphics.Color;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.helper.ActivityForResultHelper;
import com.dipcore.atvlauncher.helper.CacheHelper;
import com.dipcore.imagepicker.ImagePickerActivity;
import com.dipcore.menuitems.Text;
import com.dipcore.sidebar.OnDismissListener;
import com.dipcore.sidebar.Sidebar;

public class MenuBackground extends Menu {
    private static int PICK_IMAGE_INTENT_ID = 10001;
    private int backgroundColor;
    private String backgroundImage;
    private final Context context;
    private Text customColorTextItem;
    private OnClickListener customColorTextItemOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            final MenuColorPicker menu = new MenuColorPicker(MenuBackground.this.context, MenuBackground.this);
            menu.setColor(MenuBackground.this.backgroundColor);
            menu.show();
            menu.setOnDismissListener(new OnDismissListener() {
                public void onDismiss(Sidebar sidebar) {
                    MenuBackground.this.backgroundColor = menu.getColor();
                    if (MenuBackground.this.onBackgroundColorSelectedListener != null) {
                        MenuBackground.this.onBackgroundColorSelectedListener.onBackgroundColorSelected(MenuBackground.this.backgroundColor);
                    }
                }
            });
        }
    };
    private Text imageTextItem;
    private OnClickListener imageTextItemOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent();
            intent.setComponent(new ComponentName(MenuBackground.this.context.getPackageName(), ImagePickerActivity.class.getName()));
            intent.putExtra("PREVIEW", false);
            ActivityForResultHelper.start(MenuBackground.this.context, intent, MenuBackground.PICK_IMAGE_INTENT_ID, new ActivityForResultHelper.OnActivityResultListener() {
                @Override
                public void onActivityResult(int requestCode, int resultCode, Intent intent) {
                    if (requestCode == MenuBackground.PICK_IMAGE_INTENT_ID && resultCode == -1 && intent != null) {
                        String path = CacheHelper.storeFile(MenuBackground.this.context, Uri.parse(intent.getExtras().getString("uri")));
                        if (MenuBackground.this.onBackgroundImageSelectedListener != null) {
                            MenuBackground.this.onBackgroundImageSelectedListener.onBackgroundImageSelected(path);
                        }
                        MenuBackground.this.dismiss();
                    }
                }
            });
        }
    };
    private OnBackgroundColorSelectedListener onBackgroundColorSelectedListener;
    private OnBackgroundImageSelectedListener onBackgroundImageSelectedListener;
    private OnBackgroundRemoveListener onBackgroundRemoveListener;
    private Text removeTextItem;
    private OnClickListener removeTextItemOnClickListener = new OnClickListener() {
        @Override
        public void onClick(View view) {
            MenuBackground.this.backgroundColor = 0;
            MenuBackground.this.backgroundImage = null;
            if (MenuBackground.this.onBackgroundRemoveListener != null) {
                MenuBackground.this.onBackgroundRemoveListener.onBackgroundRemove();
            }
            MenuBackground.this.dismiss();
        }
    };

    public interface OnBackgroundColorSelectedListener {
        void onBackgroundColorSelected(int i);
    }

    public interface OnBackgroundImageSelectedListener {
        void onBackgroundImageSelected(String str);
    }

    public interface OnBackgroundRemoveListener {
        void onBackgroundRemove();
    }

    public MenuBackground(Context context) {
        super(context);
        this.context = context;
        init();
    }

    public MenuBackground(Context context, Sidebar parent) {
        super(context, parent);
        this.context = context;
        init();
    }

    public void setBackgroundColor(int backgroundColor) {
        this.backgroundColor = backgroundColor;
    }

    public void setBackgroundColor(String color) {
        if (color != null) {
            setBackgroundColor(Color.parseColor(color));
        }
    }

    public void setBackgroundImage(String backgroundImage) {
        this.backgroundImage = backgroundImage;
    }

    public int getBackgroundColor() {
        return this.backgroundColor;
    }

    public String getBackgroundImage() {
        return this.backgroundImage;
    }

    public void setOnBackgroundColorSelectedListener(OnBackgroundColorSelectedListener onBackgroundColorSelectedListener) {
        this.onBackgroundColorSelectedListener = onBackgroundColorSelectedListener;
    }

    public void setOnBackgroundImageSelectedListener(OnBackgroundImageSelectedListener onBackgroundImageSelectedListener) {
        this.onBackgroundImageSelectedListener = onBackgroundImageSelectedListener;
    }

    public void setOnBackgroundRemoveListener(OnBackgroundRemoveListener onBackgroundRemoveListener) {
        this.onBackgroundRemoveListener = onBackgroundRemoveListener;
    }

    private void init() {
        setContent(R.layout.menu_background);
        setTitle((int) R.string.menu_background_title);
        initItems();
        initCallbacks();
    }

    private void initItems() {
        LinearLayout sidebarContainer = getSidebarContainer();
        this.customColorTextItem = (Text) sidebarContainer.findViewById(R.id.custom_color_text_item);
        this.imageTextItem = (Text) sidebarContainer.findViewById(R.id.image_text_item);
        this.removeTextItem = (Text) sidebarContainer.findViewById(R.id.remove_background_text_item);
    }

    private void initCallbacks() {
        this.customColorTextItem.setOnClickListener(this.customColorTextItemOnClickListener);
        this.imageTextItem.setOnClickListener(this.imageTextItemOnClickListener);
        this.removeTextItem.setOnClickListener(this.removeTextItemOnClickListener);
    }
}
