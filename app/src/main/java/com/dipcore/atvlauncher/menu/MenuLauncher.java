package com.dipcore.atvlauncher.menu;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.view.View;
import android.view.View.OnClickListener;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.LauncherConstants;
import com.dipcore.atvlauncher.event.StorageSaveItemsAction;
import com.dipcore.atvlauncher.event.WallpaperSetAction;
import com.dipcore.atvlauncher.helper.ActivityForResultHelper;
import com.dipcore.atvlauncher.helper.ActivityForResultHelper.OnActivityResultListener;
import com.dipcore.imagepicker.ImagePickerActivity;
import com.dipcore.menuitems.Text;
import com.dipcore.sidebar.OnDismissListener;
import com.dipcore.sidebar.Sidebar;
import org.greenrobot.eventbus.EventBus;

public class MenuLauncher extends Menu {
    private static final int PICK_WALLPAPER_INTENT_ID = 90000;
    private final Context context;
    private Text hiddenAppsSettingsTextItem;
    private OnClickListener hiddenAppsTextItemOnClickListener;
    private Text wallpaperTextItem;
    private OnClickListener wallpaperTextItemOnClickListener;

    public MenuLauncher(Context context) {
        this(context, null);
    }

    public MenuLauncher(Context context, Sidebar parent) {
        super(context, parent);
        this.hiddenAppsTextItemOnClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuHiddenApps menu = new MenuHiddenApps(MenuLauncher.this.context, MenuLauncher.this);
                menu.show();
                menu.setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(Sidebar sidebar) {
                        EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.AppsSectionUuid));
                    }
                });
            }
        };
        this.wallpaperTextItemOnClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuLauncher.this.instantDismiss();
                Intent intent = new Intent("android.intent.action.SET_WALLPAPER");
                intent.setComponent(new ComponentName(MenuLauncher.this.context.getPackageName(), ImagePickerActivity.class.getName()));
                ActivityForResultHelper.start(MenuLauncher.this.context, intent, MenuLauncher.PICK_WALLPAPER_INTENT_ID, new OnActivityResultListener() {
                    @Override
                    public void onActivityResult(int requestCode, int resultCode, Intent data) {
                        if (requestCode == MenuLauncher.PICK_WALLPAPER_INTENT_ID && resultCode == -1 && data != null) {
                            EventBus.getDefault().postSticky(new WallpaperSetAction(Uri.parse(data.getExtras().getString("uri"))));
                        }
                    }
                });
            }
        };
        this.context = context;
        init();
    }

    private void init() {
        setContent(R.layout.menu_launcher);
        setTitle((int) R.string.menu_launcher_title);
        initItems();
        initCallbacks();
    }

    private void initItems() {
        this.hiddenAppsSettingsTextItem = (Text) findViewById(R.id.hidden_apps_text_item);
        this.wallpaperTextItem = (Text) findViewById(R.id.wallpaper_text_item);
    }

    private void initCallbacks() {
        this.hiddenAppsSettingsTextItem.setOnClickListener(this.hiddenAppsTextItemOnClickListener);
        this.wallpaperTextItem.setOnClickListener(this.wallpaperTextItemOnClickListener);
    }
}
