package com.dipcore.atvlauncher.menu;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.LauncherApplication;
import com.dipcore.atvlauncher.LauncherConstants;
import com.dipcore.atvlauncher.event.ItemsAdapterAddItemAction;
import com.dipcore.atvlauncher.event.ItemsAdapterRemoveItemAction;
import com.dipcore.atvlauncher.helper.AppHelper;
import com.dipcore.atvlauncher.item.app.AppLauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherSections;
import com.dipcore.menuitems.ToggleSwitch;
import com.dipcore.sidebar.Sidebar;
import java.util.ArrayList;
import org.greenrobot.eventbus.EventBus;

public class MenuHiddenApps extends Menu {
    private final Context context;
    private ArrayList<LauncherItemModel> items;
    private LauncherSections launcherSections;
    private RecyclerView recyclerView;

    private class AppListAdapter extends Adapter<AppListAdapter.ViewHolder> {

        public class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
            ToggleSwitch toggleSwitch;

            public ViewHolder(ToggleSwitch toggleSwitch) {
                super(toggleSwitch);
                this.toggleSwitch = toggleSwitch;
            }
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder((ToggleSwitch) LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_hidden_apps_item, parent, false));
        }
        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            final AppLauncherItemModel model = (AppLauncherItemModel) MenuHiddenApps.this.items.get(position);
            holder.toggleSwitch.setTitleText(model.getTitle());
            holder.toggleSwitch.setIcon(AppHelper.getIcon(MenuHiddenApps.this.context, model.getPackageName()));
            holder.toggleSwitch.setCheckedImmediatelyNoEvent(!model.isVisible());
            holder.toggleSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                @Override
                public void onCheckedChanged(CompoundButton compoundButton, boolean isChecked) {
                    model.setVisible(!isChecked);
                    if (isChecked) {
                        EventBus.getDefault().post(new ItemsAdapterRemoveItemAction(LauncherConstants.HomeAdapterUuid, model.getUuid()));
                    } else {
                        EventBus.getDefault().post(new ItemsAdapterAddItemAction(LauncherConstants.HomeAdapterUuid, (MenuHiddenApps.this.launcherSections.getSection(LauncherConstants.AppsSectionUuid).getCountBefore(position, true) + MenuHiddenApps.this.launcherSections.getSectionOffset(LauncherConstants.AppsSectionUuid)) + 1, model));
                    }
                }
            });
            holder.toggleSwitch.setOnKeyListener(new OnKeyListener() {
                @Override
                public boolean onKey(View v, int keyCode, KeyEvent event) {
                    if (event.getAction() == 0 && keyCode == 4) {
                        MenuHiddenApps.this.dismiss();
                    }
                    return false;
                }
            });
        }
        @Override
        public int getItemCount() {
            return MenuHiddenApps.this.items.size();
        }
    }

    public MenuHiddenApps(Context context) {
        this(context, null);
    }

    public MenuHiddenApps(Context context, Sidebar parent) {
        super(context, parent);
        this.context = context;
        init();
        initListView();
    }

    private void init() {
        setContent(R.layout.menu_hidden_apps);
        setTitle((int) R.string.menu_hidden_apps_title);
        this.launcherSections = LauncherApplication.getInstance().getLauncherSections();
        this.items = this.launcherSections.getSection(LauncherConstants.AppsSectionUuid).getItems();
        this.items = AppHelper.flatten(this.items);
    }

    private void initListView() {
        this.recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        this.recyclerView.setAdapter(new AppListAdapter());
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this.context));
    }
}
