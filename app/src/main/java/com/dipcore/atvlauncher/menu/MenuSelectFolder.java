package com.dipcore.atvlauncher.menu;

import android.content.Context;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.RecyclerView.Adapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.LauncherApplication;
import com.dipcore.atvlauncher.item.folder.FolderLauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherSections;
import com.dipcore.menuitems.Text;
import com.dipcore.sidebar.Sidebar;
import java.util.ArrayList;

public class MenuSelectFolder extends Menu {
    private final Context context;
    private ArrayList<FolderLauncherItemModel> items;
    private LauncherSections launcherSections;
    private OnItemClickedListener onItemClickedListener;
    private RecyclerView recyclerView;

    public interface OnItemClickedListener {
        void onClick(View view, String str, int i);
    }

    private class FolderListAdapter extends Adapter<FolderListAdapter.ViewHolder> {

        public class ViewHolder extends android.support.v7.widget.RecyclerView.ViewHolder {
            Text text;

            public ViewHolder(Text text) {
                super(text);
                this.text = text;
            }
        }
        @Override
        public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            return new ViewHolder((Text) LayoutInflater.from(parent.getContext()).inflate(R.layout.menu_select_folder_item, parent, false));
        }
        @Override
        public void onBindViewHolder(ViewHolder holder, final int position) {
            final FolderLauncherItemModel model = (FolderLauncherItemModel) MenuSelectFolder.this.items.get(position);
            holder.text.setTitleText(model.getTitle());
            holder.text.setIcon((int) R.drawable.folder);
            holder.text.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    MenuSelectFolder.this.dismiss();
                    if (MenuSelectFolder.this.onItemClickedListener != null) {
                        MenuSelectFolder.this.onItemClickedListener.onClick(view, model.getUuid(), position);
                    }
                }
            });
        }
        @Override
        public int getItemCount() {
            return MenuSelectFolder.this.items.size();
        }
    }

    public MenuSelectFolder(Context context) {
        this(context, null);
    }

    public MenuSelectFolder(Context context, Sidebar parent) {
        super(context, parent);
        this.context = context;
        init();
    }

    public void setOnItemClickedListener(OnItemClickedListener onItemClickedListener) {
        this.onItemClickedListener = onItemClickedListener;
    }

    private void init() {
        setContent(R.layout.menu_select_folder);
        setTitle((int) R.string.menu_title_select_folder);
        this.launcherSections = LauncherApplication.getInstance().getLauncherSections();
        this.items = this.launcherSections.getFolders();
        initListView();
    }

    private void initListView() {
        this.recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        this.recyclerView.setAdapter(new FolderListAdapter());
        this.recyclerView.setHasFixedSize(true);
        this.recyclerView.setLayoutManager(new LinearLayoutManager(this.context));
    }
}
