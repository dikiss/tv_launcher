package com.dipcore.atvlauncher.menu;

import android.appwidget.AppWidgetProviderInfo;
import android.content.Context;
import android.content.DialogInterface;
import android.view.View;

import com.dipcore.atvlauncher.LauncherConstants;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.dialog.DialogWidgetScaleFactor;
import com.dipcore.atvlauncher.event.StorageSaveItemsAction;
import com.dipcore.atvlauncher.event.WidgetConfigureEvent;
import com.dipcore.atvlauncher.event.WidgetPickEvent;
import com.dipcore.atvlauncher.event.WidgetRemoveEvent;
import com.dipcore.atvlauncher.event.WidgetSetScaleEvent;
import com.dipcore.atvlauncher.event.WidgetSetTapCoordinatesEvent;
import com.dipcore.atvlauncher.helper.WidgetHelper;
import com.dipcore.atvlauncher.item.widget.WidgetLauncherItemModel;
import com.dipcore.atvlauncher.widget.WidgetView;
import com.dipcore.menuitems.Text;
import com.dipcore.sidebar.OnDismissListener;
import com.dipcore.sidebar.Sidebar;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Diana 2018.
 */

public class MenuWidget extends Menu {
    private static String TAG = MenuWidget.class.getName();
    private Text changeBackgroundTextItem;
    private View.OnClickListener changeBackgroundTextItemOnClickListener;
    private Text configureWidgetTextItem;
    private View.OnClickListener configureWidgetTextItemOnClickListener;
    private Context context;
    private Text launcherSettingsTextItem;
    private View.OnClickListener launcherSettingsTextItemOnClickListener;
    private Text pickWidgetTextItem;
    private View.OnClickListener pickWidgetTextItemOnClickListener;
    private int position;
    private Text removeWidgetTextItem;
    private View.OnClickListener removeWidgetTextItemOnClickListener;
    private Text scaleWidgetTextItem;
    private View.OnClickListener scaleWidgetTextItemOnClickListener;
    private Text tapCoordinatesTextItem;
    private View.OnClickListener tapCoordinatesTextItemOnClickListener;
    private WidgetLauncherItemModel widgetItemModel;
    private WidgetView widgetView;

    public MenuWidget(Context context) {
        this(context, null);
    }

    public MenuWidget(Context context, Sidebar parent) {
        super(context, parent);
        this.pickWidgetTextItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuWidget.this.dismiss();
                EventBus.getDefault().post(new WidgetPickEvent(MenuWidget.this.position));
            }
        };
        this.removeWidgetTextItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuWidget.this.dismiss();
                EventBus.getDefault().post(new WidgetRemoveEvent(MenuWidget.this.position));
            }
        };
        this.configureWidgetTextItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuWidget.this.dismiss();
                EventBus.getDefault().post(new WidgetConfigureEvent(MenuWidget.this.position));
            }
        };
        this.launcherSettingsTextItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MenuLauncher(MenuWidget.this.context, MenuWidget.this).show();
            }
        };
        this.changeBackgroundTextItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuBackground menu = new MenuBackground(MenuWidget.this.context, MenuWidget.this);
                menu.setBackgroundColor(MenuWidget.this.widgetItemModel.getBackgroundColor());
                menu.setBackgroundImage(MenuWidget.this.widgetItemModel.getBackgroundImage());
                menu.show();
                menu.setOnBackgroundColorSelectedListener(new MenuBackground.OnBackgroundColorSelectedListener() {
                    @Override
                    public void onBackgroundColorSelected(int color) {
                        MenuWidget.this.widgetItemModel.setBackgroundColor(color);
                        MenuWidget.this.widgetView.setBackgroundColor(color);
                        MenuWidget.this.widgetView.invalidate();
                    }
                });
                menu.setOnBackgroundImageSelectedListener(new MenuBackground.OnBackgroundImageSelectedListener() {
                    @Override
                    public void onBackgroundImageSelected(String image) {
                        MenuWidget.this.widgetItemModel.setBackgroundImage(image);
                        MenuWidget.this.widgetView.setBackgroundImage(image);
                        MenuWidget.this.widgetView.invalidate();
                    }
                });
                menu.setOnBackgroundRemoveListener(new MenuBackground.OnBackgroundRemoveListener() {
                    @Override
                    public void onBackgroundRemove() {
                        MenuWidget.this.widgetItemModel.resetBackground();
                        MenuWidget.this.widgetView.resetBackground();
                        MenuWidget.this.widgetView.invalidate();
                    }
                });
                menu.setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(Sidebar sidebar) {
                        EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.WidgetSectionUuid));
                    }
                });
            }
        };
        this.tapCoordinatesTextItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final MenuWidgetTapCoordinates menu = new MenuWidgetTapCoordinates(MenuWidget.this.context, MenuWidget.this);
                menu.setWidgetItemModel(MenuWidget.this.widgetItemModel);
                menu.setWidgetHeight(MenuWidget.this.widgetView.getLauncherAppWidgetHostView().getHeight());
                menu.setWidgetWidth(MenuWidget.this.widgetView.getLauncherAppWidgetHostView().getWidth());
                menu.show();
                menu.setOnDismissListener(new OnDismissListener() {
                    @Override
                    public void onDismiss(Sidebar sidebar) {
                        EventBus.getDefault().post(new WidgetSetTapCoordinatesEvent(MenuWidget.this.position, menu.getX(), menu.getY()));
                    }
                });
            }
        };
        this.scaleWidgetTextItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                final DialogWidgetScaleFactor dialog = new DialogWidgetScaleFactor(MenuWidget.this.context);
                dialog.setWidgetItemModel(MenuWidget.this.widgetItemModel);
                dialog.setWidgetHeight(MenuWidget.this.widgetView.getLauncherAppWidgetHostView().getHeight());
                dialog.setWidgetWidth(MenuWidget.this.widgetView.getLauncherAppWidgetHostView().getWidth());
                dialog.show();
                dialog.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
                        EventBus.getDefault().post(new WidgetSetScaleEvent(MenuWidget.this.position, dialog.getScale()));
                    }
                });
                MenuWidget.this.dismiss();
            }
        };
        this.context = context;
        init();
    }

    public void setWidgetItemModel(WidgetLauncherItemModel widgetItemModel) {
        this.widgetItemModel = widgetItemModel;
        initItemsVisibility();
    }

    public void setWidgetView(WidgetView widgetView) {
        this.widgetView = widgetView;
    }

    public void setPosition(int position) {
        this.position = position;
    }

    private void init() {
        setContent(R.layout.menu_widget);
        setTitle((int) R.string.menu_widgets_title);
        initItems();
        initCallbacks();
    }

    private void initItems() {
        this.pickWidgetTextItem = (Text) findViewById(R.id.pick_widget_text_item);
        this.removeWidgetTextItem = (Text) findViewById(R.id.remove_widget_text_item);
        this.configureWidgetTextItem = (Text) findViewById(R.id.configure_widget_text_item);
        this.launcherSettingsTextItem = (Text) findViewById(R.id.launcher_settings_text_item);
        this.changeBackgroundTextItem = (Text) findViewById(R.id.change_background_text_item);
        this.tapCoordinatesTextItem = (Text) findViewById(R.id.tap_coordinates_text_item);
        this.scaleWidgetTextItem = (Text) findViewById(R.id.scale_widget_text_item);
    }

    private void initItemsVisibility() {
        if (this.widgetItemModel.getAppWidgetId() != 0) {
            AppWidgetProviderInfo appWidgetInfo = WidgetHelper.getAppWidgetInfo(this.context, this.widgetItemModel.getAppWidgetId());
            if (!(appWidgetInfo == null || appWidgetInfo.configure == null)) {
                this.configureWidgetTextItem.setVisibility(View.VISIBLE);
            }
            this.removeWidgetTextItem.setVisibility(View.VISIBLE);
            this.changeBackgroundTextItem.setVisibility(View.VISIBLE);
            this.tapCoordinatesTextItem.setVisibility(View.VISIBLE);
            this.scaleWidgetTextItem.setVisibility(View.VISIBLE);
        }
    }

    private void initCallbacks() {
        this.pickWidgetTextItem.setOnClickListener(this.pickWidgetTextItemOnClickListener);
        this.removeWidgetTextItem.setOnClickListener(this.removeWidgetTextItemOnClickListener);
        this.configureWidgetTextItem.setOnClickListener(this.configureWidgetTextItemOnClickListener);
        this.launcherSettingsTextItem.setOnClickListener(this.launcherSettingsTextItemOnClickListener);
        this.changeBackgroundTextItem.setOnClickListener(this.changeBackgroundTextItemOnClickListener);
        this.tapCoordinatesTextItem.setOnClickListener(this.tapCoordinatesTextItemOnClickListener);
        this.scaleWidgetTextItem.setOnClickListener(this.scaleWidgetTextItemOnClickListener);
    }
}

