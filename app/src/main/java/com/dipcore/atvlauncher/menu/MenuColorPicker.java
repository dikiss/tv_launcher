package com.dipcore.atvlauncher.menu;

import android.content.Context;
import android.graphics.Color;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.LinearLayout;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.helper.ColorHelper;
import com.dipcore.menuitems.SeekBar;
import com.dipcore.menuitems.Text;
import com.dipcore.sidebar.Sidebar;

public class MenuColorPicker extends Menu {
    private SeekBar alphaSeekBar;
    private int color;
    private TextView colorStringValueTextView;
    private View colorView;
    private final Context context;
    private SeekBar hueSeekBar;
    OnSeekBarChangeListener onSeekBarChangeListener;
    private Text randomColorText;
    OnClickListener randomColorTextOnClickListener;
    private SeekBar saturationSeekBar;
    private SeekBar valueSeekBar;

    public MenuColorPicker(Context context) {
        this(context, null);
    }

    public MenuColorPicker(Context context, Sidebar parent) {
        super(context, parent);
        this.onSeekBarChangeListener = new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(android.widget.SeekBar seekBar, int i, boolean b) {
                MenuColorPicker.this.HSVToColor();
            }
            @Override
            public void onStartTrackingTouch(android.widget.SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(android.widget.SeekBar seekBar) {
            }
        };
        this.randomColorTextOnClickListener = new OnClickListener() {
            @Override
            public void onClick(View v) {
                MenuColorPicker.this.setColor(ColorHelper.getMatColor(MenuColorPicker.this.context));
            }
        };
        this.context = context;
        init();
    }

    public void setColor(int color) {
        this.color = color;
        float[] hsv = new float[3];
        Color.colorToHSV(color, hsv);
        this.hueSeekBar.setValue((int) hsv[0]);
        this.saturationSeekBar.setValue((int) (hsv[1] * 255.0f));
        this.valueSeekBar.setValue((int) (hsv[2] * 255.0f));
        this.alphaSeekBar.setValue(Color.alpha(color));
        this.colorView.setBackgroundColor(color);
    }

    public int getColor() {
        return this.color;
    }

    private void init() {
        setContent(R.layout.menu_color_picker);
        setTitle((int) R.string.menu_color_picker_title);
        initItems();
        initCallbacks();
    }

    private void initItems() {
        LinearLayout sidebarContainer = getSidebarContainer();
        this.colorView = sidebarContainer.findViewById(R.id.color_view);
        this.hueSeekBar = (SeekBar) sidebarContainer.findViewById(R.id.hue_seek_bar);
        this.saturationSeekBar = (SeekBar) sidebarContainer.findViewById(R.id.saturation_seek_bar);
        this.valueSeekBar = (SeekBar) sidebarContainer.findViewById(R.id.value_seek_bar);
        this.alphaSeekBar = (SeekBar) sidebarContainer.findViewById(R.id.alpha_seek_bar);
        this.colorStringValueTextView = (TextView) sidebarContainer.findViewById(R.id.color_string_value_text_view);
        this.randomColorText = (Text) sidebarContainer.findViewById(R.id.random_color_text_item);
    }

    private void initCallbacks() {
        this.hueSeekBar.setOnSeekBarChangeListener(this.onSeekBarChangeListener);
        this.saturationSeekBar.setOnSeekBarChangeListener(this.onSeekBarChangeListener);
        this.valueSeekBar.setOnSeekBarChangeListener(this.onSeekBarChangeListener);
        this.alphaSeekBar.setOnSeekBarChangeListener(this.onSeekBarChangeListener);
        this.randomColorText.setOnClickListener(this.randomColorTextOnClickListener);
    }

    private void HSVToColor() {
        float hue = (float) this.hueSeekBar.getValue();
        float saturation = ((float) this.saturationSeekBar.getValue()) / 255.0f;
        float value = ((float) this.valueSeekBar.getValue()) / 255.0f;
        this.color = Color.HSVToColor(this.alphaSeekBar.getValue(), new float[]{hue, saturation, value});
        this.colorView.setBackgroundColor(this.color);
        this.colorStringValueTextView.setText(ColorHelper.toString(this.color));
    }
}
