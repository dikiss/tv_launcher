package com.dipcore.atvlauncher.menu;

import android.content.Context;
import android.view.View;
import android.view.View.OnClickListener;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.LauncherApplication;
import com.dipcore.atvlauncher.LauncherConstants;
import com.dipcore.atvlauncher.dialog.DialogConfirmation;
import com.dipcore.atvlauncher.event.ItemsAdapterAddItemsAction;
import com.dipcore.atvlauncher.event.ItemsAdapterRemoveItemAction;
import com.dipcore.atvlauncher.event.StorageSaveItemsAction;
import com.dipcore.atvlauncher.item.folder.FolderLauncherItemModel;
import com.dipcore.atvlauncher.item.folder.FolderViewHolder;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherSections;
import com.dipcore.menuitems.Text;
import com.dipcore.sidebar.Sidebar;
import java.util.ArrayList;
import java.util.Iterator;
import org.greenrobot.eventbus.EventBus;

public class MenuFolder extends Menu {
    private final Context context;
    private Text deleteFolderTextItem;
    private OnClickListener deleteFolderTextItemOnClickListener;
    private FolderLauncherItemModel folderItem;
    private FolderViewHolder holder;
    private LauncherSections launcherSections;
    private Text launcherSettingsTextItem;
    private OnClickListener launcherSettingsTextItemOnClickListener;
    private FolderLauncherItemModel model;
    private Text moveAppTextItem;
    private OnClickListener moveAppTextItemOnClickListener;
    private OnMoveSelectedListener onMoveSelectedListener;
    private String uuid;

    public interface OnMoveSelectedListener {
        void onMoveSelected(FolderLauncherItemModel folderLauncherItemModel, FolderViewHolder folderViewHolder);
    }

    public MenuFolder(Context context) {
        this(context, null);
    }

    public MenuFolder(Context context, Sidebar parent) {
        super(context, parent);
        this.moveAppTextItemOnClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuFolder.this.onMoveSelectedListener.onMoveSelected(MenuFolder.this.model, MenuFolder.this.holder);
                MenuFolder.this.dismiss();
            }
        };
        this.deleteFolderTextItemOnClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogConfirmation dialogConfirmation = new DialogConfirmation(getContext());
                dialogConfirmation.setTitle((int) R.string.dialog_delete_folder_title);
                dialogConfirmation.setContent((int) R.string.dialog_delete_folder_confirmation);
                dialogConfirmation.show();
                dialogConfirmation.setOnPositiveButtonClickedListener(new DialogConfirmation.OnPositiveButtonClickedListener() {
                    @Override
                    public void onClick(View view) {
                        launcherSections.removeItem(LauncherConstants.AppsSectionUuid, uuid);
                        EventBus.getDefault().post(new ItemsAdapterRemoveItemAction(LauncherConstants.HomeAdapterUuid, uuid));
                        ArrayList<LauncherItemModel> folderItems = folderItem.getItems();
                        Iterator it = folderItems.iterator();
                        while (it.hasNext()) {
                            launcherSections.addItem(LauncherConstants.AppsSectionUuid, (LauncherItemModel) it.next());
                        }
                        EventBus.getDefault().post(new ItemsAdapterAddItemsAction(LauncherConstants.HomeAdapterUuid, folderItems));
                        EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.AppsSectionUuid));
                    }
                });
                dismiss();
            }
        };
        this.launcherSettingsTextItemOnClickListener = new OnClickListener() {
            @Override
            public void onClick(View view) {
                new MenuLauncher(getContext(), MenuFolder.this).show();
            }
        };
        this.context = context;
        this.launcherSections = LauncherApplication.getInstance().getLauncherSections();
        init();
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
        this.folderItem = (FolderLauncherItemModel) this.launcherSections.getItemByUuid(uuid);
    }

    public void setModel(FolderLauncherItemModel model) {
        this.model = model;
        this.uuid = model.getUuid();
        initItemsVisibility();
    }

    public void setHolder(FolderViewHolder holder) {
        this.holder = holder;
    }

    public void setOnMoveSelectedListener(OnMoveSelectedListener onMoveSelectedListener) {
        this.onMoveSelectedListener = onMoveSelectedListener;
    }

    private void init() {
        setContent(R.layout.menu_folder);
        setTitle((int) R.string.menu_title_folder);
        initItems();
        initCallbacks();
    }

    private void initItems() {
        this.moveAppTextItem = (Text) findViewById(R.id.move_app_text_item);
        this.deleteFolderTextItem = (Text) findViewById(R.id.delete_folder_text_item);
        this.launcherSettingsTextItem = (Text) findViewById(R.id.launcher_settings_text_item);
    }

    private void initItemsVisibility() {
    }

    private void initCallbacks() {
        this.moveAppTextItem.setOnClickListener(this.moveAppTextItemOnClickListener);
        this.deleteFolderTextItem.setOnClickListener(this.deleteFolderTextItemOnClickListener);
        this.launcherSettingsTextItem.setOnClickListener(this.launcherSettingsTextItemOnClickListener);
    }
}
