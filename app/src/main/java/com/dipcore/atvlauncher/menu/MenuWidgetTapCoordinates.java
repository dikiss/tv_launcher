package com.dipcore.atvlauncher.menu;

import android.content.Context;
import android.widget.ImageView;
import android.widget.RelativeLayout.LayoutParams;
import android.widget.SeekBar.OnSeekBarChangeListener;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.helper.CompatUtils;
import com.dipcore.atvlauncher.helper.WidgetHelper;
import com.dipcore.atvlauncher.item.widget.WidgetLauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherAppWidget.LauncherAppWidgetHostView;
import com.dipcore.atvlauncher.widget.RatioLayout.RatioDatumMode;
import com.dipcore.atvlauncher.widget.RatioLayout.RatioRelativeLayout;
import com.dipcore.menuitems.SeekBar;
import com.dipcore.sidebar.Sidebar;

public class MenuWidgetTapCoordinates extends Menu {
    private final Context context;
    private LauncherAppWidgetHostView launcherAppWidgetHostView;
    OnSeekBarChangeListener onSeekBarChangeListener;
    private RatioRelativeLayout pickerContainer;
    private ImageView tapIndicator;
    private int tapIndicatorHeight;
    private int tapIndicatorWidth;
    private int widgetHeight;
    private WidgetLauncherItemModel widgetItemModel;
    private int widgetWidth;
    private int f393x;
    private SeekBar xSeekBar;
    private int f394y;
    private SeekBar ySeekBar;

    public MenuWidgetTapCoordinates(Context context) {
        this(context, null);
    }

    public MenuWidgetTapCoordinates(Context context, Sidebar parent) {
        super(context, parent);
        this.onSeekBarChangeListener = new OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(android.widget.SeekBar seekBar, int i, boolean b) {
                MenuWidgetTapCoordinates.this.f393x = MenuWidgetTapCoordinates.this.xSeekBar.getValue();
                MenuWidgetTapCoordinates.this.f394y = MenuWidgetTapCoordinates.this.ySeekBar.getValue();
                MenuWidgetTapCoordinates.this.setX(MenuWidgetTapCoordinates.this.f393x);
                MenuWidgetTapCoordinates.this.setY(MenuWidgetTapCoordinates.this.f394y);
            }
            @Override
            public void onStartTrackingTouch(android.widget.SeekBar seekBar) {
            }
            @Override
            public void onStopTrackingTouch(android.widget.SeekBar seekBar) {
            }
        };
        this.context = context;
        this.tapIndicatorHeight = CompatUtils.dp2px(context, 20.0f);
        this.tapIndicatorWidth = CompatUtils.dp2px(context, 20.0f);
        init();
    }

    public void setWidgetItemModel(WidgetLauncherItemModel widgetItemModel) {
        this.widgetItemModel = widgetItemModel;
    }

    public void setWidgetHeight(int widgetHeight) {
        this.widgetHeight = widgetHeight;
    }

    public void setWidgetWidth(int widgetWidth) {
        this.widgetWidth = widgetWidth;
    }

    public void show() {
        this.launcherAppWidgetHostView = WidgetHelper.createAppWidgetHostView(getContext(), this.widgetItemModel.getLauncherAppWidgetHost(), this.widgetItemModel.getAppWidgetId());
        this.launcherAppWidgetHostView.setBackgroundColor(1073741824);
        this.pickerContainer.addView(this.launcherAppWidgetHostView, 0);
        super.show();
    }

    private void init() {
        setContent(R.layout.menu_widget_tap_coordinates);
        setTitle((int) R.string.menu_widget_tap_coordinates_title);
        initItems();
        initCallbacks();
    }

    private void initItems() {
        this.pickerContainer = (RatioRelativeLayout) findViewById(R.id.picker_container);
        this.pickerContainer.setRatio(RatioDatumMode.DATUM_WIDTH, 16.0f, 10.0f);
        this.pickerContainer.setPadding(this.tapIndicatorWidth / 2, this.tapIndicatorHeight / 2, this.tapIndicatorWidth / 2, this.tapIndicatorHeight / 2);
        this.pickerContainer.post(new Runnable() {
            @Override
            public void run() {
                MenuWidgetTapCoordinates.this.setX(MenuWidgetTapCoordinates.this.widgetItemModel.getTapX());
                MenuWidgetTapCoordinates.this.setY(MenuWidgetTapCoordinates.this.widgetItemModel.getTapY());
                MenuWidgetTapCoordinates.this.launcherAppWidgetHostView.setScale(((MenuWidgetTapCoordinates.this.widgetItemModel.getScaleFactor() * ((MenuWidgetTapCoordinates.this.launcherAppWidgetHostView.getWidth() - MenuWidgetTapCoordinates.this.launcherAppWidgetHostView.getPaddingRight()) - MenuWidgetTapCoordinates.this.pickerContainer.getPaddingLeft())) / MenuWidgetTapCoordinates.this.widgetWidth) - 1);
            }
        });
        this.tapIndicator = (ImageView) findViewById(R.id.tap_indicator);
        this.tapIndicator.setLayoutParams(new LayoutParams(this.tapIndicatorHeight, this.tapIndicatorWidth));
        this.xSeekBar = (SeekBar) findViewById(R.id.x_seek_bar);
        this.ySeekBar = (SeekBar) findViewById(R.id.y_seek_bar);
    }

    private void initCallbacks() {
        this.xSeekBar.setOnSeekBarChangeListener(this.onSeekBarChangeListener);
        this.ySeekBar.setOnSeekBarChangeListener(this.onSeekBarChangeListener);
    }

    private void setX(int x) {
        int coordX = this.pickerContainer.getWidth() - this.tapIndicatorWidth;
        this.f393x = x;
        this.tapIndicator.setX((float) ((coordX * x) / 100));
        this.xSeekBar.setValue(x);
    }

    private void setY(int y) {
        int coordY = this.pickerContainer.getHeight() - this.tapIndicatorHeight;
        this.f394y = y;
        this.tapIndicator.setY((float) ((coordY * y) / 100));
        this.ySeekBar.setValue(y);
    }

    public int getX() {
        return this.f393x;
    }

    public int getY() {
        return this.f394y;
    }
}
