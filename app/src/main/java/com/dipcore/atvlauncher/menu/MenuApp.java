package com.dipcore.atvlauncher.menu;

import android.content.Context;
import android.content.pm.ApplicationInfo;
import android.util.Log;
import android.view.View;

import com.dipcore.atvlauncher.LauncherActivity;
import com.dipcore.atvlauncher.LauncherApplication;
import com.dipcore.atvlauncher.LauncherConstants;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.dialog.DialogConfirmation;
import com.dipcore.atvlauncher.dialog.DialogCreateFolder;
import com.dipcore.atvlauncher.dialog.DialogNotification;
import com.dipcore.atvlauncher.event.ItemsAdapterAddItemAction;
import com.dipcore.atvlauncher.event.ItemsAdapterRemoveItemAction;
import com.dipcore.atvlauncher.event.StorageSaveItemsAction;
import com.dipcore.atvlauncher.helper.AppHelper;
import com.dipcore.atvlauncher.item.app.AppLauncherItemModel;
import com.dipcore.atvlauncher.item.app.AppViewHolder;
import com.dipcore.atvlauncher.item.folder.FolderLauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherSections;
import com.dipcore.menuitems.Text;
import com.dipcore.sidebar.Sidebar;

import org.greenrobot.eventbus.EventBus;

/**
 * Created by Diana 2018.
 */

public class MenuApp extends Menu {
    private Text configureAppTextItem;
    private View.OnClickListener configureAppTextItemOnClickListener;
    private Text createFolderTextItem;
    private View.OnClickListener createFolderTextItemOnClickListener;
    private AppViewHolder holder;
    private LauncherItemModel item;
    private LauncherSections launcherSections;
    private Text launcherSettingsTextItem;
    private View.OnClickListener launcherSettingsTextItemOnClickListener;
    private AppLauncherItemModel model;
    private Text moveAppTextItem;
    private View.OnClickListener moveAppTextItemOnClickListener;
    private Text moveToFolderTextItem;
    private View.OnClickListener moveToFolderTextItemOnClickListener;
    private OnMoveSelectedListener onMoveSelectedListener;
    private Text removeFromFolderTextItem;
    private View.OnClickListener removeFromFolderTextItemOnClickListener;
    private Text uninstallAppTextItem;
    private View.OnClickListener uninstallAppTextItemOnClickListener;
    private String uuid;

    public interface OnMoveSelectedListener {
        void onMoveSelected(AppLauncherItemModel appLauncherItemModel, AppViewHolder appViewHolder);
    }

    public MenuApp(Context context) {
        this(context, null);
    }

    public MenuApp(Context context, Sidebar parent) {
        super(context, parent);
        this.moveAppTextItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuApp.this.onMoveSelectedListener.onMoveSelected(MenuApp.this.model, MenuApp.this.holder);
                MenuApp.this.dismiss();
            }
        };
        this.uninstallAppTextItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                AppHelper.uninstall(MenuApp.this.getContext(), MenuApp.this.model.getPackageName());
                MenuApp.this.dismiss();
            }
        };
        this.configureAppTextItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuAppConfigure menu = new MenuAppConfigure(MenuApp.this.getContext(), MenuApp.this);
                menu.setModel(MenuApp.this.model);
                menu.setHolder(MenuApp.this.holder);
                menu.show();
            }
        };
        this.launcherSettingsTextItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                new MenuLauncher(MenuApp.this.getContext(), MenuApp.this).show();
            }
        };
        this.createFolderTextItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                dismiss();
                if (launcherSections.getFolders().size() >= 2) {
                    DialogNotification dialog = new DialogNotification(getContext());
                    dialog.setTitle((int) R.string.dialog_max_folders_title);
                    dialog.setContent((int) R.string.dialog_max_folders_notification);
                    dialog.show();
                    return;
                }
                new DialogCreateFolder(getContext()).show();
            }
        };
        this.moveToFolderTextItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                MenuSelectFolder menu = new MenuSelectFolder(getContext(), MenuApp.this);
                menu.setOnItemClickedListener(new MenuSelectFolder.OnItemClickedListener() {
                    @Override
                    public void onClick(View view, String folderUuid, int i) {
                        dismiss();
                        launcherSections.removeItemByUuid(uuid);
                        EventBus.getDefault().post(new ItemsAdapterRemoveItemAction(LauncherConstants.HomeAdapterUuid, uuid));
                        ((FolderLauncherItemModel) launcherSections.getItemByUuid(folderUuid)).addItem(item);
                        EventBus.getDefault().post(new ItemsAdapterRemoveItemAction(LauncherConstants.FolderAdapterUuid, uuid));
                        EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.AppsSectionUuid));
                    }
                });
                menu.show();
            }
        };
        this.removeFromFolderTextItemOnClickListener = new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                DialogConfirmation dialogConfirmation = new DialogConfirmation(getContext());
                dialogConfirmation.setTitle((int) R.string.dialog_delete_application_from_folder_title);
                dialogConfirmation.setContent((int) R.string.dialog_delete_application_from_folder_confirmation);
                dialogConfirmation.show();
                dialogConfirmation.setOnPositiveButtonClickedListener(new DialogConfirmation.OnPositiveButtonClickedListener() {
                    @Override
                    public void onClick(View view) {
                        launcherSections.getFolderByItemUuid(uuid).removeItem(item);
                        EventBus.getDefault().post(new ItemsAdapterRemoveItemAction(LauncherConstants.FolderAdapterUuid, uuid));
                        launcherSections.addItem(LauncherConstants.AppsSectionUuid, item);
                        EventBus.getDefault().post(new ItemsAdapterAddItemAction(LauncherConstants.HomeAdapterUuid, item));
                        EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.AppsSectionUuid));
                    }
                });
                dismiss();
            }
        };
        this.launcherSections = LauncherApplication.getInstance().getLauncherSections();
        init();
    }

    public void setModel(AppLauncherItemModel model) {
        this.model = model;
        setUuid(model.getUuid());
        initItemsVisibility();
    }

    public String getUuid() {
        return this.uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
        this.item = this.launcherSections.getItemByUuid(uuid);
        Log.d("AAA", "model " + this.model);
        Log.d("AAA", "item " + this.item);
    }

    public void setHolder(AppViewHolder holder) {
        this.holder = holder;
    }

    public void setOnMoveSelectedListener(OnMoveSelectedListener onMoveSelectedListener) {
        this.onMoveSelectedListener = onMoveSelectedListener;
    }

    private void init() {
        setContent(R.layout.menu_app);
        setTitle((int) R.string.menu_app_title);
        initItems();
        initCallbacks();
    }

    private void initItems() {
        this.moveAppTextItem = (Text) findViewById(R.id.move_app_text_item);
        this.uninstallAppTextItem = (Text) findViewById(R.id.uninstall_app_text_item);
        this.configureAppTextItem = (Text) findViewById(R.id.configure_app_text_item);
        this.launcherSettingsTextItem = (Text) findViewById(R.id.launcher_settings_text_item);
        this.createFolderTextItem = (Text) findViewById(R.id.create_folder_text_item);
        this.moveToFolderTextItem = (Text) findViewById(R.id.move_to_folder_text_item);
        this.removeFromFolderTextItem = (Text) findViewById(R.id.remove_from_folder_text_item);
    }

    private void initItemsVisibility() {
        ApplicationInfo applicationInfo = AppHelper.getApplicationInfo(getContext(), this.model.getPackageName());
        if (applicationInfo != null) {
            int i;
            boolean isSystemApp = (applicationInfo.flags & 1) != 0;
            Text text = this.uninstallAppTextItem;
            if (isSystemApp) {
                i = 8;
            } else {
                i = 0;
            }
            text.setVisibility(i);
        }
        if (!(getContext() instanceof LauncherActivity) || this.launcherSections.getFolders().size() <= 0) {
            this.moveToFolderTextItem.setVisibility(View.GONE);
        } else {
            this.moveToFolderTextItem.setVisibility(View.VISIBLE);
        }
        if (getContext() instanceof LauncherActivity) {
            this.createFolderTextItem.setVisibility(View.VISIBLE);
            this.removeFromFolderTextItem.setVisibility(View.GONE);
            return;
        }
        this.createFolderTextItem.setVisibility(View.GONE);
        this.removeFromFolderTextItem.setVisibility(View.VISIBLE);
    }

    private void initCallbacks() {
        this.moveAppTextItem.setOnClickListener(this.moveAppTextItemOnClickListener);
        this.uninstallAppTextItem.setOnClickListener(this.uninstallAppTextItemOnClickListener);
        this.configureAppTextItem.setOnClickListener(this.configureAppTextItemOnClickListener);
        this.launcherSettingsTextItem.setOnClickListener(this.launcherSettingsTextItemOnClickListener);
        this.createFolderTextItem.setOnClickListener(this.createFolderTextItemOnClickListener);
        this.moveToFolderTextItem.setOnClickListener(this.moveToFolderTextItemOnClickListener);
        this.removeFromFolderTextItem.setOnClickListener(this.removeFromFolderTextItemOnClickListener);
    }
}

