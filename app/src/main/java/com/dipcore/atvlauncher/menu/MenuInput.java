package com.dipcore.atvlauncher.menu;

import android.content.Context;
import com.dipcore.atvlauncher.R;
import com.dipcore.sidebar.Sidebar;

public class MenuInput extends Menu {
    private final Context context;

    public MenuInput(Context context) {
        this(context, null);
    }


    public MenuInput(Context context, Sidebar parent) {
        super(context, parent);
        this.context = context;
        init();
    }

    private void init() {
        setContent(R.layout.menu_input); // боковое меню слева для usb, hdmi, память устройств
        setTitle((int) R.string.menu_title_inputs);
        initItems();
        initCallbacks();
    }

    private void initItems() {
    }

    private void initCallbacks() {
    }
}
