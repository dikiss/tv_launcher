package com.dipcore.atvlauncher.menu;

import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;
import android.widget.TextView;
import com.dipcore.atvlauncher.R;
import com.dipcore.sidebar.Sidebar;

public class Menu extends Sidebar {
    public Menu(Context context) {
        this(context, null);
    }

    public Menu(Context context, Sidebar parent) {
        super(context, parent);
        init();
    }

    private void init() {
        setTitlePadding(R.dimen.sidebar_title_padding);
        setTitleTextSize(R.dimen.sidebar_title_text_size);
        setWidth(R.dimen.sidebar_width);
        setGravity(5);
        sidebarFooterContainer(R.layout.menu_footer);
        try {
            ((TextView) findViewById(R.id.version)).setText(getContext().getPackageManager().getPackageInfo(getContext().getPackageName(), 0).versionName);
        } catch (NameNotFoundException e) {
            e.printStackTrace();
        }
    }
}
