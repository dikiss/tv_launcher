package com.dipcore.atvlauncher.item.app;

import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import com.dipcore.atvlauncher.ItemsActivity;
import com.dipcore.atvlauncher.LauncherConstants;
import com.dipcore.atvlauncher.event.StorageSaveItemsAction;
import com.dipcore.atvlauncher.helper.AppHelper;
import com.dipcore.atvlauncher.helper.ViewHelper;
import com.dipcore.atvlauncher.item.app.AppViewRenderer.Listener;
import com.dipcore.atvlauncher.menu.MenuApp;
import com.dipcore.atvlauncher.menu.MenuApp.OnMoveSelectedListener;
import com.dipcore.atvlauncher.widget.CardView;
import org.greenrobot.eventbus.EventBus;

public class AppItemListener implements Listener {
    private static final String TAG = AppItemListener.class.getCanonicalName();
    private ItemsActivity activity;
    private OnMoveSelectedListener onMoveSelectedListener = new C03061();
    private AppItemState state = AppItemState.STATE_DEFAULT;

    class C03061 implements OnMoveSelectedListener {
        @Override
        public void onMoveSelected(AppLauncherItemModel model, AppViewHolder holder) {
            AppItemListener.this.state = AppItemState.STATE_MOVE;
            holder.iconMoveImageView.setVisibility(View.VISIBLE);
        }
    }

    public AppItemListener(ItemsActivity activity) {
        this.activity = activity;
    }
    @Override
    public void onItemFocusChange(@NonNull AppLauncherItemModel model, View view, @NonNull boolean focus) {
        if (focus) {
            this.activity.getRecyclerView().invalidate();
        }
    }
    @Override
    public void onItemClick(@NonNull AppLauncherItemModel model, AppViewHolder holder, View view) {
        if (this.state == AppItemState.STATE_DEFAULT) {
            ViewHelper.focusView(view);
            AppHelper.launch(this.activity, model.getPackageName());
        }
    }

    public boolean onItemLongClick(@NonNull AppLauncherItemModel model, AppViewHolder holder, View view) {
        if (this.state == AppItemState.STATE_DEFAULT) {
            ViewHelper.focusView(view);
            showMenu(model, holder);
        }
        return true;
    }

    public boolean onKey(@NonNull AppLauncherItemModel model, AppViewHolder holder, int i, KeyEvent keyEvent) {
        Log.d(TAG, "onKey state = " + this.state);
        if (this.state == AppItemState.STATE_DEFAULT) {
            return onKeyDefault(model, holder, i, keyEvent);
        }
        if (this.state == AppItemState.STATE_MOVE) {
            return onKeyMove(model, holder, i, keyEvent);
        }
        return false;
    }

    private boolean onKeyDefault(AppLauncherItemModel model, AppViewHolder holder, int i, KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() == 1) {
            switch (keyCode) {
                case 82:
                    showMenu(model, holder);
                    return true;
            }
        }
        return false;
    }

    private boolean onKeyMove(AppLauncherItemModel model, AppViewHolder holder, int i, KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() == 1) {
            switch (keyCode) {
                case 4:
                case 23:
                case 66:
                    this.state = AppItemState.STATE_DEFAULT;
                    holder.iconMoveImageView.setVisibility(View.GONE);
                    EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.AppsSectionUuid));
                    break;
                case 19:
                case 20:
                case 21:
                case 22:
                    View focusedView = this.activity.getGridLayoutManager().getFocusedChild();
                    View nextFocusedView = this.activity.getRecyclerView().focusSearch(focusedView, focusDirectionByKeyCode(keyCode));
                    if (nextFocusedView != null && (nextFocusedView instanceof CardView)) {
                        moveGridItemView((CardView) focusedView, (CardView) nextFocusedView);
                        break;
                    }
            }
        }
        return true;
    }

    private int focusDirectionByKeyCode(int keyCode) {
        switch (keyCode) {
            case 19:
                return 33;
            case 20:
                return 130;
            case 21:
                return 17;
            case 22:
                return 66;
            default:
                return -1;
        }
    }

    private void moveGridItemView(CardView fromView, CardView toView) {
        Log.d(TAG, " moveView fromView = " + fromView + " toView = " + toView);
        if (fromView != null && toView != null) {
            String fromUuid = fromView.getItemUuid();
            String toUuid = toView.getItemUuid();
            Log.d(TAG, " moveView fromUuid = " + fromUuid + " toUuid = " + toUuid);
            this.activity.moveItem(fromUuid, toUuid);
        }
    }

    private void showMenu(AppLauncherItemModel model, AppViewHolder holder) {
        MenuApp menu = new MenuApp(this.activity);
        menu.setModel(model);
        menu.setHolder(holder);
        menu.setOnMoveSelectedListener(this.onMoveSelectedListener);
        menu.show();
    }
}
