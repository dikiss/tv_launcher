package com.dipcore.atvlauncher.item.folder;

import com.dipcore.atvlauncher.item.BaseLauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherItemModel;
import java.util.ArrayList;
import java.util.Iterator;

public class FolderLauncherItemModel extends BaseLauncherItemModel {
    public static final int TYPE = 4;
    private ArrayList<LauncherItemModel> items = new ArrayList<>();
    private String title;

    public int getType() {
        return 4;
    }

    public ArrayList<LauncherItemModel> getItems() {
        return this.items;
    }

    public void setItems(ArrayList<LauncherItemModel> items) {
        this.items = items;
    }

    public void addItem(LauncherItemModel item) {
        this.items.add(item);
    }

    public void addItem(int index, LauncherItemModel item) {
        if (index >= this.items.size()) {
            this.items.add(item);
        } else {
            this.items.add(index, item);
        }
    }

    public void removeItem(LauncherItemModel item) {
        this.items.remove(item);
    }

    public void removeItem(int index) {
        if (index < this.items.size()) {
            this.items.remove(index);
        }
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public LauncherItemModel getItem(String uuid) {
        Iterator it = this.items.iterator();
        while (it.hasNext()) {
            LauncherItemModel item = (LauncherItemModel) it.next();
            if (item.getUuid().equals(uuid)) {
                return item;
            }
        }
        return null;
    }
}
