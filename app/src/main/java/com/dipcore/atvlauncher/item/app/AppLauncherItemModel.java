package com.dipcore.atvlauncher.item.app;

import com.dipcore.atvlauncher.item.BaseLauncherItemModel;

public class AppLauncherItemModel extends BaseLauncherItemModel {
    public static final int TYPE = 3;
    private int mainPosition = 0;
    private String packageName;
    private boolean showIcon = true;
    private boolean showTitle = true;
    private String title;
    @Override
    public int getType() {
        return 3;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPackageName() {
        return this.packageName;
    }

    public void setPackageName(String packageName) {
        this.packageName = packageName;
    }

    public boolean isShowIcon() {
        return this.showIcon;
    }

    public void setShowIcon(boolean showIcon) {
        this.showIcon = showIcon;
    }

    public boolean isShowTitle() {
        return this.showTitle;
    }

    public void setShowTitle(boolean showTitle) {
        this.showTitle = showTitle;
    }

    public int getMainPosition() {
        return this.mainPosition;
    }

    public void setMainPosition(int mainPosition) {
        this.mainPosition = mainPosition;
    }
}
