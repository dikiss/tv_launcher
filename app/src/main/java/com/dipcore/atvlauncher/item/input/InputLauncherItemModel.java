package com.dipcore.atvlauncher.item.input;

import android.content.Context;
import com.dipcore.atvlauncher.item.BaseLauncherItemModel;

public class InputLauncherItemModel extends BaseLauncherItemModel {
    public static final int TYPE = 2;
    private String backgroundRes;
    private transient Context context;
    private String title;
    @Override
    public int getType() {
        return 2;
    }

    public String getBackgroundRes() {
        return this.backgroundRes;
    }

    public void setBackgroundRes(String backgroundRes) {
        this.backgroundRes = backgroundRes;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
