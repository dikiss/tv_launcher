package com.dipcore.atvlauncher.item.app;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.dipcore.atvlauncher.R;

public class AppViewHolder extends ViewHolder {
    public ImageView iconImageView;
    public ImageView iconMoveImageView;
    public TextView titleTextView;

    public AppViewHolder(View itemView) {
        super(itemView);
        this.titleTextView = itemView.findViewById(R.id.title_text_view);
        this.iconImageView = itemView.findViewById(R.id.icon_image_view);
        this.iconMoveImageView = itemView.findViewById(R.id.icon_move_image_view);
    }
}
