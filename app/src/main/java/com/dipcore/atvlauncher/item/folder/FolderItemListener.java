package com.dipcore.atvlauncher.item.folder;

import android.content.Intent;
import android.support.annotation.NonNull;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import com.dipcore.atvlauncher.FolderActivity;
import com.dipcore.atvlauncher.ItemsActivity;
import com.dipcore.atvlauncher.LauncherConstants;
import com.dipcore.atvlauncher.event.StorageSaveItemsAction;
import com.dipcore.atvlauncher.helper.ViewHelper;
import com.dipcore.atvlauncher.item.app.AppItemState;
import com.dipcore.atvlauncher.item.folder.FolderViewRenderer.Listener;
import com.dipcore.atvlauncher.menu.MenuFolder;
import com.dipcore.atvlauncher.menu.MenuFolder.OnMoveSelectedListener;
import com.dipcore.atvlauncher.widget.CardView;
import org.greenrobot.eventbus.EventBus;

public class FolderItemListener implements Listener {
    private static final String TAG = FolderItemListener.class.getCanonicalName();
    ItemsActivity activity;
    private OnMoveSelectedListener onMoveSelectedListener = new OnMoveSelectedListener() {
        @Override
        public void onMoveSelected(FolderLauncherItemModel model, FolderViewHolder holder) {
            FolderItemListener.this.state = AppItemState.STATE_MOVE;
            holder.iconMoveImageView.setVisibility(View.VISIBLE);
        }
    };
    private AppItemState state = AppItemState.STATE_DEFAULT;

    public FolderItemListener(ItemsActivity activity) {
        this.activity = activity;
    }
    @Override
    public void onItemClick(@NonNull FolderLauncherItemModel model, FolderViewHolder holder, View view) {
        Intent intent = new Intent(this.activity, FolderActivity.class);
        intent.putExtra("ITEM_UUID", model.getUuid());
        this.activity.startActivity(intent);
    }
    @Override
    public boolean onItemLongClick(@NonNull FolderLauncherItemModel model, FolderViewHolder holder, View view) {
        if (this.state == AppItemState.STATE_DEFAULT) {
            ViewHelper.focusView(view);
            showMenu(model, holder);
        }
        return true;
    }
    @Override
    public void onItemFocusChange(@NonNull FolderLauncherItemModel model, View view, @NonNull boolean focus) {
        if (focus) {
            this.activity.getRecyclerView().invalidate();
        }
    }
    @Override
    public boolean onKey(@NonNull FolderLauncherItemModel model, FolderViewHolder holder, int i, KeyEvent keyEvent) {
        Log.d(TAG, "onKey state = " + this.state);
        if (this.state == AppItemState.STATE_DEFAULT) {
            return onKeyDefault(model, holder, i, keyEvent);
        }
        if (this.state == AppItemState.STATE_MOVE) {
            return onKeyMove(model, holder, i, keyEvent);
        }
        return false;
    }

    private boolean onKeyDefault(FolderLauncherItemModel model, FolderViewHolder holder, int i, KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() == 1) {
            switch (keyCode) {
                case 82 /*82*/:
                    showMenu(model, holder);
                    return true;
            }
        }
        return false;
    }

    private boolean onKeyMove(FolderLauncherItemModel model, FolderViewHolder holder, int i, KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() == 1) {
            switch (keyCode) {
                case 4:
                case 23:
                case 66 /*66*/:
                    this.state = AppItemState.STATE_DEFAULT;
                    holder.iconMoveImageView.setVisibility(View.GONE);
                    EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.AppsSectionUuid));
                    break;
                case 19:
                case 20:
                case 21:
                case 22:
                    View focusedView = this.activity.getGridLayoutManager().getFocusedChild();
                    View nextFocusedView = this.activity.getRecyclerView().focusSearch(focusedView, focusDirectionByKeyCode(keyCode));
                    if (nextFocusedView != null && (nextFocusedView instanceof CardView)) {
                        moveGridItemView((CardView) focusedView, (CardView) nextFocusedView);
                        break;
                    }
            }
        }
        return true;
    }

    private int focusDirectionByKeyCode(int keyCode) {
        switch (keyCode) {
            case 19:
                return 33;
            case 20:
                return 130;
            case 21:
                return 17;
            case 22:
                return 66;
            default:
                return -1;
        }
    }

    private void moveGridItemView(CardView fromView, CardView toView) {
        Log.d(TAG, " moveView fromView = " + fromView + " toView = " + toView);
        if (fromView != null && toView != null) {
            String fromUuid = fromView.getItemUuid();
            String toUuid = toView.getItemUuid();
            Log.d(TAG, " moveView fromUuid = " + fromUuid + " toUuid = " + toUuid);
            this.activity.moveItem(fromUuid, toUuid);
        }
    }

    private void showMenu(FolderLauncherItemModel model, FolderViewHolder holder) {
        MenuFolder menu = new MenuFolder(this.activity);
        menu.setUuid(model.getUuid());
        menu.setModel(model);
        menu.setHolder(holder);
        menu.setOnMoveSelectedListener(this.onMoveSelectedListener);
        menu.show();
    }
}
