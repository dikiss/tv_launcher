package com.dipcore.atvlauncher.item;

import android.content.Context;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherItemModel;
import java.util.UUID;

public class BaseLauncherItemModel implements LauncherItemModel {
    private String backgroundColor;
    private String backgroundImagePath;
    private transient Context context;
    private String sectionUuid;
    private String uuid = UUID.randomUUID().toString();
    private boolean visible = true;

    public int getType() {
        return -1;
    }

    public Context getContext() {
        return this.context;
    }

    public void setContext(Context context) {
        this.context = context;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getUuid() {
        return this.uuid;
    }

    public String getSectionUuid() {
        return this.sectionUuid;
    }

    public void setSectionUuid(String sectionUuid) {
        this.sectionUuid = sectionUuid;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    public boolean isVisible() {
        return this.visible;
    }

    public String getBackgroundColor() {
        return this.backgroundColor;
    }

    public void setBackgroundColor(String color) {
        this.backgroundColor = color;
        this.backgroundImagePath = null;
    }

    public void setBackgroundColor(int color) {
        setBackgroundColor(String.format("#%08X", color & -1));
    }

    public String getBackgroundImage() {
        return this.backgroundImagePath;
    }

    public void setBackgroundImage(String path) {
        this.backgroundImagePath = path;
        this.backgroundColor = null;
    }

    public void resetBackground() {
        this.backgroundImagePath = null;
        this.backgroundColor = null;
    }
}
