package com.dipcore.atvlauncher.item.folder;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.widget.CardView;

public class FolderViewHolder extends ViewHolder {
    public final ImageView iconMoveImageView;
    public final TextView titleTextView;
    public final CardView viewAll;

    public FolderViewHolder(View itemView) {
        super(itemView);
        this.viewAll = (CardView) itemView;
        this.iconMoveImageView = (this.viewAll.findViewById(R.id.icon_move_image_view));
        this.titleTextView = (this.viewAll.findViewById(R.id.title_text_view));
    }
}
