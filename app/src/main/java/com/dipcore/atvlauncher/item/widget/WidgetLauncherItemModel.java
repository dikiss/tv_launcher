package com.dipcore.atvlauncher.item.widget;

import com.dipcore.atvlauncher.helper.WidgetHelper;
import com.dipcore.atvlauncher.item.BaseLauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherAppWidget.LauncherAppWidgetHost;
import com.dipcore.atvlauncher.widget.LauncherAppWidget.LauncherAppWidgetHostView;

public class WidgetLauncherItemModel extends BaseLauncherItemModel {
    public static final int TYPE = 1;
    private transient LauncherAppWidgetHostView appWidgetHostView;
    private int appWidgetId = 0;
    private transient LauncherAppWidgetHost launcherAppWidgetHost;
    private int scaleFactor = 100;
    private int tapX = 50;
    private int tapY = 50;

    public int getType() {
        return 1;
    }

    public int getAppWidgetId() {
        return this.appWidgetId;
    }

    public void setAppWidgetId(int appWidgetId) {
        if (appWidgetId == 0) {
            reset();
        }
        this.appWidgetId = appWidgetId;
    }

    public LauncherAppWidgetHost getLauncherAppWidgetHost() {
        return this.launcherAppWidgetHost;
    }

    public void setLauncherAppWidgetHost(LauncherAppWidgetHost appWidgetHost) {
        this.launcherAppWidgetHost = appWidgetHost;
    }

    public void reset() {
        this.appWidgetId = 0;
        this.appWidgetHostView = null;
        this.tapX = 50;
        this.tapY = 50;
        this.scaleFactor = 100;
        resetBackground();
    }

    public int getTapX() {
        return this.tapX;
    }

    public void setTapX(int tapX) {
        this.tapX = tapX;
    }

    public int getTapY() {
        return this.tapY;
    }

    public void setTapY(int tapY) {
        this.tapY = tapY;
    }

    public int getScaleFactor() {
        return this.scaleFactor;
    }

    public void setScaleFactor(int scaleFactor) {
        this.scaleFactor = scaleFactor;
    }

    public LauncherAppWidgetHostView getLauncherAppWidgetHostView() {
        if (!((this.appWidgetHostView != null && this.appWidgetId == this.appWidgetHostView.getAppWidgetId()) || this.appWidgetId == 0 || this.launcherAppWidgetHost == null)) {
            this.appWidgetHostView = WidgetHelper.createAppWidgetHostView(getContext(), this.launcherAppWidgetHost, this.appWidgetId);
        }
        return this.appWidgetHostView;
    }

    public void setLauncherAppWidgetHostView(LauncherAppWidgetHostView appWidgetHostView) {
        this.appWidgetHostView = appWidgetHostView;
    }
}
