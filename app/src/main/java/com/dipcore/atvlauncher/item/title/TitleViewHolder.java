package com.dipcore.atvlauncher.item.title;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import com.dipcore.atvlauncher.R;

public class TitleViewHolder extends ViewHolder {
    public final TextView mTitleView;
    public final View mViewAll;

    public TitleViewHolder(View itemView) {
        super(itemView);
        this.mViewAll = itemView;
        this.mTitleView = (this.mViewAll.findViewById(R.id.title_text_view));
    }
}
