package com.dipcore.atvlauncher.item.widget;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import com.dipcore.atvlauncher.widget.WidgetView;

public class WidgetViewHolder extends ViewHolder {
    public final WidgetView mViewAll;

    public WidgetViewHolder(View itemView) {
        super(itemView);
        this.mViewAll = (WidgetView) itemView;
    }
}
