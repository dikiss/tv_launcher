package com.dipcore.atvlauncher.item.widget;

import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.View;
import com.dipcore.atvlauncher.LauncherActivity;
import com.dipcore.atvlauncher.event.WidgetPickEvent;
import com.dipcore.atvlauncher.event.WidgetTapEvent;
import com.dipcore.atvlauncher.helper.ViewHelper;
import com.dipcore.atvlauncher.item.widget.WidgetViewRenderer.Listener;
import com.dipcore.atvlauncher.menu.MenuWidget;
import com.dipcore.atvlauncher.widget.WidgetView;
import org.greenrobot.eventbus.EventBus;

public class WidgetItemListener implements Listener {
    private static final String TAG = WidgetItemListener.class.getCanonicalName();
    LauncherActivity activity;

    public WidgetItemListener(LauncherActivity activity) {
        this.activity = activity;
    }
    @Override
    public void onItemClick(@NonNull WidgetLauncherItemModel widgetItemModel, @NonNull WidgetViewHolder holder, @NonNull int position) {
        ViewHelper.focusView(holder.itemView);
        if (widgetItemModel.getAppWidgetId() == 0) {
            EventBus.getDefault().post(new WidgetPickEvent(position));
        } else {
            EventBus.getDefault().post(new WidgetTapEvent(position));
        }
    }
    @Override
    public boolean onItemLongClick(@NonNull WidgetLauncherItemModel model, @NonNull WidgetViewHolder holder, @NonNull int position) {
        ViewHelper.focusView(holder.itemView);
        showMenu(model, holder.itemView, position);
        return true;
    }
    @Override
    public void onItemFocusChange(@NonNull WidgetLauncherItemModel model, @NonNull WidgetViewHolder holder, @NonNull boolean focus, @NonNull int position) {
        if (focus) {
            this.activity.getRecyclerView().invalidate();
        }
    }
    @Override
    public boolean onKey(@NonNull WidgetLauncherItemModel model, @NonNull WidgetViewHolder holder, int i, KeyEvent keyEvent, @NonNull int position) {
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 1 || keyCode != 82) {
            return false;
        }
        showMenu(model, holder.itemView, position);
        return true;
    }

    private void showMenu(WidgetLauncherItemModel model, View view, int position) {
        WidgetView widgetView = (WidgetView) view;
        MenuWidget menu = new MenuWidget(this.activity);
        menu.setWidgetItemModel(model);
        menu.setWidgetView(widgetView);
        menu.setPosition(position);
        menu.show();
    }
}
