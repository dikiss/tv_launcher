package com.dipcore.atvlauncher.item.widget;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.helper.WidgetHelper;
import com.dipcore.atvlauncher.widget.LauncherAppWidget.LauncherAppWidgetHostView;
import com.dipcore.atvlauncher.widget.RatioLayout.RatioDatumMode;
import com.dipcore.atvlauncher.widget.RendererRecyclerView.ViewRenderer;

public class WidgetViewRenderer extends ViewRenderer<WidgetLauncherItemModel, WidgetViewHolder> {
    @NonNull
    private final Listener mListener;

    public interface Listener {
        void onItemClick(@NonNull WidgetLauncherItemModel widgetLauncherItemModel, @NonNull WidgetViewHolder widgetViewHolder, @NonNull int i);

        void onItemFocusChange(@NonNull WidgetLauncherItemModel widgetLauncherItemModel, @NonNull WidgetViewHolder widgetViewHolder, @NonNull boolean z, @NonNull int i);

        boolean onItemLongClick(@NonNull WidgetLauncherItemModel widgetLauncherItemModel, @NonNull WidgetViewHolder widgetViewHolder, @NonNull int i);

        boolean onKey(@NonNull WidgetLauncherItemModel widgetLauncherItemModel, @NonNull WidgetViewHolder widgetViewHolder, @NonNull int i, @NonNull KeyEvent keyEvent, @NonNull int i2);
    }

    public WidgetViewRenderer(int type, Context context, @NonNull Listener listener) {
        super(type, context);
        this.mListener = listener;
    }
    @Override
    public void bindView(@NonNull final WidgetLauncherItemModel model, @NonNull final WidgetViewHolder holder, @NonNull final int position) {
        holder.mViewAll.setRatio(RatioDatumMode.DATUM_WIDTH, 16.0f, 10.0f);
        holder.mViewAll.setCornerRadius(5.0f);
        holder.mViewAll.setOverlayColor(805306368);
        LauncherAppWidgetHostView launcherAppWidgetHostView = null;
        if (model.getAppWidgetId() != 0) {
            launcherAppWidgetHostView = model.getLauncherAppWidgetHostView();
            if (launcherAppWidgetHostView == null) {
                WidgetHelper.createAppWidgetHostView(getContext(), model.getLauncherAppWidgetHost(), model.getAppWidgetId());
                launcherAppWidgetHostView = WidgetHelper.createAppWidgetHostView(getContext(), model.getLauncherAppWidgetHost(), model.getAppWidgetId());
                model.setLauncherAppWidgetHostView(launcherAppWidgetHostView);
            }
        }
        if (launcherAppWidgetHostView != null) {
            holder.mViewAll.setAppWidgetHostView(model.getLauncherAppWidgetHostView());
            holder.mViewAll.setAppWidgetScaleFactor(model.getScaleFactor());
        } else {
            holder.mViewAll.removeAppWidgetHostView();
        }
        holder.mViewAll.setBackgroundColor(model.getBackgroundColor());
        holder.mViewAll.setBackgroundImage(model.getBackgroundImage());
        if (launcherAppWidgetHostView != null) {
            launcherAppWidgetHostView.setOnLongClickListener(new OnLongClickListener() {
                @Override
                public boolean onLongClick(View v) {
                    return WidgetViewRenderer.this.mListener.onItemLongClick(model, holder, position);
                }
            });
        }
        holder.itemView.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return WidgetViewRenderer.this.mListener.onItemLongClick(model, holder, position);
            }
        });
        holder.itemView.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                WidgetViewRenderer.this.mListener.onItemClick(model, holder, position);
            }
        });
        holder.itemView.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                WidgetViewRenderer.this.mListener.onItemFocusChange(model, holder, b, position);
            }
        });
        holder.itemView.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                return WidgetViewRenderer.this.mListener.onKey(model, holder, i, keyEvent, position);
            }
        });
    }

    @NonNull
    public WidgetViewHolder createViewHolder(@Nullable ViewGroup parent) {
        return new WidgetViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_widget, parent, false));
    }
}
