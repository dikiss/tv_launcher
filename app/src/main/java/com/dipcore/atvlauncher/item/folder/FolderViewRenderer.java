package com.dipcore.atvlauncher.item.folder;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.widget.CardView;
import com.dipcore.atvlauncher.widget.RatioLayout.RatioDatumMode;
import com.dipcore.atvlauncher.widget.RendererRecyclerView.ViewRenderer;

public class FolderViewRenderer extends ViewRenderer<FolderLauncherItemModel, FolderViewHolder> {
    private final Context context;
    @NonNull
    private final Listener mListener;

    public interface Listener {
        void onItemClick(@NonNull FolderLauncherItemModel folderLauncherItemModel, FolderViewHolder folderViewHolder, View view);

        void onItemFocusChange(@NonNull FolderLauncherItemModel folderLauncherItemModel, View view, @NonNull boolean z);

        boolean onItemLongClick(@NonNull FolderLauncherItemModel folderLauncherItemModel, FolderViewHolder folderViewHolder, View view);

        boolean onKey(@NonNull FolderLauncherItemModel folderLauncherItemModel, FolderViewHolder folderViewHolder, int i, KeyEvent keyEvent);
    }

    public FolderViewRenderer(int type, Context context, @NonNull Listener listener) {
        super(type, context);
        this.context = context;
        this.mListener = listener;
    }

    public void bindView(@NonNull final FolderLauncherItemModel model, @NonNull final FolderViewHolder holder, @NonNull int position) {
        CardView view = (CardView) holder.itemView;
        view.setItemUuid(model.getUuid());
        view.setFocusable(true);
        view.useOverlayLayer(true);
        view.useShadowLayer(true);
        view.setDuplicateParentStateEnabled(true);
        view.setRatio(RatioDatumMode.DATUM_WIDTH, 16.0f, 9.0f);
        view.setCornerRadius(5.0f);
        view.setShadowRadius(2.5f);
        view.setShadowColorResource(R.color.shadow_color);
        if (model.getTitle() != null) {
            holder.titleTextView.setText(model.getTitle());
        }
        if (model.getBackgroundColor() != null) {
            view.setBackgroundColor(model.getBackgroundColor());
        } else {
            view.setBackgroundImage(model.getBackgroundImage());
        }
        view.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                FolderViewRenderer.this.mListener.onItemClick(model, holder, v);
            }
        });
        view.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                FolderViewRenderer.this.mListener.onItemFocusChange(model, view, b);
            }
        });
        view.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                return FolderViewRenderer.this.mListener.onKey(model, holder, i, keyEvent);
            }
        });
        holder.itemView.setOnLongClickListener(new OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                return FolderViewRenderer.this.mListener.onItemLongClick(model, holder, v);
            }
        });
    }

    @NonNull @Override
    public FolderViewHolder createViewHolder(@Nullable ViewGroup parent) {
        return new FolderViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_folder, parent, false));
    }
}
