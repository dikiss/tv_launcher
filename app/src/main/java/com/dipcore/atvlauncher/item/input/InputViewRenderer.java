package com.dipcore.atvlauncher.item.input;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.View.OnKeyListener;
import android.view.ViewGroup;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.widget.RatioLayout.RatioDatumMode;
import com.dipcore.atvlauncher.widget.RendererRecyclerView.ViewRenderer;

public class InputViewRenderer extends ViewRenderer<InputLauncherItemModel, InputViewHolder> {
    private final Context context;
    @NonNull
    private final Listener mListener;

    public interface Listener {
        void onItemClicked(@NonNull InputLauncherItemModel inputLauncherItemModel, View view);

        void onItemFocusChange(@NonNull InputLauncherItemModel inputLauncherItemModel, View view, @NonNull boolean z);

        boolean onKey(@NonNull InputLauncherItemModel inputLauncherItemModel, View view, int i, KeyEvent keyEvent);
    }

    public InputViewRenderer(int type, Context context, @NonNull Listener listener) {
        super(type, context);
        this.context = context;
        this.mListener = listener;
    }
    @Override
    public void bindView(@NonNull final InputLauncherItemModel model, @NonNull final InputViewHolder holder, @NonNull int position) {
        holder.mViewAll.setFocusable(true);
        holder.mViewAll.useOverlayLayer(true);
        holder.mViewAll.useShadowLayer(true);
        holder.mViewAll.setDuplicateParentStateEnabled(true);
        holder.mViewAll.setRatio(RatioDatumMode.DATUM_WIDTH, 16.0f, 9.0f);
        holder.mViewAll.setCornerRadius(5.0f);
        holder.mViewAll.setShadowRadius(5.0f);
        holder.mViewAll.setShadowColorResource(R.color.shadow_color);
        if (model.getBackgroundRes() != null) {
            holder.mViewAll.setBackgroundImageResource(this.context.getResources().getIdentifier(model.getBackgroundRes(), "mipmap", this.context.getPackageName()));
        }
        if (model.getTitle() != null) {
            holder.textView.setText(model.getTitle());
        }
        holder.mViewAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                InputViewRenderer.this.mListener.onItemClicked(model, v);
            }
        });
        holder.mViewAll.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                InputViewRenderer.this.mListener.onItemFocusChange(model, view, b);
            }
        });
        holder.mViewAll.setOnKeyListener(new OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                return InputViewRenderer.this.mListener.onKey(model, holder.mViewAll, i, keyEvent);
            }
        });
    }

    @NonNull
    public InputViewHolder createViewHolder(@Nullable ViewGroup parent) {
        return new InputViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_input, parent, false));
    }
}
