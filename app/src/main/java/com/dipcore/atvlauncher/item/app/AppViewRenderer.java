package com.dipcore.atvlauncher.item.app;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.helper.AppHelper;
import com.dipcore.atvlauncher.widget.CardView;
import com.dipcore.atvlauncher.widget.RatioLayout.RatioDatumMode;
import com.dipcore.atvlauncher.widget.RendererRecyclerView.ViewRenderer;

public class AppViewRenderer extends ViewRenderer<AppLauncherItemModel, AppViewHolder> {
    @NonNull
    private final Listener mListener;

    public interface Listener {
        void onItemClick(@NonNull AppLauncherItemModel appLauncherItemModel, AppViewHolder appViewHolder, View view);

        void onItemFocusChange(@NonNull AppLauncherItemModel appLauncherItemModel, View view, @NonNull boolean z);

        boolean onItemLongClick(@NonNull AppLauncherItemModel appLauncherItemModel, AppViewHolder appViewHolder, View view);

        boolean onKey(@NonNull AppLauncherItemModel appLauncherItemModel, AppViewHolder appViewHolder, int i, KeyEvent keyEvent);
    }

    public AppViewRenderer(int type, Context context, @NonNull Listener listener) {
        super(type, context);
        this.mListener = listener;
    }
    @Override
    public void bindView(@NonNull final AppLauncherItemModel model, @NonNull final AppViewHolder holder, @NonNull int position) {
        int i = 0;
        CardView view = (CardView) holder.itemView;
        view.setItemUuid(model.getUuid());
        view.setFocusable(true);
        view.useOverlayLayer(true);
        view.useShadowLayer(true);
        view.setDuplicateParentStateEnabled(true);
        view.setRatio(RatioDatumMode.DATUM_WIDTH, 16.0f, 9.0f);
        view.setCornerRadius(5.0f);
        view.setShadowRadius(2.5f);
        view.setShadowColorResource(R.color.shadow_color);
        Drawable banner = null;
        if (VERSION.SDK_INT >= 20) {
            banner = AppHelper.getBanner(getContext(), model.getPackageName());
        }
        if (banner != null) {
            model.setShowTitle(false);
            model.setShowIcon(false);
        }
        holder.iconImageView.setVisibility(model.isShowIcon() ? View.VISIBLE : View.GONE);
        holder.iconImageView.setImageDrawable(AppHelper.getIcon(getContext(), model.getPackageName()));
        TextView textView = holder.titleTextView;
        if (!model.isShowTitle()) {
            i = View.GONE;
        }
        textView.setVisibility(i);
        holder.titleTextView.setText(model.getTitle());
        if (banner != null) {
            view.setBackgroundImageDrawable(banner);
        } else if (model.getBackgroundColor() != null) {
            view.setBackgroundColor(model.getBackgroundColor());
        } else {
            view.setBackgroundImage(model.getBackgroundImage());
        }
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mListener.onItemClick(model, holder, view);
            }
        });
        view.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean z) {
                holder.titleTextView.setSelected(z);
                mListener.onItemFocusChange(model, view, z);
            }
        });
        view.setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View view, int i, KeyEvent keyEvent) {
                return mListener.onKey(model, holder, i, keyEvent);
            }
        });
        view.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                return mListener.onItemLongClick(model, holder, view);
            }
        });
    }

    @NonNull
    public AppViewHolder createViewHolder(@Nullable ViewGroup parent) {
        return new AppViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_app, parent, false));
    }
}
