package com.dipcore.atvlauncher.item.title;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.widget.RendererRecyclerView.ViewRenderer;

public class TitleViewRenderer extends ViewRenderer<TitleLauncherItemModel, TitleViewHolder> {
    public TitleViewRenderer(int type, Context context) {
        super(type, context);
    }
    @Override
    public void bindView(@NonNull TitleLauncherItemModel model, @NonNull TitleViewHolder holder, @NonNull int position) {
        holder.mTitleView.setText(model.getTitle());
    }

    @NonNull @Override
    public TitleViewHolder createViewHolder(@Nullable ViewGroup parent) {
        return new TitleViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_title, parent, false));
    }
}
