package com.dipcore.atvlauncher.item.title;

import android.content.Context;
import com.dipcore.atvlauncher.item.BaseLauncherItemModel;

public class TitleLauncherItemModel extends BaseLauncherItemModel {
    public static final int TYPE = 0;
    private String backgroundColor;
    private String backgroundImage;
    private transient Context context;
    private String title;
    private boolean visibility = true;

    public int getType() {
        return 0;
    }

    public String getTitle() {
        return this.title;
    }

    public void setTitle(String title) {
        this.title = title;
    }
}
