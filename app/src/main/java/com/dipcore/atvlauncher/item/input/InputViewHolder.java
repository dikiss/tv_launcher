package com.dipcore.atvlauncher.item.input;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.TextView;
import com.dipcore.atvlauncher.R;
import com.dipcore.atvlauncher.widget.CardView;

public class InputViewHolder extends ViewHolder {
    public final CardView mViewAll;
    public final TextView textView;

    public InputViewHolder(View itemView) {
        super(itemView);
        this.mViewAll = (CardView) itemView;
        this.textView = (this.mViewAll.findViewById(R.id.textView));
    }
}
