package com.dipcore.atvlauncher.item.input;

import android.support.annotation.NonNull;
import android.view.KeyEvent;
import android.view.View;
import com.dipcore.atvlauncher.LauncherActivity;
import com.dipcore.atvlauncher.item.input.InputViewRenderer.Listener;

public class InputItemListener implements Listener {
    LauncherActivity activity;

    public InputItemListener(LauncherActivity activity) {
        this.activity = activity;
    }
    @Override
    public void onItemClicked(@NonNull InputLauncherItemModel model, View view) {
    }
    @Override
    public void onItemFocusChange(@NonNull InputLauncherItemModel model, View view, @NonNull boolean focus) {
        if (focus) {
            this.activity.getRecyclerView().invalidate();
        }
    }
    @Override
    public boolean onKey(@NonNull InputLauncherItemModel model, View view, int i, KeyEvent keyEvent) {
        int keyCode = keyEvent.getKeyCode();
        if (keyEvent.getAction() != 1 || keyCode == 82) {
        }
        return false;
    }
}
