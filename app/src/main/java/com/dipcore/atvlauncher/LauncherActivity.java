package com.dipcore.atvlauncher;

import android.content.ActivityNotFoundException;
import android.content.Intent;
import android.content.res.Configuration;
import android.net.Uri;
import android.os.Bundle;
import android.provider.MediaStore;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.GridLayoutManager.SpanSizeLookup;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.dipcore.atvlauncher.cache.ImageCache;
import com.dipcore.atvlauncher.decorator.EqualSpacesItemDecoration;
import com.dipcore.atvlauncher.event.LoaderLoadApplicationsAction;
import com.dipcore.atvlauncher.event.LoaderLoadInputsAction;
import com.dipcore.atvlauncher.event.LoaderLoadWidgetsAction;
import com.dipcore.atvlauncher.event.StorageSaveItemsAction;
import com.dipcore.atvlauncher.helper.CompatUtils;
import com.dipcore.atvlauncher.item.app.AppItemListener;
import com.dipcore.atvlauncher.item.app.AppViewRenderer;
import com.dipcore.atvlauncher.item.folder.FolderItemListener;
import com.dipcore.atvlauncher.item.folder.FolderViewRenderer;
import com.dipcore.atvlauncher.item.input.InputItemListener;
import com.dipcore.atvlauncher.item.input.InputViewRenderer;
import com.dipcore.atvlauncher.item.title.TitleViewRenderer;
import com.dipcore.atvlauncher.item.widget.WidgetItemListener;
import com.dipcore.atvlauncher.item.widget.WidgetViewRenderer;
import com.dipcore.atvlauncher.listener.BootEventListener;
import com.dipcore.atvlauncher.listener.ItemsAdapterEventListener;
import com.dipcore.atvlauncher.listener.LoaderEventListener;
import com.dipcore.atvlauncher.listener.StorageEventListener;
import com.dipcore.atvlauncher.listener.WidgetEventListener;
import com.dipcore.atvlauncher.settings.SettingsFactory;
import com.dipcore.atvlauncher.settings.SettingsModel;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherRendererRecyclerViewAdapter;
import com.dipcore.atvlauncher.widget.RecyclerView;
import com.google.gson.GsonBuilder;
import org.greenrobot.eventbus.EventBus;

public class LauncherActivity extends ItemsActivity {
    private static final String TAG = LauncherActivity.class.getCanonicalName();
    int appColSize;
    private BootEventListener bootEventListener;
    int columnNumber;
    private ImageCache imageCache;
    int inputColSize;
    private ItemsAdapterEventListener itemsAdapterEventListener;
    private LoaderEventListener loaderEventListener;
    private SettingsFactory settingsFactory;
    private SettingsModel settingsModel;
    private StorageEventListener storageEventListener;
    int titleColSize;
    int widgetColSize;
    private WidgetEventListener widgetEventListener;



    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_launcher);
        setLauncherSections(LauncherApplication.getInstance().getLauncherSections());
        this.itemsAdapterEventListener = new ItemsAdapterEventListener(this);
        initSettings();
        initImageCache();
        initSettings();
        initRecyclerView();
        this.widgetEventListener = new WidgetEventListener(this);
        this.storageEventListener = new StorageEventListener(this);
        this.loaderEventListener = new LoaderEventListener(this);
        this.bootEventListener = new BootEventListener(this);

        ImageView imageclick = (ImageView) findViewById(R.id.image);
        imageclick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent();
                intent.setAction(android.content.Intent.ACTION_VIEW);
                intent.setType("image/*");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);

            }
        });

        ImageView videoclick = (ImageView) findViewById(R.id.video);
        videoclick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK, MediaStore.Images.Media.EXTERNAL_CONTENT_URI);
                intent.setType("video/*");
                intent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(intent);


            }
        });

        ImageView inetclick = (ImageView) findViewById(R.id.internet);
        inetclick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://www.google.com"));
                startActivity(browserIntent);

            }
        });


        ImageView folderclick = (ImageView) findViewById(R.id.folder);
        folderclick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("*/*");
                startActivityForResult(intent, 1);

            }
        });


        ImageView provclick = (ImageView) findViewById(R.id.prov);
        provclick.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {

                Intent intent = new Intent(Intent.ACTION_PICK);
                intent.setType("*/*");
                startActivityForResult(intent, 1);

            }
        });

    }




        @Override
    protected void onStart() {
        super.onStart();
        this.storageEventListener.register();
        this.widgetEventListener.register();
        this.loaderEventListener.register();
        this.bootEventListener.register();
        this.itemsAdapterEventListener.register();
    }
    @Override
    protected void onStop() {
        this.storageEventListener.unregister();
        this.widgetEventListener.unregister();
        this.loaderEventListener.unregister();
        this.bootEventListener.unregister();
        this.itemsAdapterEventListener.unregister();
        super.onStop();
    }
    @Override
    protected void onResume() {
        loadItems();
        super.onResume();
    }
    @Override
    protected void onPause() {
        saveItems();
        super.onPause();
    }
    @Override
    protected void onDestroy() {
        super.onDestroy();
    }
    @Override
    public void onBackPressed() {
    }
    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        System.out.println("onConfigurationChanged " + new GsonBuilder().setPrettyPrinting().create().toJson((Object) newConfig));
    }

    private void initSettings() {
        int appCols;
        int inputCols;
        int widgetCols;
        this.settingsFactory = new SettingsFactory(getApplicationContext());
        this.settingsModel = this.settingsFactory.getSettings();
        if (this.settingsModel.isAppSectionDisplay()) {
            appCols = this.settingsModel.getAppCols();
        } else {
            appCols = 1;
        }
        if (this.settingsModel.isInputSectionDisplay()) {
            inputCols = this.settingsModel.getInputCols();
        } else {
            inputCols = 1;
        }
        if (this.settingsModel.isWidgetSectionDisplay()) {
            widgetCols = this.settingsModel.getWidgetCols();
        } else {
            widgetCols = 1;
        }
        this.columnNumber = (appCols * inputCols) * widgetCols;
        this.titleColSize = this.columnNumber;
        this.appColSize = inputCols * widgetCols;
        this.inputColSize = appCols * widgetCols;
        this.widgetColSize = appCols * inputCols;
    }

    private void initRecyclerView() {
        final LauncherRendererRecyclerViewAdapter recyclerViewAdapter = new LauncherRendererRecyclerViewAdapter();
        recyclerViewAdapter.setUuid(LauncherConstants.HomeAdapterUuid);
        recyclerViewAdapter.registerRenderer(new TitleViewRenderer(0, this));
        recyclerViewAdapter.registerRenderer(new WidgetViewRenderer(1, this, new WidgetItemListener(this)));
        recyclerViewAdapter.registerRenderer(new InputViewRenderer(2, this, new InputItemListener(this)));
        recyclerViewAdapter.registerRenderer(new AppViewRenderer(3, this, new AppItemListener(this)));
        recyclerViewAdapter.registerRenderer(new FolderViewRenderer(4, this, new FolderItemListener(this)));
        setRecyclerViewAdapter(recyclerViewAdapter);
        this.itemsAdapterEventListener.setRecyclerViewAdapter(recyclerViewAdapter);
        GridLayoutManager gridLayoutManager = new GridLayoutManager(this, this.columnNumber);
        //GridLayoutManager gridLayoutManager = new GridLayoutManager(this, 3, GridLayoutManager.HORIZONTAL, false);
        gridLayoutManager.setSpanSizeLookup(new SpanSizeLookup() {
            @Override
            public int getSpanSize(int position) {
                switch (recyclerViewAdapter.getItemViewType(position)) {
                    case 0:
                        return titleColSize;
                    case 1:
                        return widgetColSize;
                    case 2:
                        return inputColSize;
                    case 3:
                    case 4:

                        return appColSize;
                    default:
                        return 1;
                }
            }
        });
        setGridLayoutManager(gridLayoutManager);
        RecyclerView recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        recyclerView.setLayoutManager(getGridLayoutManager());
        recyclerView.setAdapter(recyclerViewAdapter);
        recyclerView.addItemDecoration(new EqualSpacesItemDecoration(CompatUtils.dp2px(this, 5.0f)));
        setRecyclerView(recyclerView);
    }

    private void initImageCache() {
        this.imageCache = ImageCache.getInstance();
        this.imageCache.initializeCache();
    }

    private void loadItems() {
        if (this.settingsModel.isWidgetSectionDisplay()) {
            EventBus.getDefault().post(new LoaderLoadWidgetsAction());
        }
        if (this.settingsModel.isInputSectionDisplay()) {
            EventBus.getDefault().post(new LoaderLoadInputsAction());
        }
        if (this.settingsModel.isAppSectionDisplay()) {
            EventBus.getDefault().post(new LoaderLoadApplicationsAction());
        }
    }

    private void saveItems() {
        if (this.settingsModel.isWidgetSectionDisplay()) {
            EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.WidgetSectionUuid));
        }
        if (this.settingsModel.isInputSectionDisplay()) {
            EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.InputsSectionUuid));
        }
        if (this.settingsModel.isAppSectionDisplay()) {
            EventBus.getDefault().post(new StorageSaveItemsAction(LauncherConstants.AppsSectionUuid));
        }
    }
}
