package com.dipcore.atvlauncher.settings;

import android.content.Context;
import com.dipcore.atvlauncher.helper.ExportHelper;
import com.google.gson.reflect.TypeToken;
import java.lang.reflect.Type;

public class SettingsFactory {
    private static final String SETTINGS_FILE_NAME = "settings.json";
    private static final Type SETTINGS_TYPE = new C03261().getType();
    private Context context;
    private final ExportHelper exportHelper;

    static class C03261 extends TypeToken<SettingsModel> {
        C03261() {
        }
    }

    public SettingsFactory(Context context) {
        this.context = context;
        this.exportHelper = new ExportHelper(context);
    }

    public SettingsModel getSettings() {
        return (SettingsModel) this.exportHelper.getObject(SETTINGS_FILE_NAME, SETTINGS_TYPE);
    }
}
