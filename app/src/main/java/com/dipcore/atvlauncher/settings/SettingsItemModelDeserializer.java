package com.dipcore.atvlauncher.settings;

import android.content.Context;
import com.dipcore.atvlauncher.item.app.AppLauncherItemModel;
import com.dipcore.atvlauncher.item.folder.FolderLauncherItemModel;
import com.dipcore.atvlauncher.item.input.InputLauncherItemModel;
import com.dipcore.atvlauncher.item.title.TitleLauncherItemModel;
import com.dipcore.atvlauncher.item.widget.WidgetLauncherItemModel;
import com.dipcore.atvlauncher.widget.LauncherRendererRecyclerView.LauncherItemModel;
import com.google.gson.Gson;
import com.google.gson.JsonDeserializationContext;
import com.google.gson.JsonDeserializer;
import com.google.gson.JsonElement;
import com.google.gson.JsonObject;
import com.google.gson.JsonParseException;
import java.lang.reflect.Type;
import java.util.ArrayList;

public class SettingsItemModelDeserializer implements JsonDeserializer<LauncherItemModel> {
    Context context;
    private final Type type;

    public SettingsItemModelDeserializer(Context context, Type type) {
        this.context = context;
        this.type = type;
    }

    public LauncherItemModel deserialize(JsonElement json, Type typeOfT, JsonDeserializationContext jsonDeserializationContext) throws JsonParseException {
        JsonObject jsonObject = json.getAsJsonObject();
        Type itemType = null;
        switch (jsonObject.get("TYPE").getAsInt()) {
            case 0:
                itemType = TitleLauncherItemModel.class;
                break;
            case 1:
                itemType = WidgetLauncherItemModel.class;
                break;
            case 2:
                itemType = InputLauncherItemModel.class;
                break;
            case 3:
                itemType = AppLauncherItemModel.class;
                break;
            case 4:
                ArrayList<LauncherItemModel> items = jsonDeserializationContext.deserialize(jsonObject.get("items"), this.type);
                FolderLauncherItemModel folder = new FolderLauncherItemModel();
                folder.setTitle(jsonObject.get("title").getAsString());
                folder.setVisible(jsonObject.get("visibility").getAsBoolean());
                folder.setItems(items);
                return folder;
        }
        LauncherItemModel typeModel = (LauncherItemModel) new Gson().fromJson(json.toString(), itemType);
        typeModel.setContext(this.context);
        return typeModel;
    }
}
