package com.dipcore.atvlauncher.settings;

public class SettingsModel {
    private int appCols;
    private boolean appSectionDisplay;
    private int inputCols;
    private boolean inputSectionDisplay;
    private int widgetCols;
    private int widgetRows;
    private boolean widgetSectionDisplay;

    public boolean isWidgetSectionDisplay() {
        return this.widgetSectionDisplay;
    }

    public void setWidgetSectionDisplay(boolean widgetSectionDisplay) {
        this.widgetSectionDisplay = widgetSectionDisplay;
    }

    public int getWidgetCols() {
        return this.widgetCols;
    }

    public void setWidgetCols(int widgetCols) {
        this.widgetCols = widgetCols;
    }

    public int getWidgetRows() {
        return this.widgetRows;
    }

    public void setWidgetRows(int widgetRows) {
        this.widgetRows = widgetRows;
    }

    public boolean isInputSectionDisplay() {
        return this.inputSectionDisplay;
    }

    public void setInputSectionDisplay(boolean inputSectionDisplay) {
        this.inputSectionDisplay = inputSectionDisplay;
    }

    public int getInputCols() {
        return this.inputCols;
    }

    public void setInputCols(int inputCols) {
        this.inputCols = inputCols;
    }

    public boolean isAppSectionDisplay() {
        return this.appSectionDisplay;
    }

    public void setAppSectionDisplay(boolean appSectionDisplay) {
        this.appSectionDisplay = appSectionDisplay;
    }

    public int getAppCols() {
        return this.appCols;
    }

    public void setAppCols(int appCols) {
        this.appCols = appCols;
    }
}
