package com.dipcore.menuitems;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.LinearLayout.LayoutParams;
import android.widget.TextView;

import com.dipcore.atvlauncher.R;
import com.dipcore.smoothcheckbox.SmoothCheckBox;
import com.dipcore.smoothcheckbox.SmoothCheckBox.OnCheckedChangeListener;

public class CheckBox extends LinearLayout {
    SmoothCheckBox checkBox;
    TextView detailTextView;
    TextView titleTextView;

    public CheckBox(Context context) {
        this(context, null);
    }

    public CheckBox(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public CheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public CheckBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        View.inflate(context, R.layout.checkbox, this);
        initViews();
        assignCustomAttributes(context, attrs, defStyleAttr);
    }

    private void initViews() {
        this.checkBox = (SmoothCheckBox) findViewById(R.id.checkBox);
        this.titleTextView = (TextView) findViewById(R.id.title_text_view);
        this.detailTextView = (TextView) findViewById(R.id.detail_text_view);
        setClickable(true);
        setFocusable(true);
        setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        setGravity(17);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                CheckBox.this.checkBox.callOnClick();
            }
        });
    }

    private void assignCustomAttributes(Context context, AttributeSet attrs, int defStyle) {
        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MenuItem, defStyle, R.style.MenuItem);
            String str = null;
            float f = 0.0f;
            int i = 0;
            String str2 = null;
            String str3 = null;
            float f2 = 0.0f;
            int i2 = 0;
            String str4 = null;
            int i3 = -2;
            boolean checked = false;
            try {
                str = a.getString(R.styleable.MenuItem_titleText);
                f = (float) a.getDimensionPixelSize(R.styleable.MenuItem_menuItemTitleTextSize, 20);
                i = a.getColor(R.styleable.MenuItem_menuItemTitleTextColor, -1);
                str2 = a.getString(R.styleable.MenuItem_menuItemTitleTextFontFamily);
                str3 = a.getString(R.styleable.MenuItem_detailText);
                f2 = (float) a.getDimensionPixelSize(R.styleable.MenuItem_menuItemSubTitleTextSize, 15);
                i2 = a.getColor(R.styleable.MenuItem_menuItemSubTitleTextColor, -1);
                str4 = a.getString(R.styleable.MenuItem_menuItemSubTitleTextFontFamily);
                i3 = a.getDimensionPixelSize(R.styleable.MenuItem_menuItemCheckboxRadius, -2);
                checked = a.getBoolean(R.styleable.MenuItem_checked, false);
            } catch (Exception e) {
                Log.e("MenuItemCheckBox", "There was an error loading attributes.");
            } finally {
                a.recycle();
            }
            setTitleText(str);
            setTitleTextSize(f);
            setTitleTextColor(i);
            setTitleTextFontFamily(str2);
            setDetailText(str3);
            setDetailTextSize(f2);
            setDetailTextColor(i2);
            setDetailTextFontFamily(str4);
            setCheckboxRadius(i3);
            setChecked(checked);
        }
    }

    public void setTitleText(String text) {
        this.titleTextView.setText(text);
    }

    public void setTitleTextSize(float size) {
        this.titleTextView.setTextSize(0, size);
    }

    public void setTitleTextColor(int color) {
        this.titleTextView.setTextColor(color);
    }

    public void setTitleTextFontFamily(String name) {
        if (name != null) {
            this.titleTextView.setTypeface(Typeface.create(name, Typeface.NORMAL));
        }
    }

    public void setDetailText(String text) {
        if (text != null) {
            this.detailTextView.setVisibility(View.VISIBLE);
            this.detailTextView.setText(text);
        }
    }

    public void setDetailTextSize(float size) {
        this.detailTextView.setTextSize(0, size);
    }

    public void setDetailTextColor(int color) {
        this.detailTextView.setTextColor(color);
    }

    public void setDetailTextFontFamily(String name) {
        if (name != null) {
            this.detailTextView.setTypeface(Typeface.create(name, Typeface.NORMAL));
        }
    }

    public void setChecked(boolean checked) {
        this.checkBox.setChecked(checked);
    }

    public void setChecked(boolean checked, boolean animate) {
        this.checkBox.setChecked(checked, animate);
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        this.checkBox.setOnCheckedChangeListener(listener);
    }

    public void setCheckboxRadius(int radius) {
        this.checkBox.setLayoutParams(new LayoutParams(radius, radius));
    }
}
