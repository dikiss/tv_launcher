package com.dipcore.menuitems;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dipcore.atvlauncher.R;
import com.dipcore.switchbutton.SwitchButton;

public class ToggleSwitch extends LinearLayout {
    private TextView detailTextView;
    private ImageView imageView;
    OnCheckedChangeListener listener;
    private String offText;
    private String onText;
    private TextView titleTextView;
    private SwitchButton toggleSwitch;

    public ToggleSwitch(Context context) {
        this(context, null);
    }

    public ToggleSwitch(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public ToggleSwitch(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.onText = null;
        this.offText = null;
        init(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public ToggleSwitch(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.onText = null;
        this.offText = null;
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        View.inflate(context, R.layout.toggle_switch, this);
        initViews();
        assignCustomAttributes(context, attrs, defStyleAttr);
    }

    private void initViews() {
        this.toggleSwitch = (SwitchButton) findViewById(R.id.toggle_switch);
        this.titleTextView = (TextView) findViewById(R.id.title_text_view);
        this.detailTextView = (TextView) findViewById(R.id.detail_text_view);
        this.imageView = (ImageView) findViewById(R.id.image_view);
        setClickable(true);
        setFocusable(true);
        setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        setGravity(17);
        setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                ToggleSwitch.this.toggleSwitch.toggle();
            }
        });
        this.toggleSwitch.setOnCheckedChangeListener(new OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                ToggleSwitch.this.setChecked(b);
                ToggleSwitch.this.listener.onCheckedChanged(compoundButton, b);
            }
        });
    }

    private void assignCustomAttributes(Context context, AttributeSet attrs, int defStyle) {
        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MenuItem, defStyle, R.style.MenuItem);
            String str = null;
            float f = 0.0f;
            int i = 0;
            String str2 = null;
            String str3 = null;
            float f2 = 0.0f;
            int i2 = 0;
            String str4 = null;
            boolean z = false;
            int imageResId = -1;
            try {
                str = a.getString(R.styleable.MenuItem_titleText);
                f = (float) a.getDimensionPixelSize(R.styleable.MenuItem_menuItemTitleTextSize, 20);
                i = a.getColor(R.styleable.MenuItem_menuItemTitleTextColor, -1);
                str2 = a.getString(R.styleable.MenuItem_menuItemTitleTextFontFamily);
                str3 = a.getString(R.styleable.MenuItem_detailText);
                f2 = (float) a.getDimensionPixelSize(R.styleable.MenuItem_menuItemSubTitleTextSize, 15);
                i2 = a.getColor(R.styleable.MenuItem_menuItemSubTitleTextColor, -1);
                str4 = a.getString(R.styleable.MenuItem_menuItemSubTitleTextFontFamily);
                this.onText = a.getString(R.styleable.MenuItem_onText);
                this.offText = a.getString(R.styleable.MenuItem_offText);
                z = a.getBoolean(R.styleable.MenuItem_checked, false);
                imageResId = a.getResourceId(R.styleable.MenuItem_icon, -1);
            } catch (Exception e) {
                Log.e("MenuItemToggleSwitch", "There was an error loading attributes.");
            } finally {
                a.recycle();
            }
            if (this.offText == null || this.onText == null) {
                this.detailTextView.setVisibility(View.GONE);
            }
            if (imageResId == -1) {
                this.imageView.setVisibility(View.GONE);
            }
            setTitleText(str);
            setTitleTextSize(f);
            setTitleTextColor(i);
            setTitleTextFontFamily(str2);
            setDetailText(str3);
            setDetailTextSize(f2);
            setDetailTextColor(i2);
            setDetailTextFontFamily(str4);
            setChecked(z);
        }
    }

    public void setTitleText(String text) {
        this.titleTextView.setText(text);
    }

    public void setTitleTextSize(float size) {
        this.titleTextView.setTextSize(0, size);
    }

    public void setTitleTextColor(int color) {
        this.titleTextView.setTextColor(color);
    }

    public void setTitleTextFontFamily(String name) {
        if (name != null) {
            this.titleTextView.setTypeface(Typeface.create(name, Typeface.NORMAL));
        }
    }

    public void setDetailText(String text) {
        if (text != null) {
            this.detailTextView.setVisibility(View.VISIBLE);
            this.detailTextView.setText(text);
        }
    }

    public void setDetailTextSize(float size) {
        this.detailTextView.setTextSize(0, size);
    }

    public void setDetailTextColor(int color) {
        this.detailTextView.setTextColor(color);
    }

    public void setDetailTextFontFamily(String name) {
        if (name != null) {
            this.detailTextView.setTypeface(Typeface.create(name, Typeface.NORMAL));
        }
    }

    public void setChecked(boolean checked) {
        if (checked) {
            this.detailTextView.setText(this.onText);
        } else {
            this.detailTextView.setText(this.offText);
        }
        this.toggleSwitch.setChecked(checked);
    }

    public void setCheckedImmediatelyNoEvent(boolean checked) {
        if (checked) {
            this.detailTextView.setText(this.onText);
        } else {
            this.detailTextView.setText(this.offText);
        }
        this.toggleSwitch.setCheckedImmediatelyNoEvent(checked);
    }

    public void setIcon(int resId) {
        this.imageView.setImageResource(resId);
        this.imageView.setVisibility(View.VISIBLE);
    }

    public void setIcon(Drawable drawable) {
        this.imageView.setImageDrawable(drawable);
        this.imageView.setVisibility(View.VISIBLE);
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener listener) {
        this.listener = listener;
    }
}
