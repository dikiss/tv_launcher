package com.dipcore.menuitems;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dipcore.atvlauncher.R;

public class Text extends LinearLayout {
    TextView detailTextView;
    private ImageView imageView;
    TextView titleTextView;

    public Text(Context context) {
        this(context, null);
    }

    public Text(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public Text(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public Text(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr);
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        View.inflate(context, R.layout.text, this);
        initViews();
        assignCustomAttributes(context, attrs, defStyleAttr);
    }

    private void initViews() {
        this.titleTextView = findViewById(R.id.title_text_view);
        this.detailTextView = findViewById(R.id.detail_text_view);
        this.imageView = findViewById(R.id.image_view);
        setClickable(true);
        setFocusable(true);
        setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        setGravity(17);
    }

    private void assignCustomAttributes(Context context, AttributeSet attrs, int defStyle) {
        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MenuItem, defStyle, R.style.MenuItem);
            String str = null;
            float f = 0.0f;
            int i = 0;
            String str2 = null;
            String str3 = null;
            float f2 = 0.0f;
            int i2 = 0;
            String str4 = null;
            int imageResId = -1;
            try {
                str = a.getString(R.styleable.MenuItem_titleText);
                f = (float) a.getDimensionPixelSize(R.styleable.MenuItem_menuItemTitleTextSize, 20);
                i = a.getColor(R.styleable.MenuItem_menuItemTitleTextColor, -1);
                str2 = a.getString(R.styleable.MenuItem_menuItemTitleTextFontFamily);
                str3 = a.getString(R.styleable.MenuItem_detailText);
                f2 = (float) a.getDimensionPixelSize(R.styleable.MenuItem_menuItemSubTitleTextSize, 15);
                i2 = a.getColor(R.styleable.MenuItem_menuItemSubTitleTextColor, -1);
                str4 = a.getString(R.styleable.MenuItem_menuItemSubTitleTextFontFamily);
                imageResId = a.getResourceId(R.styleable.MenuItem_icon, -1);
            } catch (Exception e) {
                Log.e("Menu Item Text", "There was an error loading attributes.");
            } finally {
                a.recycle();
            }
            if (imageResId == -1) {
                this.imageView.setVisibility(View.GONE);
            }
            setTitleText(str);
            setTitleTextSize(f);
            setTitleTextColor(i);
            setTitleTextFontFamily(str2);
            setDetailText(str3);
            setDetailTextSize(f2);
            setDetailTextColor(i2);
            setDetailTextFontFamily(str4);
        }
    }

    public void setTitleText(String text) {
        this.titleTextView.setText(text);
    }

    public void setTitleTextSize(float size) {
        this.titleTextView.setTextSize(0, size);
    }

    public void setTitleTextColor(int color) {
        this.titleTextView.setTextColor(color);
    }

    public void setTitleTextFontFamily(String name) {
        if (name != null) {
            this.titleTextView.setTypeface(Typeface.create(name, Typeface.NORMAL));
        }
    }

    public void setDetailText(String text) {
        if (text != null) {
            this.detailTextView.setVisibility(View.VISIBLE);
            this.detailTextView.setText(text);
            return;
        }
        this.detailTextView.setVisibility(View.GONE);
    }

    public void setDetailTextSize(float size) {
        this.detailTextView.setTextSize(0, size);
    }

    public void setDetailTextColor(int color) {
        this.detailTextView.setTextColor(color);
    }

    public void setDetailTextFontFamily(String name) {
        if (name != null) {
            this.detailTextView.setTypeface(Typeface.create(name, Typeface.NORMAL));
        }
    }

    public void setIcon(int resId) {
        this.imageView.setImageResource(resId);
        this.imageView.setVisibility(View.VISIBLE);
    }

    public void setIcon(Drawable drawable) {
        this.imageView.setImageDrawable(drawable);
        this.imageView.setVisibility(View.VISIBLE);
    }
}
