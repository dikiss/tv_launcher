package com.dipcore.menuitems;

import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Typeface;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.SeekBar.OnSeekBarChangeListener;
import android.widget.TextView;

import com.dipcore.atvlauncher.R;

public class SeekBar extends LinearLayout {
    TextView detailTextView;
    private int maxValue;
    private int minValue;
    OnSeekBarChangeListener onSeekBarChangeListener;
    private android.widget.SeekBar seekBar;
    private int step;
    TextView titleTextView;

    public SeekBar(Context context) {
        this(context, null);
    }

    public SeekBar(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SeekBar(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        init(context, attrs, defStyleAttr);
    }

    @TargetApi(21)
    public SeekBar(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        init(context, attrs, defStyleAttr);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 21) {
            setValue(getValue() - this.step);
            return true;
        } else if (keyCode != 22) {
            return super.onKeyDown(keyCode, event);
        } else {
            setValue(getValue() + this.step);
            return true;
        }
    }

    private void init(Context context, AttributeSet attrs, int defStyleAttr) {
        setOrientation(LinearLayout.VERTICAL);
        View.inflate(context, R.layout.seekbar, this);
        initViews();
        assignCustomAttributes(context, attrs, defStyleAttr);
    }

    public void setTitleText(String text) {
        this.titleTextView.setText(text);
    }

    public void setTitleTextSize(float size) {
        this.titleTextView.setTextSize(0, size);
    }

    public void setTitleTextColor(int color) {
        this.titleTextView.setTextColor(color);
    }

    public void setTitleTextFontFamily(String name) {
        if (name != null) {
            this.titleTextView.setTypeface(Typeface.create(name, Typeface.NORMAL));
        }
    }

    public void setDetailText(String text) {
        if (text != null) {
            this.detailTextView.setVisibility(View.VISIBLE);
            this.detailTextView.setText(text);
        }
    }

    public void setDetailTextSize(float size) {
        this.detailTextView.setTextSize(0, size);
    }

    public void setDetailTextColor(int color) {
        this.detailTextView.setTextColor(color);
    }

    public void setDetailTextFontFamily(String name) {
        if (name != null) {
            this.detailTextView.setTypeface(Typeface.create(name, Typeface.NORMAL));
        }
    }

    public void setValue(int value) {
        if (value > this.maxValue) {
            value = this.maxValue;
        }
        if (value < this.minValue) {
            value = this.minValue;
        }
        this.detailTextView.setText(String.valueOf(value - this.minValue));
        this.seekBar.setProgress(value - this.minValue);
    }

    public int getValue() {
        return this.seekBar.getProgress() + this.minValue;
    }

    public void setMinValue(int minValue) {
        this.minValue = minValue;
    }

    public void setMaxValue(int maxValue) {
        this.maxValue = maxValue;
        this.seekBar.setMax(maxValue - this.minValue);
    }

    public int getStep() {
        return this.step;
    }

    public void setStep(int step) {
        this.step = step;
        this.seekBar.incrementProgressBy(step);
    }

    public void setOnSeekBarChangeListener(OnSeekBarChangeListener onSeekBarChangeListener) {
        this.seekBar.setOnSeekBarChangeListener(onSeekBarChangeListener);
    }

    private void initViews() {
        this.titleTextView = (TextView) findViewById(R.id.title_text_view);
        this.detailTextView = (TextView) findViewById(R.id.detail_text_view);
        this.seekBar = (android.widget.SeekBar) findViewById(R.id.seekBar);
        setClickable(true);
        setFocusable(true);
        setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        setGravity(17);
    }

    private void assignCustomAttributes(Context context, AttributeSet attrs, int defStyle) {
        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MenuItem, defStyle, R.style.MenuItem);
            String str = null;
            float f = 0.0f;
            int i = 0;
            String str2 = null;
            String str3 = null;
            float f2 = 0.0f;
            int i2 = 0;
            String str4 = null;
            int i3 = 0;
            int i4 = 100;
            int i5 = 0;
            int step = 1;
            try {
                str = a.getString(R.styleable.MenuItem_titleText);
                f = (float) a.getDimensionPixelSize(R.styleable.MenuItem_menuItemTitleTextSize, 20);
                i = a.getColor(R.styleable.MenuItem_menuItemTitleTextColor, -1);
                str2 = a.getString(R.styleable.MenuItem_menuItemTitleTextFontFamily);
                str3 = a.getString(R.styleable.MenuItem_detailText);
                f2 = (float) a.getDimensionPixelSize(R.styleable.MenuItem_menuItemSubTitleTextSize, 15);
                i2 = a.getColor(R.styleable.MenuItem_menuItemSubTitleTextColor, -1);
                str4 = a.getString(R.styleable.MenuItem_menuItemSubTitleTextFontFamily);
                i3 = a.getInteger(R.styleable.MenuItem_menuItemvalue, 0);
                i4 = a.getInteger(R.styleable.MenuItem_maxValue, 100);
                i5 = a.getInteger(R.styleable.MenuItem_minValue, 0);
                step = a.getInteger(R.styleable.MenuItem_step, 1);
            } catch (Exception e) {
                Log.e("Menu Item Text", "There was an error loading attributes.");
            } finally {
                a.recycle();
            }
            setTitleText(str);
            setTitleTextSize(f);
            setTitleTextColor(i);
            setTitleTextFontFamily(str2);
            setDetailText(str3);
            setDetailTextSize(f2);
            setDetailTextColor(i2);
            setDetailTextFontFamily(str4);
            setValue(i3);
            setMinValue(i5);
            setMaxValue(i4);
            setStep(step);
        }
    }
}
