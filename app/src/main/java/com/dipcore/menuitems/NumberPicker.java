package com.dipcore.menuitems;

import android.content.Context;
import android.content.res.TypedArray;
import android.util.AttributeSet;
import android.util.Log;
import android.view.KeyEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dipcore.atvlauncher.R;

public class NumberPicker extends LinearLayout {
    TextView detailTextView;
    int maxValue;
    int minValue;
    Button nextButton;
    Button prevButton;
    TextView titleTextView;
    int value;
    TextView valueTextView;

    public NumberPicker(Context context) {
        this(context, null);
    }

    public NumberPicker(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public NumberPicker(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        View.inflate(context, R.layout.number_picker, this);
        initViews();
        assignCustomAttributes(context, attrs);
    }

    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == 21) {
            this.value--;
            if (this.value < this.minValue) {
                this.value = this.minValue;
            }
            this.valueTextView.setText(String.valueOf(this.value));
            return true;
        } else if (keyCode != 22) {
            return super.onKeyDown(keyCode, event);
        } else {
            this.value++;
            if (this.value > this.maxValue) {
                this.value = this.maxValue;
            }
            this.valueTextView.setText(String.valueOf(this.value));
            return true;
        }
    }

    public void setTitleText(String text) {
        this.titleTextView.setText(text);
    }

    public void setDetailText(String text) {
        if (text != null) {
            this.detailTextView.setVisibility(View.VISIBLE);
            this.detailTextView.setText(text);
        }
    }

    private void initViews() {
        this.titleTextView = (TextView) findViewById(R.id.title_text_view);
        this.detailTextView = (TextView) findViewById(R.id.detail_text_view);
        this.nextButton = (Button) findViewById(R.id.nextButton);
        this.prevButton = (Button) findViewById(R.id.prevButton);
        this.valueTextView = (TextView) findViewById(R.id.valueTextView);
        setClickable(true);
        setFocusable(true);
        setDescendantFocusability(ViewGroup.FOCUS_BLOCK_DESCENDANTS);
        setBackgroundResource(R.drawable.item_background);
        int padding = CompatUtils.dp2px(getContext(), 20.0f);
        setPadding(padding, padding, padding, padding);
        setGravity(17);
    }

    private void assignCustomAttributes(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.MenuItemNumberPicker, 0, 0);
            String str = null;
            String str2 = null;
            try {
                str = a.getString(R.styleable.MenuItemNumberPicker_titleText);
                str2 = a.getString(R.styleable.MenuItemNumberPicker_detailText);
                this.value = a.getInt(R.styleable.MenuItemNumberPicker_value, 0);
                this.minValue = a.getInt(R.styleable.MenuItemNumberPicker_minValue, 0);
                this.maxValue = a.getInt(R.styleable.MenuItemNumberPicker_maxValue, 999999);
            } catch (Exception e) {
                Log.e("Menu Item Number Picker", "There was an error loading attributes.");
            } finally {
                a.recycle();
            }
            setTitleText(str);
            setDetailText(str2);
        }
    }
}
