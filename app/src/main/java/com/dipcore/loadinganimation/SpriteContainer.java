package com.dipcore.loadinganimation;

import android.animation.ValueAnimator;
import android.graphics.Canvas;
import android.graphics.Rect;
import com.dipcore.loadinganimation.animation.AnimationUtils;

public abstract class SpriteContainer extends Sprite {
    private int color;
    private Sprite[] sprites = onCreateChild();

    public abstract Sprite[] onCreateChild();

    public SpriteContainer() {
        initCallBack();
        onChildCreated(this.sprites);
    }

    private void initCallBack() {
        if (this.sprites != null) {
            for (Sprite sprite : this.sprites) {
                sprite.setCallback(this);
            }
        }
    }

    public void onChildCreated(Sprite... sprites) {
    }

    public int getChildCount() {
        return this.sprites == null ? 0 : this.sprites.length;
    }

    public Sprite getChildAt(int index) {
        return this.sprites == null ? null : this.sprites[index];
    }

    public void setColor(int color) {
        this.color = color;
        for (int i = 0; i < getChildCount(); i++) {
            getChildAt(i).setColor(color);
        }
    }

    public int getColor() {
        return this.color;
    }

    public void draw(Canvas canvas) {
        super.draw(canvas);
        drawChild(canvas);
    }

    public void drawChild(Canvas canvas) {
        if (this.sprites != null) {
            for (Sprite sprite : this.sprites) {
                int count = canvas.save();
                sprite.draw(canvas);
                canvas.restoreToCount(count);
            }
        }
    }

    protected void drawSelf(Canvas canvas) {
    }

    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        for (Sprite sprite : this.sprites) {
            sprite.setBounds(bounds);
        }
    }

    public void start() {
        super.start();
        AnimationUtils.start(this.sprites);
    }

    public void stop() {
        super.stop();
        AnimationUtils.stop(this.sprites);
    }

    public boolean isRunning() {
        return AnimationUtils.isRunning(this.sprites) || super.isRunning();
    }

    public ValueAnimator onCreateAnimation() {
        return null;
    }
}
