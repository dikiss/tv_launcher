package com.dipcore.loadinganimation;

import android.animation.ValueAnimator;
import android.graphics.Rect;
import com.dipcore.loadinganimation.animation.SpriteAnimatorBuilder;
import org.objectweb.asm.Opcodes;

public class ThreeBounce extends SpriteContainer {

    private class Bounce extends CircleSprite {
        Bounce() {
            setScale(0.0f);
        }

        public ValueAnimator onCreateAnimation() {
            float[] fractions = new float[]{0.0f, 0.4f, 0.8f, 1.0f};
            return new SpriteAnimatorBuilder(this).scale(fractions, 0.0f, 1.0f, 0.0f, 0.0f).duration(1400).easeInOut(fractions).build();
        }
    }

    public Sprite[] onCreateChild() {
        return new Sprite[]{new Bounce(), new Bounce(), new Bounce()};
    }

    public void onChildCreated(Sprite... sprites) {
        super.onChildCreated(sprites);
        sprites[1].setAnimationDelay(Opcodes.IF_ICMPNE);
        sprites[2].setAnimationDelay(320);
    }

    protected void onBoundsChange(Rect bounds) {
        super.onBoundsChange(bounds);
        bounds = clipSquare(bounds);
        int radius = bounds.width() / 8;
        int top = bounds.centerY() - radius;
        int bottom = bounds.centerY() + radius;
        for (int i = 0; i < getChildCount(); i++) {
            int left = ((bounds.width() * i) / 3) + bounds.left;
            getChildAt(i).setDrawBounds(left, top, (radius * 2) + left, bottom);
        }
    }
}
