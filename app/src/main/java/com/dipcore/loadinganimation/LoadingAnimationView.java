package com.dipcore.loadinganimation;

import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.drawable.Drawable;
import android.util.AttributeSet;
import android.util.Log;
import android.view.View;
import android.widget.ProgressBar;

import com.dipcore.atvlauncher.R;

public class LoadingAnimationView extends ProgressBar {
    private int mColor;
    private Sprite mSprite;

    public LoadingAnimationView(Context context) {
        this(context, null);
    }

    public LoadingAnimationView(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public LoadingAnimationView(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        assignCustomAttributes(context, attrs);
        init();
        setIndeterminate(true);
    }

    private void assignCustomAttributes(Context context, AttributeSet attrs) {
        if (attrs != null) {
            TypedArray a = context.getTheme().obtainStyledAttributes(attrs, R.styleable.LoadingAnimation, 0, 0);
            try {
                this.mColor = a.getColor(R.styleable.LoadingAnimation_color, -1);
            } catch (Exception e) {
                Log.e("MenuItemCheckBox", "There was an error loading attributes.");
            } finally {
                a.recycle();
            }
        }
    }

    private void init() {
        setIndeterminateDrawable(new ThreeBounce());
    }

    public void setIndeterminateDrawable(Drawable d) {
        if (d instanceof Sprite) {
            setIndeterminateDrawable((Sprite) d);
            return;
        }
        throw new IllegalArgumentException("this d must be instanceof Sprite");
    }

    public void setIndeterminateDrawable(Sprite d) {
        super.setIndeterminateDrawable(d);
        this.mSprite = d;
        if (this.mSprite.getColor() == 0) {
            this.mSprite.setColor(this.mColor);
        }
        onSizeChanged(getWidth(), getHeight(), getWidth(), getHeight());
        if (getVisibility() == View.VISIBLE) {
            this.mSprite.start();
        }
    }

    public Sprite getIndeterminateDrawable() {
        return this.mSprite;
    }

    public void setColor(int color) {
        this.mColor = color;
        if (this.mSprite != null) {
            this.mSprite.setColor(color);
        }
        invalidate();
    }

    public void unscheduleDrawable(Drawable who) {
        super.unscheduleDrawable(who);
        if (who instanceof Sprite) {
            ((Sprite) who).stop();
        }
    }

    public void onWindowFocusChanged(boolean hasWindowFocus) {
        super.onWindowFocusChanged(hasWindowFocus);
        if (hasWindowFocus && this.mSprite != null && getVisibility() == View.VISIBLE) {
            this.mSprite.start();
        }
    }

    public void onScreenStateChanged(int screenState) {
        super.onScreenStateChanged(screenState);
        if (screenState == 0 && this.mSprite != null) {
            this.mSprite.stop();
        }
    }
}
