package com.dipcore.loadinganimation.animation.interpolator;

import android.animation.TimeInterpolator;
import android.view.animation.Interpolator;

public class KeyFrameInterpolator implements Interpolator {
    private float[] fractions;
    private TimeInterpolator interpolator;

    public static KeyFrameInterpolator easeInOut(float... fractions) {
        KeyFrameInterpolator interpolator = new KeyFrameInterpolator(Ease.inOut(), new float[0]);
        interpolator.setFractions(fractions);
        return interpolator;
    }

    public static KeyFrameInterpolator pathInterpolator(float controlX1, float controlY1, float controlX2, float controlY2, float... fractions) {
        KeyFrameInterpolator interpolator = new KeyFrameInterpolator(PathInterpolatorCompat.create(controlX1, controlY1, controlX2, controlY2), new float[0]);
        interpolator.setFractions(fractions);
        return interpolator;
    }

    public KeyFrameInterpolator(TimeInterpolator interpolator, float... fractions) {
        this.interpolator = interpolator;
        this.fractions = fractions;
    }

    public void setFractions(float... fractions) {
        this.fractions = fractions;
    }

    public synchronized float getInterpolation(float input) {
        float interpolation;
        if (this.fractions.length > 1) {
            for (int i = 0; i < this.fractions.length - 1; i++) {
                float start = this.fractions[i];
                float end = this.fractions[i + 1];
                float duration = end - start;
                if (input >= start && input <= end) {
                    interpolation = (this.interpolator.getInterpolation((input - start) / duration) * duration) + start;
                    break;
                }
            }
        }
        interpolation = this.interpolator.getInterpolation(input);
        return interpolation;
    }
}
