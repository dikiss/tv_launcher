package com.dipcore.smoothcheckbox;

import android.animation.ValueAnimator;
import android.animation.ValueAnimator.AnimatorUpdateListener;
import android.annotation.TargetApi;
import android.content.Context;
import android.content.res.TypedArray;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.Paint.Cap;
import android.graphics.Paint.Style;
import android.graphics.Path;
import android.graphics.Point;
import android.os.Bundle;
import android.os.Parcelable;
import android.util.AttributeSet;
import android.view.View;
import android.view.View.MeasureSpec;
import android.view.View.OnClickListener;
import android.view.animation.LinearInterpolator;
import android.widget.Checkable;

import com.dipcore.atvlauncher.R;

public class SmoothCheckBox extends View implements Checkable {
    private static final int COLOR_CHECKED = Color.parseColor("#FB4846");
    private static final int COLOR_FLOOR_UNCHECKED = Color.parseColor("#DFDFDF");
    private static final int COLOR_TICK = -1;
    private static final int COLOR_UNCHECKED = -1;
    private static final int DEF_ANIM_DURATION = 300;
    private static final int DEF_DRAW_SIZE = 25;
    private static final String KEY_INSTANCE_STATE = "InstanceState";
    private int mAnimDuration;
    private Point mCenterPoint;
    private boolean mChecked;
    private int mCheckedColor;
    private float mDrewDistance;
    private int mFloorColor;
    private Paint mFloorPaint;
    private float mFloorScale;
    private int mFloorUnCheckedColor;
    private float mLeftLineDistance;
    private OnCheckedChangeListener mListener;
    private Paint mPaint;
    private float mRightLineDistance;
    private float mScaleVal;
    private int mStrokeWidth;
    private boolean mTickDrawing;
    private Paint mTickPaint;
    private Path mTickPath;
    private Point[] mTickPoints;
    private int mUnCheckedColor;
    private int mWidth;

    class C01451 implements OnClickListener {
        @Override
        public void onClick(View v) {
            SmoothCheckBox.this.toggle();
            SmoothCheckBox.this.mTickDrawing = false;
            SmoothCheckBox.this.mDrewDistance = 0.0f;
            if (SmoothCheckBox.this.isChecked()) {
                SmoothCheckBox.this.startCheckedAnimation();
            } else {
                SmoothCheckBox.this.startUnCheckedAnimation();
            }
        }
    }

    class C01462 implements Runnable {
        @Override
        public void run() {
            SmoothCheckBox.this.postInvalidate();
        }
    }

    class C01473 implements AnimatorUpdateListener {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            SmoothCheckBox.this.mScaleVal = ((Float) animation.getAnimatedValue());
            SmoothCheckBox.this.mFloorColor = SmoothCheckBox.getGradientColor(SmoothCheckBox.this.mUnCheckedColor, SmoothCheckBox.this.mCheckedColor, 1.0f - SmoothCheckBox.this.mScaleVal);
            SmoothCheckBox.this.postInvalidate();
        }
    }

    class C01484 implements AnimatorUpdateListener {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            SmoothCheckBox.this.mFloorScale = ((Float) animation.getAnimatedValue());
            SmoothCheckBox.this.postInvalidate();
        }
    }

    class C01495 implements AnimatorUpdateListener {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            SmoothCheckBox.this.mScaleVal = ((Float) animation.getAnimatedValue());
            SmoothCheckBox.this.mFloorColor = SmoothCheckBox.getGradientColor(SmoothCheckBox.this.mCheckedColor, SmoothCheckBox.this.mFloorUnCheckedColor, SmoothCheckBox.this.mScaleVal);
            SmoothCheckBox.this.postInvalidate();
        }
    }

    class C01506 implements AnimatorUpdateListener {
        @Override
        public void onAnimationUpdate(ValueAnimator animation) {
            SmoothCheckBox.this.mFloorScale = ((Float) animation.getAnimatedValue());
            SmoothCheckBox.this.postInvalidate();
        }
    }

    class C01517 implements Runnable {
        @Override
        public void run() {
            SmoothCheckBox.this.mTickDrawing = true;
            SmoothCheckBox.this.postInvalidate();
        }
    }

    public interface OnCheckedChangeListener {
        void onCheckedChanged(SmoothCheckBox smoothCheckBox, boolean z);
    }

    public SmoothCheckBox(Context context) {
        this(context, null);
    }

    public SmoothCheckBox(Context context, AttributeSet attrs) {
        this(context, attrs, 0);
    }

    public SmoothCheckBox(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
        this.mScaleVal = 1.0f;
        this.mFloorScale = 1.0f;
        init(attrs);
    }

    @TargetApi(21)
    public SmoothCheckBox(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
        this.mScaleVal = 1.0f;
        this.mFloorScale = 1.0f;
        init(attrs);
    }

    private void init(AttributeSet attrs) {
        TypedArray ta = getContext().obtainStyledAttributes(attrs, R.styleable.SmoothCheckBox);
        int tickColor = ta.getColor(R.styleable.SmoothCheckBox_color_tick, -1);
        this.mAnimDuration = ta.getInt(R.styleable.SmoothCheckBox_duration, DEF_ANIM_DURATION);
        this.mFloorColor = ta.getColor(R.styleable.SmoothCheckBox_color_unchecked_stroke, COLOR_FLOOR_UNCHECKED);
        this.mCheckedColor = ta.getColor(R.styleable.SmoothCheckBox_color_checked, COLOR_CHECKED);
        this.mUnCheckedColor = ta.getColor(R.styleable.SmoothCheckBox_color_unchecked, -1);
        this.mStrokeWidth = ta.getDimensionPixelSize(R.styleable.SmoothCheckBox_stroke_width, CompatUtils.dp2px(getContext(), 0.0f));
        ta.recycle();
        this.mFloorUnCheckedColor = this.mFloorColor;
        this.mTickPaint = new Paint(1);
        this.mTickPaint.setStyle(Style.STROKE);
        this.mTickPaint.setStrokeCap(Cap.ROUND);
        this.mTickPaint.setColor(tickColor);
        this.mFloorPaint = new Paint(1);
        this.mFloorPaint.setStyle(Style.FILL);
        this.mFloorPaint.setColor(this.mFloorColor);
        this.mPaint = new Paint(1);
        this.mPaint.setStyle(Style.FILL);
        this.mPaint.setColor(this.mCheckedColor);
        this.mTickPath = new Path();
        this.mCenterPoint = new Point();
        this.mTickPoints = new Point[3];
        this.mTickPoints[0] = new Point();
        this.mTickPoints[1] = new Point();
        this.mTickPoints[2] = new Point();
        setOnClickListener(new C01451());
    }

    protected Parcelable onSaveInstanceState() {
        Bundle bundle = new Bundle();
        bundle.putParcelable(KEY_INSTANCE_STATE, super.onSaveInstanceState());
        bundle.putBoolean(KEY_INSTANCE_STATE, isChecked());
        return bundle;
    }

    protected void onRestoreInstanceState(Parcelable state) {
        if (state instanceof Bundle) {
            Bundle bundle = (Bundle) state;
            setChecked(bundle.getBoolean(KEY_INSTANCE_STATE));
            super.onRestoreInstanceState(bundle.getParcelable(KEY_INSTANCE_STATE));
            return;
        }
        super.onRestoreInstanceState(state);
    }

    public boolean isChecked() {
        return this.mChecked;
    }

    public void toggle() {
        setChecked(!isChecked());
    }

    public void setChecked(boolean checked) {
        this.mChecked = checked;
        reset();
        invalidate();
        if (this.mListener != null) {
            this.mListener.onCheckedChanged(this, this.mChecked);
        }
    }

    public void setChecked(boolean checked, boolean animate) {
        if (animate) {
            this.mTickDrawing = false;
            this.mChecked = checked;
            this.mDrewDistance = 0.0f;
            if (checked) {
                startCheckedAnimation();
            } else {
                startUnCheckedAnimation();
            }
            if (this.mListener != null) {
                this.mListener.onCheckedChanged(this, this.mChecked);
                return;
            }
            return;
        }
        setChecked(checked);
    }

    private void reset() {
        float f = 1.0f;
        float f2 = 0.0f;
        this.mTickDrawing = true;
        this.mFloorScale = 1.0f;
        if (isChecked()) {
            f = 0.0f;
        }
        this.mScaleVal = f;
        this.mFloorColor = isChecked() ? this.mCheckedColor : this.mFloorUnCheckedColor;
        if (isChecked()) {
            f2 = this.mRightLineDistance + this.mLeftLineDistance;
        }
        this.mDrewDistance = f2;
    }

    private int measureSize(int measureSpec) {
        int defSize = CompatUtils.dp2px(getContext(), 25.0f);
        int specSize = MeasureSpec.getSize(measureSpec);
        switch (MeasureSpec.getMode(measureSpec)) {
            case Integer.MIN_VALUE:
            case 0:
                return Math.min(defSize, specSize);
            case 1073741824:
                return specSize;
            default:
                return 0;
        }
    }

    protected void onMeasure(int widthMeasureSpec, int heightMeasureSpec) {
        super.onMeasure(widthMeasureSpec, heightMeasureSpec);
        setMeasuredDimension(measureSize(widthMeasureSpec), measureSize(heightMeasureSpec));
    }

    protected void onLayout(boolean changed, int left, int top, int right, int bottom) {
        this.mWidth = getMeasuredWidth();
        this.mStrokeWidth = this.mStrokeWidth == 0 ? getMeasuredWidth() / 10 : this.mStrokeWidth;
        this.mStrokeWidth = this.mStrokeWidth > getMeasuredWidth() / 5 ? getMeasuredWidth() / 5 : this.mStrokeWidth;
        this.mStrokeWidth = this.mStrokeWidth < 3 ? 3 : this.mStrokeWidth;
        this.mCenterPoint.x = this.mWidth / 2;
        this.mCenterPoint.y = getMeasuredHeight() / 2;
        this.mTickPoints[0].x = Math.round((((float) getMeasuredWidth()) / 30.0f) * 7.0f);
        this.mTickPoints[0].y = Math.round((((float) getMeasuredHeight()) / 30.0f) * 14.0f);
        this.mTickPoints[1].x = Math.round((((float) getMeasuredWidth()) / 30.0f) * 13.0f);
        this.mTickPoints[1].y = Math.round((((float) getMeasuredHeight()) / 30.0f) * 20.0f);
        this.mTickPoints[2].x = Math.round((((float) getMeasuredWidth()) / 30.0f) * 22.0f);
        this.mTickPoints[2].y = Math.round((((float) getMeasuredHeight()) / 30.0f) * 10.0f);
        this.mLeftLineDistance = (float) Math.sqrt(Math.pow((double) (this.mTickPoints[1].x - this.mTickPoints[0].x), 2.0d) + Math.pow((double) (this.mTickPoints[1].y - this.mTickPoints[0].y), 2.0d));
        this.mRightLineDistance = (float) Math.sqrt(Math.pow((double) (this.mTickPoints[2].x - this.mTickPoints[1].x), 2.0d) + Math.pow((double) (this.mTickPoints[2].y - this.mTickPoints[1].y), 2.0d));
        this.mTickPaint.setStrokeWidth((float) this.mStrokeWidth);
    }

    protected void onDraw(Canvas canvas) {
        drawBorder(canvas);
        drawCenter(canvas);
        drawTick(canvas);
    }

    private void drawCenter(Canvas canvas) {
        this.mPaint.setColor(this.mUnCheckedColor);
        canvas.drawCircle((float) this.mCenterPoint.x, (float) this.mCenterPoint.y, ((float) (this.mCenterPoint.x - this.mStrokeWidth)) * this.mScaleVal, this.mPaint);
    }

    private void drawBorder(Canvas canvas) {
        this.mFloorPaint.setColor(this.mFloorColor);
        canvas.drawCircle((float) this.mCenterPoint.x, (float) this.mCenterPoint.y, ((float) this.mCenterPoint.x) * this.mFloorScale, this.mFloorPaint);
    }

    private void drawTick(Canvas canvas) {
        if (this.mTickDrawing && isChecked()) {
            drawTickPath(canvas);
        }
    }

    private void drawTickPath(Canvas canvas) {
        float step = 3.0f;
        this.mTickPath.reset();
        float stopX;
        float stopY;
        if (this.mDrewDistance < this.mLeftLineDistance) {
            if (((float) this.mWidth) / 20.0f >= 3.0f) {
                step = ((float) this.mWidth) / 20.0f;
            }
            this.mDrewDistance += step;
            stopX = ((float) this.mTickPoints[0].x) + ((((float) (this.mTickPoints[1].x - this.mTickPoints[0].x)) * this.mDrewDistance) / this.mLeftLineDistance);
            stopY = ((float) this.mTickPoints[0].y) + ((((float) (this.mTickPoints[1].y - this.mTickPoints[0].y)) * this.mDrewDistance) / this.mLeftLineDistance);
            this.mTickPath.moveTo((float) this.mTickPoints[0].x, (float) this.mTickPoints[0].y);
            this.mTickPath.lineTo(stopX, stopY);
            canvas.drawPath(this.mTickPath, this.mTickPaint);
            if (this.mDrewDistance > this.mLeftLineDistance) {
                this.mDrewDistance = this.mLeftLineDistance;
            }
        } else {
            this.mTickPath.moveTo((float) this.mTickPoints[0].x, (float) this.mTickPoints[0].y);
            this.mTickPath.lineTo((float) this.mTickPoints[1].x, (float) this.mTickPoints[1].y);
            canvas.drawPath(this.mTickPath, this.mTickPaint);
            if (this.mDrewDistance < this.mLeftLineDistance + this.mRightLineDistance) {
                stopX = ((float) this.mTickPoints[1].x) + ((((float) (this.mTickPoints[2].x - this.mTickPoints[1].x)) * (this.mDrewDistance - this.mLeftLineDistance)) / this.mRightLineDistance);
                stopY = ((float) this.mTickPoints[1].y) - ((((float) (this.mTickPoints[1].y - this.mTickPoints[2].y)) * (this.mDrewDistance - this.mLeftLineDistance)) / this.mRightLineDistance);
                this.mTickPath.reset();
                this.mTickPath.moveTo((float) this.mTickPoints[1].x, (float) this.mTickPoints[1].y);
                this.mTickPath.lineTo(stopX, stopY);
                canvas.drawPath(this.mTickPath, this.mTickPaint);
                if (this.mWidth / 20 >= 3) {
                    step = (float) (this.mWidth / 20);
                }
                this.mDrewDistance += step;
            } else {
                this.mTickPath.reset();
                this.mTickPath.moveTo((float) this.mTickPoints[1].x, (float) this.mTickPoints[1].y);
                this.mTickPath.lineTo((float) this.mTickPoints[2].x, (float) this.mTickPoints[2].y);
                canvas.drawPath(this.mTickPath, this.mTickPaint);
            }
        }
        if (this.mDrewDistance < this.mLeftLineDistance + this.mRightLineDistance) {
            postDelayed(new C01462(), 10);
        }
    }

    private void startCheckedAnimation() {
        ValueAnimator animator = ValueAnimator.ofFloat(new float[]{1.0f, 0.0f});
        animator.setDuration((long) ((this.mAnimDuration / 3) * 2));
        animator.setInterpolator(new LinearInterpolator());
        animator.addUpdateListener(new C01473());
        animator.start();
        ValueAnimator floorAnimator = ValueAnimator.ofFloat(new float[]{1.0f, 0.8f, 1.0f});
        floorAnimator.setDuration((long) this.mAnimDuration);
        floorAnimator.setInterpolator(new LinearInterpolator());
        floorAnimator.addUpdateListener(new C01484());
        floorAnimator.start();
        drawTickDelayed();
    }

    private void startUnCheckedAnimation() {
        ValueAnimator animator = ValueAnimator.ofFloat(new float[]{0.0f, 1.0f});
        animator.setDuration((long) this.mAnimDuration);
        animator.setInterpolator(new LinearInterpolator());
        animator.addUpdateListener(new C01495());
        animator.start();
        ValueAnimator floorAnimator = ValueAnimator.ofFloat(new float[]{1.0f, 0.8f, 1.0f});
        floorAnimator.setDuration((long) this.mAnimDuration);
        floorAnimator.setInterpolator(new LinearInterpolator());
        floorAnimator.addUpdateListener(new C01506());
        floorAnimator.start();
    }

    private void drawTickDelayed() {
        postDelayed(new C01517(), (long) this.mAnimDuration);
    }

    private static int getGradientColor(int startColor, int endColor, float percent) {
        int startA = Color.alpha(startColor);
        int startR = Color.red(startColor);
        int startG = Color.green(startColor);
        int startB = Color.blue(startColor);
        return Color.argb((int) ((((float) startA) * (1.0f - percent)) + (((float) Color.alpha(endColor)) * percent)), (int) ((((float) startR) * (1.0f - percent)) + (((float) Color.red(endColor)) * percent)), (int) ((((float) startG) * (1.0f - percent)) + (((float) Color.green(endColor)) * percent)), (int) ((((float) startB) * (1.0f - percent)) + (((float) Color.blue(endColor)) * percent)));
    }

    public void setOnCheckedChangeListener(OnCheckedChangeListener l) {
        this.mListener = l;
    }
}
