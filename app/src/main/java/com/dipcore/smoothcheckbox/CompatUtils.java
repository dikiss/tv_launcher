package com.dipcore.smoothcheckbox;

import android.content.Context;

public class CompatUtils {
    public static int dp2px(Context context, float dipValue) {
        return (int) ((dipValue * context.getResources().getDisplayMetrics().density) + 0.5f);
    }
}
