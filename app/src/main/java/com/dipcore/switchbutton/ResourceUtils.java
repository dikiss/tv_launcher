package com.dipcore.switchbutton;

import android.content.Context;
import android.content.res.ColorStateList;
import android.graphics.drawable.Drawable;
import android.os.Build.VERSION;

public class ResourceUtils {
    public static Drawable getDrawable(Context context, int id) {
        if (VERSION.SDK_INT >= 21) {
            return context.getResources().getDrawable(id, context.getTheme());
        }
        return context.getResources().getDrawable(id);
    }

    public static ColorStateList getColorStateList(Context context, int id) {
        if (VERSION.SDK_INT >= 21) {
            return context.getResources().getColorStateList(id, context.getTheme());
        }
        return context.getResources().getColorStateList(id);
    }
}
