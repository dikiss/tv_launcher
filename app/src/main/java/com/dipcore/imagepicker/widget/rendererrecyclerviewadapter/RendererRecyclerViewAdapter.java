package com.dipcore.imagepicker.widget.rendererrecyclerviewadapter;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView.Adapter;
import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.ViewGroup;
import java.util.ArrayList;
import java.util.Iterator;

public class RendererRecyclerViewAdapter extends Adapter {
    @NonNull
    private final ArrayList<ItemModel> mItems = new ArrayList();
    @NonNull
    private final ArrayList<ViewRenderer> mRenderers = new ArrayList();

    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Iterator it = this.mRenderers.iterator();
        while (it.hasNext()) {
            ViewRenderer renderer = (ViewRenderer) it.next();
            if (renderer.isSupportType(viewType)) {
                return renderer.createViewHolder(parent);
            }
        }
        throw new RuntimeException("Not supported Item View Type: " + viewType);
    }

    public void registerRenderer(@NonNull ViewRenderer renderer) {
        this.mRenderers.add(renderer);
    }

    public void onBindViewHolder(ViewHolder holder, int position) {
        ItemModel item = getItem(position);
        Iterator it = this.mRenderers.iterator();
        while (it.hasNext()) {
            ViewRenderer renderer = (ViewRenderer) it.next();
            if (renderer.isSupportType(item.getType())) {
                renderer.bindView(item, holder);
                return;
            }
        }
        throw new RuntimeException("Not supported View Holder: " + holder);
    }

    public int getItemViewType(int position) {
        return getItem(position).getType();
    }

    @NonNull
    ItemModel getItem(int position) {
        return (ItemModel) this.mItems.get(position);
    }

    public int getItemCount() {
        return this.mItems.size();
    }

    public void setItems(@NonNull ArrayList<ItemModel> items) {
        this.mItems.clear();
        this.mItems.addAll(items);
    }
}
