package com.dipcore.imagepicker.widget.rendererrecyclerviewadapter;

public interface ItemModel {
    int getType();
}
