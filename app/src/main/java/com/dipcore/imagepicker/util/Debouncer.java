package com.dipcore.imagepicker.util;

import android.os.CountDownTimer;

public class Debouncer {
    private CountDownTimer debounceTimer;
    private Runnable pendingRunnable;

    public void debounce(Runnable runnable, long delayMs) {
        this.pendingRunnable = runnable;
        cancelTimer();
        startTimer(delayMs);
    }

    public void cancel() {
        cancelTimer();
        this.pendingRunnable = null;
    }

    private void startTimer(long updateIntervalMs) {
        if (updateIntervalMs > 0) {
            this.debounceTimer = new CountDownTimer(updateIntervalMs, updateIntervalMs) {
                public void onTick(long millisUntilFinished) {
                }

                public void onFinish() {
                    Debouncer.this.execute();
                }
            };
            this.debounceTimer.start();
            return;
        }
        execute();
    }

    private void cancelTimer() {
        if (this.debounceTimer != null) {
            this.debounceTimer.cancel();
            this.debounceTimer = null;
        }
    }

    private void execute() {
        if (this.pendingRunnable != null) {
            this.pendingRunnable.run();
            this.pendingRunnable = null;
        }
    }
}
