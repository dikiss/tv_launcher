package com.dipcore.imagepicker.factory;

import android.content.Context;
import android.database.Cursor;
import android.graphics.BitmapFactory.Options;
import android.net.Uri;
import android.provider.MediaStore.Images.Media;
import android.provider.MediaStore.Images.Thumbnails;
import com.dipcore.imagepicker.item.category.CategoryItemModel;
import com.dipcore.imagepicker.item.image.ImageItemModel;
import com.dipcore.imagepicker.widget.rendererrecyclerviewadapter.ItemModel;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class MediaStoreFactory {
    private static MediaStoreFactory factory;
    Map<String, ArrayList<ItemModel>> categoryMap;
    private Context context;
    int totalImages;

    public static MediaStoreFactory getInstance() {
        if (factory == null) {
            factory = new MediaStoreFactory();
        }
        return factory;
    }

    public void init(Context context) {
        this.context = context;
    }

    public void scan() {
        this.totalImages = 0;
        this.categoryMap = new HashMap<>();
        Uri baseUri = Media.EXTERNAL_CONTENT_URI;
        String[] projection = new String[]{"_id", "_data", "_display_name", "bucket_display_name", "datetaken", "date_modified", "orientation", "mime_type", "height", "width", "_size"};
        Cursor cur = this.context.getContentResolver().query(baseUri, projection, null, null, "date_modified DESC");
        if (cur != null && cur.moveToFirst()) {
            do {
                long id = cur.getLong(cur.getColumnIndex("_id"));
                String data = cur.getString(cur.getColumnIndex("_data"));
                String name = cur.getString(cur.getColumnIndex("_display_name"));
                String bucket = cur.getString(cur.getColumnIndex("bucket_display_name"));
                long dateTaken = cur.getLong(cur.getColumnIndex("datetaken"));
                long dateModified = cur.getLong(cur.getColumnIndex("date_modified"));
                int orientation = cur.getInt(cur.getColumnIndex("orientation"));
                String mimeType = cur.getString(cur.getColumnIndex("mime_type"));
                int height = cur.getInt(cur.getColumnIndex("height"));
                int width = cur.getInt(cur.getColumnIndex("width"));
                long size = cur.getLong(cur.getColumnIndex("_size"));
                ArrayList<ItemModel> imageList = this.categoryMap.get(bucket);
                if (imageList == null) {
                    imageList = new ArrayList<>();
                    this.categoryMap.put(bucket, imageList);
                }
                imageList.add(new ImageItemModel(data, name, bucket, id, dateTaken, dateModified, mimeType, orientation, height, width, size));
                this.totalImages++;
            } while (cur.moveToNext());
        }
    }

    public ArrayList<ItemModel> getCategoryList() {
        ArrayList<ItemModel> categoryList = new ArrayList<>();
        for (String key : this.categoryMap.keySet()) {
            CategoryItemModel categoryItemModel = new CategoryItemModel(key);
            categoryItemModel.setCount(getImageList(key).size());
            ArrayList<ItemModel> imageList = this.categoryMap.get(key);
            if (imageList.size() > 0) {
                categoryItemModel.setBackgroundImageBitmap(Thumbnails.getThumbnail(this.context.getContentResolver(), ((ImageItemModel) imageList.get(0)).getId(), 1, (Options) null));
            }
            categoryList.add(categoryItemModel);
        }
        return categoryList;
    }

    public ArrayList<ItemModel> getImageList(String name) {
        return this.categoryMap.get(name);
    }

    public int getTotalImagesCount() {
        return this.totalImages;
    }

    public boolean isCategoryExists(String name) {
        return getImageList(name) != null;
    }
}
