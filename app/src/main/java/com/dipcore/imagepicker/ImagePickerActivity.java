package com.dipcore.imagepicker;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build.VERSION;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLayoutChangeListener;
import android.view.animation.AlphaAnimation;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.DecelerateInterpolator;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout.LayoutParams;

import com.dipcore.atvlauncher.R;
import com.dipcore.imagepicker.decorator.EqualSpacesItemDecoration;
import com.dipcore.imagepicker.factory.MediaStoreFactory;
import com.dipcore.imagepicker.item.back.BackItemListener;
import com.dipcore.imagepicker.item.back.BackItemModel;
import com.dipcore.imagepicker.item.back.BackViewRenderer;
import com.dipcore.imagepicker.item.category.CategoryItemListener;
import com.dipcore.imagepicker.item.category.CategoryViewRenderer;
import com.dipcore.imagepicker.item.image.ImageItemListener;
import com.dipcore.imagepicker.item.image.ImageItemModel;
import com.dipcore.imagepicker.item.image.ImageViewRenderer;
import com.dipcore.imagepicker.util.Debouncer;
import com.dipcore.imagepicker.widget.RecyclerView;
import com.dipcore.imagepicker.widget.rendererrecyclerviewadapter.ItemModel;
import com.dipcore.imagepicker.widget.rendererrecyclerviewadapter.RendererRecyclerViewAdapter;
import com.squareup.picasso.Callback;
import com.squareup.picasso.Picasso;
import java.util.ArrayList;

public class ImagePickerActivity extends Activity {
    private static final int PERMISSIONS_REQUEST_READ_EXTERNAL_STORAGE = 1001;
    private static final String TAG = ImagePickerActivity.class.getCanonicalName();
    private Button backButton;
    OnClickListener backButtonOnClickListener = new C01245();
    private String categoryName;
    private Debouncer debouncer = new Debouncer();
    private boolean focused = false;
    private Button grantButton;
    OnClickListener grantButtonOnClickListener = new C01234();
    private ImageView imageView;
    private LinearLayoutManager mLayoutManager;
    private MediaStoreFactory mMediaStoreFactory;
    private RecyclerView mRecyclerView;
    @NonNull
    private final OnLayoutChangeListener mRecyclerViewLayoutChangeListener = new OnLayoutChangeListener() {
        @Override
        public void onLayoutChange(View view, int i, int i1, int i2, int i3, int i4, int i5, int i6, int i7) {
            if (!ImagePickerActivity.this.focused) {
                ImagePickerActivity.this.mRecyclerView.requestFocus();
                ImagePickerActivity.this.focused = true;
            }
        }
    };
    private RendererRecyclerViewAdapter mRendererRecyclerViewAdapter;
    private LinearLayout permissionDeniedLinearLayout;
    private ImageView placeHolderImageView;
    private boolean preview;

    class C01234 implements OnClickListener {
        @Override
        public void onClick(View v) {
            if (VERSION.SDK_INT > 22) {
                ImagePickerActivity.this.requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, 1001);
            }
        }
    }

    class C01245 implements OnClickListener {
        @Override
        public void onClick(View v) {
            ImagePickerActivity.this.finish();
        }
    }

    /*class C03272 implements Callback {

        class C01211 implements AnimationListener {
            @Override
            public void onAnimationStart(Animation animation) {
            }
            @Override
            public void onAnimationEnd(Animation animation) {
                ImagePickerActivity.this.placeHolderImageView.setImageDrawable(ImagePickerActivity.this.imageView.getDrawable());
            }
            @Override
            public void onAnimationRepeat(Animation animation) {
            }
        }
        @Override
        public void onSuccess() {
            Animation fadeOut = new AlphaAnimation(0.0f, 1.0f);
            fadeOut.setInterpolator(new DecelerateInterpolator());
            fadeOut.setDuration(300);
            ImagePickerActivity.this.imageView.startAnimation(fadeOut);
            fadeOut.setAnimationListener(new C01211());
        }
        @Override
        public void onError() {
        }
    }*/

    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.preview = getIntent().getBooleanExtra("PREVIEW", true);
        init();
        initPermissions();
    }

    public void onBackPressed() {
        if (this.categoryName == null) {
            super.onBackPressed();
        } else {
            renderCategoryItems();
        }
    }

    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case 1001:
                if (grantResults.length <= 0 || grantResults[0] != 0) {
                    this.mRecyclerView.setVisibility(View.GONE);
                    this.permissionDeniedLinearLayout.setVisibility(View.VISIBLE);
                    this.grantButton.setFocusableInTouchMode(true);
                    this.grantButton.requestFocus();
                    this.grantButton.setFocusableInTouchMode(false);
                    return;
                }
                displayMedia();
                return;
            default:
                return;
        }
    }

    public RecyclerView getRecyclerView() {
        return this.mRecyclerView;
    }

    public RendererRecyclerViewAdapter getRendererRecyclerViewAdapter() {
        return this.mRendererRecyclerViewAdapter;
    }

    public void renderCategoryItems() {
        this.categoryName = null;
        this.focused = false;
        ArrayList<ItemModel> list = new ArrayList();
        list.addAll(this.mMediaStoreFactory.getCategoryList());
        this.mRendererRecyclerViewAdapter.setItems(list);
        this.mRendererRecyclerViewAdapter.notifyDataSetChanged();
    }

    public void renderImageItems(String name) {
        this.categoryName = name;
        this.focused = false;
        ArrayList<ItemModel> imageList = this.mMediaStoreFactory.getImageList(name);
        ArrayList<ItemModel> list = new ArrayList<>();
        list.add(new BackItemModel());
        list.addAll(imageList);
        this.mRendererRecyclerViewAdapter.setItems(list);
        this.mRendererRecyclerViewAdapter.notifyDataSetChanged();
    }

    public void showImageModelDenounced(final ImageItemModel model, int delayMs) {
        if (this.preview) {
            this.debouncer.debounce(new Runnable() {
                public void run() {
                    ImagePickerActivity.this.showImageModel(model);
                }
            }, (long) delayMs);
        }
    }

    public void showImageModel(ImageItemModel model) {
        //Picasso.with(this).load(model.getUri()).placeholder(this.imageView.getDrawable()).fit().noFade().into(this.imageView, new C03272());
    }

    public void resetImageModel() {
    }

    public void setImage(ImageItemModel model) {
        Intent intent = new Intent();
        intent.putExtra("uri", model.getUri().toString());
        setResult(-1, intent);
        finish();
    }

    private void init() {
        setContentView(R.layout.image_picker_activity);
        this.imageView = (ImageView) findViewById(R.id.background_image);
        this.placeHolderImageView = (ImageView) findViewById(R.id.background_image2);
        this.mRecyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        if (this.preview) {
            this.mLayoutManager = new LinearLayoutManager(this, 0, false);
            this.mRecyclerView.setLayoutParams(new LayoutParams(-1, -2));
        } else {
            this.mLayoutManager = new GridLayoutManager(this, 5);
            this.mRecyclerView.setLayoutParams(new LayoutParams(-1, -1));
        }
        this.mRendererRecyclerViewAdapter = new RendererRecyclerViewAdapter();
        this.mRendererRecyclerViewAdapter.registerRenderer(new CategoryViewRenderer(0, this, new CategoryItemListener(this)));
        this.mRendererRecyclerViewAdapter.registerRenderer(new ImageViewRenderer(1, this, new ImageItemListener(this)));
        this.mRendererRecyclerViewAdapter.registerRenderer(new BackViewRenderer(2, this, new BackItemListener(this)));
        this.mRecyclerView.setLayoutManager(this.mLayoutManager);
        this.mRecyclerView.setAdapter(this.mRendererRecyclerViewAdapter);
        this.mRecyclerView.addItemDecoration(new EqualSpacesItemDecoration(10));
        this.mRecyclerView.addOnLayoutChangeListener(this.mRecyclerViewLayoutChangeListener);
        this.mMediaStoreFactory = MediaStoreFactory.getInstance();
        this.mMediaStoreFactory.init(this);
        this.permissionDeniedLinearLayout = (LinearLayout) findViewById(R.id.permission_denied_linear_layout);
        this.grantButton = (Button) findViewById(R.id.grant_button);
        this.backButton = (Button) findViewById(R.id.back_button);
        this.grantButton.setOnClickListener(this.grantButtonOnClickListener);
        this.backButton.setOnClickListener(this.backButtonOnClickListener);
    }

    private void initPermissions() {
        if (VERSION.SDK_INT <= 22 || checkSelfPermission("android.permission.READ_EXTERNAL_STORAGE") == PackageManager.PERMISSION_GRANTED) {
            displayMedia();
            return;
        }
        requestPermissions(new String[]{"android.permission.READ_EXTERNAL_STORAGE"}, 1001);
    }

    private void displayMedia() {
        this.mRecyclerView.setVisibility(View.VISIBLE);
        this.permissionDeniedLinearLayout.setVisibility(View.GONE);
        this.mMediaStoreFactory.scan();
        renderCategoryItems();
    }
}
