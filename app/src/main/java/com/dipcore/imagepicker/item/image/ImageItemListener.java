package com.dipcore.imagepicker.item.image;

import android.support.annotation.NonNull;
import android.view.View;
import com.dipcore.imagepicker.ImagePickerActivity;
import com.dipcore.imagepicker.item.image.ImageViewRenderer.Listener;

public class ImageItemListener implements Listener {
    private ImagePickerActivity activity;

    public ImageItemListener(ImagePickerActivity activity) {
        this.activity = activity;
    }
    @Override
    public void onItemClicked(@NonNull ImageItemModel model, @NonNull View view) {
        this.activity.setImage(model);
    }
    @Override
    public void onItemFocusChange(@NonNull ImageItemModel model, @NonNull View view, @NonNull boolean focus) {
        if (focus) {
            this.activity.getRecyclerView().invalidate();
            this.activity.showImageModelDenounced(model, 300);
        }
    }
}
