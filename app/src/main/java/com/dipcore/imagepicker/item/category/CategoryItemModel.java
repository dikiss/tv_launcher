package com.dipcore.imagepicker.item.category;

import android.graphics.Bitmap;
import android.support.annotation.NonNull;
import com.dipcore.imagepicker.widget.rendererrecyclerviewadapter.ItemModel;

public class CategoryItemModel implements ItemModel {
    public static final int TYPE = 0;
    private Bitmap bitmap;
    private int mCount;
    @NonNull
    private String mName;

    public CategoryItemModel(@NonNull String name) {
        this.mName = name;
        this.mCount = 0;
    }

    public CategoryItemModel(@NonNull String name, int count) {
        this.mName = name;
        this.mCount = count;
    }
    @Override
    public int getType() {
        return 0;
    }

    @NonNull
    public String getName() {
        return this.mName;
    }

    public void setName(@NonNull String mName) {
        this.mName = mName;
    }

    public int getCount() {
        return this.mCount;
    }

    public void setCount(int mCount) {
        this.mCount = mCount;
    }

    public void setBackgroundImageBitmap(Bitmap bitmap) {
        this.bitmap = bitmap;
    }

    public Bitmap getBackgroundImageBitmap() {
        return this.bitmap;
    }
}
