package com.dipcore.imagepicker.item.image;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dipcore.atvlauncher.R;

public class ImageViewHolder extends ViewHolder {
    public final ImageView mImageView;
    public final TextView mTextView;
    public final View mViewAll;

    public ImageViewHolder(View itemView) {
        super(itemView);
        this.mImageView = (ImageView) itemView.findViewById(R.id.imageView);
        this.mTextView = (TextView) itemView.findViewById(R.id.textView);
        this.mViewAll = itemView;
    }
}
