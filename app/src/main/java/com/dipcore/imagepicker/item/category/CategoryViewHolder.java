package com.dipcore.imagepicker.item.category;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.dipcore.atvlauncher.R;

public class CategoryViewHolder extends ViewHolder {
    public final TextView mCount;
    public final ImageView mImageView;
    public final TextView mTitle;
    public final View mViewAll;

    public CategoryViewHolder(View itemView) {
        super(itemView);
        this.mTitle = (TextView) itemView.findViewById(R.id.title);
        this.mCount = (TextView) itemView.findViewById(R.id.count);
        this.mImageView = (ImageView) itemView.findViewById(R.id.imageView);
        this.mViewAll = itemView;
    }
}
