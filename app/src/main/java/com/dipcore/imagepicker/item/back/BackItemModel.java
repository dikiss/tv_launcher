package com.dipcore.imagepicker.item.back;

import com.dipcore.imagepicker.widget.rendererrecyclerviewadapter.ItemModel;

public class BackItemModel implements ItemModel {
    public static final int TYPE = 2;
    @Override
    public int getType() {
        return 2;
    }
}
