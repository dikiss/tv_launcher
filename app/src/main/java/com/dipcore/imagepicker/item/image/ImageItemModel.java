package com.dipcore.imagepicker.item.image;

import android.net.Uri;
import android.provider.MediaStore.Images.Media;
import com.dipcore.imagepicker.widget.rendererrecyclerviewadapter.ItemModel;
import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.Date;

public class ImageItemModel implements ItemModel, Serializable {
    public static final int TYPE = 1;
    private String bucket;
    private long bytes;
    private String data;
    private long dateModified;
    private long dateTaken;
    private int height;
    private long id;
    private String mimeType;
    private String name;
    private int orientation;
    private int width;

    public ImageItemModel(String name) {
        this.name = name;
    }

    public ImageItemModel(String data, String name, String bucket, long id, long dateTaken, long dateModified, String mimeType, int orientation, int height, int width, long bytes) {
        this.data = data;
        this.name = name;
        this.bucket = bucket;
        this.id = id;
        this.dateTaken = dateTaken;
        this.dateModified = dateModified;
        this.mimeType = mimeType;
        this.orientation = orientation;
        this.height = height;
        this.width = width;
        this.bytes = bytes;
    }

    public int getType() {
        return 1;
    }

    public String getData() {
        return this.data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getBucket() {
        return this.bucket;
    }

    public void setBucket(String bucket) {
        this.bucket = bucket;
    }

    public long getId() {
        return this.id;
    }

    public void setId(long id) {
        this.id = id;
    }

    public long getDateTaken() {
        return this.dateTaken;
    }

    public String getDateTaken(String format) {
        return dateFormat(this.dateTaken, format);
    }

    public void setDateTaken(long dateTaken) {
        this.dateTaken = dateTaken;
    }

    public long getDateModified() {
        return this.dateModified;
    }

    public String getDateModified(String format) {
        return dateFormat(this.dateModified * 1000, format);
    }

    public void setDateModified(long dateModified) {
        this.dateModified = dateModified;
    }

    public String getMimeType() {
        return this.mimeType;
    }

    public void setMimeType(String mimeType) {
        this.mimeType = mimeType;
    }

    public int getOrientation() {
        return this.orientation;
    }

    public void setOrientation(int orientation) {
        this.orientation = orientation;
    }

    public int getHeight() {
        return this.height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public int getWidth() {
        return this.width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public long getBytes() {
        return this.bytes;
    }

    public void setBytes(long bytes) {
        this.bytes = bytes;
    }

    public String getSizeHumanReadable() {
        return humanReadableByteCount(this.bytes, true);
    }

    public Uri getUri() {
        return Uri.withAppendedPath(Media.EXTERNAL_CONTENT_URI, Long.toString(this.id));
    }

    private String humanReadableByteCount(long bytes, boolean si) {
        int unit = si ? 1000 : 1024;
        if (bytes < ((long) unit)) {
            return bytes + " B";
        }
        String pre = (si ? "kMGTPE" : "KMGTPE").charAt(((int) (Math.log((double) bytes) / Math.log((double) unit))) - 1) + (si ? "" : "i");
        return String.format("%.1f %sB", ((double) bytes) / Math.pow((double) unit, (double) ((int) (Math.log((double) bytes) / Math.log((double) unit)))), pre);
    }

    private String dateFormat(long date, String format) {
        return new SimpleDateFormat(format).format(new Date(date));
    }
}
