package com.dipcore.imagepicker.item.image;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;

import com.dipcore.atvlauncher.R;
import com.dipcore.imagepicker.util.Debouncer;
import com.dipcore.imagepicker.widget.rendererrecyclerviewadapter.ViewRenderer;
import com.squareup.picasso.Picasso;

public class ImageViewRenderer extends ViewRenderer<ImageItemModel, ImageViewHolder> {
    final Debouncer debouncer = new Debouncer();
    private int height;
    @NonNull
    private final Listener mListener;
    private int width;

    public interface Listener {
        void onItemClicked(@NonNull ImageItemModel imageItemModel, View view);

        void onItemFocusChange(@NonNull ImageItemModel imageItemModel, View view, @NonNull boolean z);
    }

    public ImageViewRenderer(int type, Context context, @NonNull Listener listener) {
        super(type, context);
        this.mListener = listener;
        this.width = ((int) getContext().getResources().getDimension(R.dimen.image_picker_item_width)) * 2;
        this.height = ((int) getContext().getResources().getDimension(R.dimen.image_picker_item_height)) * 2;
    }

    public void bindView(@NonNull final ImageItemModel model, @NonNull final ImageViewHolder holder) {
        holder.mViewAll.setFocusable(true);
        holder.mViewAll.setDuplicateParentStateEnabled(true);
        //Picasso.with(getContext()).load(model.getUri()).resize(this.width, this.height).centerCrop().into(holder.mImageView);
        holder.mViewAll.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                ImageViewRenderer.this.mListener.onItemClicked(model, v);
            }
        });
        holder.mViewAll.setOnFocusChangeListener(new OnFocusChangeListener() {
            public void onFocusChange(View view, boolean b) {
                ImageViewRenderer.this.mListener.onItemFocusChange(model, view, b);
                if (b) {
                    ImageViewRenderer.this.showHint(holder.mTextView);
                    ImageViewRenderer.this.animIn(view);
                    return;
                }
                ImageViewRenderer.this.hideHint(holder.mTextView);
                ImageViewRenderer.this.animOut(view);
            }
        });
    }

    @NonNull
    public ImageViewHolder createViewHolder(@Nullable ViewGroup parent) {
        return new ImageViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_image, parent, false));
    }

    private void animOut(View view) {
        view.animate().scaleX(1.0f).scaleY(1.0f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
        view.findViewById(R.id.overlay).animate().alpha(1.0f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
    }

    private void animIn(View view) {
        view.animate().scaleX(1.2f).scaleY(1.2f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
        view.findViewById(R.id.overlay).animate().alpha(0.0f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
    }

    private void showHint(final View view) {
        this.debouncer.debounce(new Runnable() {
            public void run() {
                view.setVisibility(View.VISIBLE);
            }
        }, 300);
    }

    private void hideHint(View view) {
        view.setVisibility(View.GONE);
    }
}
