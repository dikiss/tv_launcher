package com.dipcore.imagepicker.item.back;

import android.support.annotation.NonNull;
import android.view.View;
import com.dipcore.imagepicker.ImagePickerActivity;
import com.dipcore.imagepicker.item.back.BackViewRenderer.Listener;

public class BackItemListener implements Listener {
    private ImagePickerActivity activity;

    public BackItemListener(ImagePickerActivity activity) {
        this.activity = activity;
    }
    @Override
    public void onItemClicked(@NonNull BackItemModel model, @NonNull View view) {
        this.activity.resetImageModel();
        this.activity.renderCategoryItems();
    }
    @Override
    public void onItemFocusChange(@NonNull BackItemModel model, @NonNull View view, @NonNull boolean focus) {
        if (focus) {
            this.activity.getRecyclerView().invalidate();
        }
    }
}
