package com.dipcore.imagepicker.item.category;

import android.support.annotation.NonNull;
import android.view.View;
import com.dipcore.imagepicker.ImagePickerActivity;
import com.dipcore.imagepicker.factory.MediaStoreFactory;
import com.dipcore.imagepicker.item.category.CategoryViewRenderer.Listener;

public class CategoryItemListener implements Listener {
    private ImagePickerActivity activity;

    public CategoryItemListener(ImagePickerActivity activity) {
        this.activity = activity;
    }
    @Override
    public void onItemClicked(@NonNull CategoryItemModel model, @NonNull View view) {
        String name = model.getName();
        if (MediaStoreFactory.getInstance().isCategoryExists(name)) {
            this.activity.renderImageItems(name);
        } else {
            this.activity.renderCategoryItems();
        }
    }
    @Override
    public void onItemFocusChange(@NonNull CategoryItemModel model, @NonNull View view, @NonNull boolean focus) {
        if (focus) {
            this.activity.getRecyclerView().invalidate();
        }
    }
}
