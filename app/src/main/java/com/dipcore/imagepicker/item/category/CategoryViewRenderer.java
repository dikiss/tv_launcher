package com.dipcore.imagepicker.item.category;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnFocusChangeListener;
import android.view.ViewGroup;
import android.view.animation.DecelerateInterpolator;

import com.dipcore.atvlauncher.R;
import com.dipcore.imagepicker.widget.rendererrecyclerviewadapter.ViewRenderer;

public class CategoryViewRenderer extends ViewRenderer<CategoryItemModel, CategoryViewHolder> {
    @NonNull
    private final Listener mListener;

    public interface Listener {
        void onItemClicked(@NonNull CategoryItemModel categoryItemModel, @NonNull View view);

        void onItemFocusChange(@NonNull CategoryItemModel categoryItemModel, @NonNull View view, @NonNull boolean z);
    }

    public CategoryViewRenderer(int type, Context context, @NonNull Listener listener) {
        super(type, context);
        this.mListener = listener;
    }
    @Override
    public void bindView(@NonNull final CategoryItemModel model, @NonNull CategoryViewHolder holder) {
        holder.mViewAll.setFocusable(true);
        holder.mViewAll.setDuplicateParentStateEnabled(true);
        holder.mTitle.setText(model.getName());
        holder.mCount.setText(String.valueOf(model.getCount()));
        if (model.getBackgroundImageBitmap() != null) {
            holder.mImageView.setImageBitmap(model.getBackgroundImageBitmap());
        }
        holder.mViewAll.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                CategoryViewRenderer.this.mListener.onItemClicked(model, v);
            }
        });
        holder.mViewAll.setOnFocusChangeListener(new OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                CategoryViewRenderer.this.mListener.onItemFocusChange(model, view, b);
                if (b) {
                    CategoryViewRenderer.this.animIn(view);
                } else {
                    CategoryViewRenderer.this.animOut(view);
                }
            }
        });
    }

    @NonNull
    public CategoryViewHolder createViewHolder(@Nullable ViewGroup parent) {
        return new CategoryViewHolder(LayoutInflater.from(getContext()).inflate(R.layout.item_category, parent, false));
    }

    private void animOut(View view) {
        view.animate().scaleX(1.0f).scaleY(1.0f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
        view.findViewById(R.id.overlay).animate().alpha(1.0f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
    }

    private void animIn(View view) {
        view.animate().scaleX(1.2f).scaleY(1.2f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
        view.findViewById(R.id.overlay).animate().alpha(0.0f).setDuration(150).setInterpolator(new DecelerateInterpolator()).start();
    }
}
