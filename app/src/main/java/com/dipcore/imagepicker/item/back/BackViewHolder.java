package com.dipcore.imagepicker.item.back;

import android.support.v7.widget.RecyclerView.ViewHolder;
import android.view.View;

public class BackViewHolder extends ViewHolder {
    public final View mViewAll;

    public BackViewHolder(View itemView) {
        super(itemView);
        this.mViewAll = itemView;
    }
}
