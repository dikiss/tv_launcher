package com.dipcore.sidebar;

public interface OnDismissListener {
    void onDismiss(Sidebar sidebar);
}
