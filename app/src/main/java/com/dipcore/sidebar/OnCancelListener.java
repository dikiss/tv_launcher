package com.dipcore.sidebar;

public interface OnCancelListener {
    void onCancel(Sidebar sidebar);
}
