package com.dipcore.sidebar;

import android.app.Activity;
import android.content.Context;
import android.support.v4.view.PointerIconCompat;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.Animation.AnimationListener;
import android.view.animation.AnimationUtils;
import android.widget.FrameLayout;
import android.widget.FrameLayout.LayoutParams;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dipcore.atvlauncher.R;

import org.objectweb.asm.Opcodes;

public class
Sidebar {
    private static final int INVALID = -1;
    private static final String TAG = Sidebar.class.getCanonicalName();
    private Context context;
    private ViewGroup decorView;
    private boolean isCancelable = true;
    private boolean isDismissing = false;
    private OnCancelListener onCancelListener;
    private OnDismissListener onDismissListener;
    private OnKeyListener onKeyListener;
    private AnimationListener outAnimationListener = new AnimationListener() {
        @Override
        public void onAnimationStart(Animation animation) {
        }
        @Override
        public void onAnimationEnd(Animation animation) {
            Sidebar.this.decorView.post(new Runnable() {
                @Override
                public void run() {
                    Sidebar.this.windowManager.removeViewImmediate(Sidebar.this.rootView);
                    Sidebar.this.isDismissing = false;
                    if (Sidebar.this.onDismissListener != null) {
                        Sidebar.this.onDismissListener.onDismiss(Sidebar.this);
                    }
                }
            });
        }
        @Override
        public void onAnimationRepeat(Animation animation) {
        }
    };
    private final LayoutParams params = new LayoutParams(-2, -1, 5);
    private Sidebar parent;
    private ViewGroup rootView;
    private View sidebarBackdrop;
    private LinearLayout sidebarContainer;
    private FrameLayout sidebarContentContainer;
    private FrameLayout sidebarFooterContainer;
    private LinearLayout sidebarHeaderContainer;
    private TextView title;
    private WindowManager windowManager;
    private WindowManager.LayoutParams windowManagerParams;

    public Sidebar(Context context) {
        this.context = context;
        init();
    }

    public Sidebar(Context context, Sidebar parent) {
        this.context = context;
        this.parent = parent;
        init();
    }

    public void show() {
        if (!isShowing()) {
            this.windowManager.addView(this.rootView, this.windowManagerParams);
            this.sidebarContentContainer.focusSearch(View.FOCUS_DOWN);
            if (this.parent == null) {
                inAnimation();
            } else {
                inChildAnimation();
            }
        }
    }

    public void dismiss() {
        if (!this.isDismissing) {
            this.isDismissing = true;
            if (this.parent == null) {
                outAnimation().setAnimationListener(this.outAnimationListener);
            } else {
                outChildAnimation().setAnimationListener(this.outAnimationListener);
            }
        }
    }

    public void instantDismiss() {
        this.windowManager.removeView(this.rootView);
        this.isDismissing = false;
        if (this.onDismissListener != null) {
            this.onDismissListener.onDismiss(this);
        }
        if (this.parent != null) {
            this.parent.instantDismiss();
        }
    }

    public Sidebar setContent(int resId) {
        Utils.setView(this.context, this.sidebarContentContainer, resId);
        return this;
    }

    public Sidebar sidebarFooterContainer(int resId) {
        Utils.setView(this.context, this.sidebarFooterContainer, resId);
        return this;
    }

    public Sidebar setGravity(int gravity) {
        this.params.gravity = gravity;
        this.sidebarContainer.setLayoutParams(this.params);
        return this;
    }

    public Sidebar setWidth(int resId) {
        this.params.width = (int) this.context.getResources().getDimension(resId);
        this.sidebarContainer.setLayoutParams(this.params);
        return this;
    }

    public Sidebar setWidthDp(int width) {
        if (width > 0) {
            width = Utils.dp2px(this.context, (float) width);
        }
        this.params.width = width;
        this.sidebarContainer.setLayoutParams(this.params);
        return this;
    }

    public Sidebar setHeightDp(int height) {
        if (height > 0) {
            height = Utils.dp2px(this.context, (float) height);
        }
        this.params.height = height;
        this.sidebarContainer.setLayoutParams(this.params);
        return this;
    }

    public Sidebar setCancelable(boolean isCancelable) {
        this.isCancelable = isCancelable;
        return this;
    }

    public View findViewById(int resId) {
        return this.rootView.findViewById(resId);
    }

    public void setTitle(String title) {
        this.title.setText(title);
    }

    public void setTitle(int resId) {
        this.title.setText(this.context.getText(resId));
    }

    public void setTitleTextSize(int resId) {
        this.title.setTextSize(0, (float) ((int) getContext().getResources().getDimension(resId)));
    }

    public void setTitlePadding(int resId) {
        int size = (int) getContext().getResources().getDimension(resId);
        this.sidebarHeaderContainer.setPadding(size, size, size, size);
    }

    public boolean isShowing() {
        return this.decorView.findViewById(R.id.sidebar_base_container) != null;
    }

    public void onBackPressed() {
        if (this.onCancelListener != null) {
            this.onCancelListener.onCancel(this);
        }
        dismiss();
    }

    public Sidebar setOnKeyListener(OnKeyListener onKeyListener) {
        this.onKeyListener = onKeyListener;
        return this;
    }

    public Sidebar setOnCancelListener(OnCancelListener onCancelListener) {
        this.onCancelListener = onCancelListener;
        return this;
    }

    public Sidebar setOnDismissListener(OnDismissListener onDismissListener) {
        this.onDismissListener = onDismissListener;
        return this;
    }

    public Context getContext() {
        return this.context;
    }

    public ViewGroup getRootView() {
        return this.rootView;
    }

    public View getSidebarBackdrop() {
        return this.sidebarBackdrop;
    }

    public LinearLayout getSidebarContainer() {
        return this.sidebarContainer;
    }

    public LinearLayout getSidebarHeaderContainer() {
        return this.sidebarHeaderContainer;
    }

    public FrameLayout getSidebarContentContainer() {
        return this.sidebarContentContainer;
    }

    public FrameLayout getSidebarFooterContainer() {
        return this.sidebarFooterContainer;
    }

    private void init() {
        Activity activity = (Activity) this.context;
        LayoutInflater layoutInflater = LayoutInflater.from(this.context);
        this.windowManager = (WindowManager) this.context.getSystemService(Context.WINDOW_SERVICE);
        this.windowManagerParams = new WindowManager.LayoutParams(-1, -1, PointerIconCompat.TYPE_HELP, 16778240, -2);
        this.decorView = (ViewGroup) activity.getWindow().getDecorView().findViewById(android.R.id.content);
        this.rootView = new FrameLayout(this.context) {
            public boolean dispatchKeyEvent(KeyEvent keyEvent) {
                if (Sidebar.this.onKeyListener != null) {
                    Sidebar.this.onKeyListener.onKey(Sidebar.this, keyEvent);
                }
                if (!Sidebar.this.isCancelable || (keyEvent.getKeyCode() != 4 && keyEvent.getKeyCode() != Opcodes.DDIV)) {
                    return super.dispatchKeyEvent(keyEvent);
                }
                Sidebar.this.onBackPressed();
                return true;
            }
        };
        layoutInflater.inflate(R.layout.base_container, this.rootView);
        this.sidebarBackdrop = this.rootView.findViewById(R.id.sidebar_backdrop);
        this.sidebarContainer = this.rootView.findViewById(R.id.sidebar_container);
        this.sidebarHeaderContainer = this.rootView.findViewById(R.id.sidebar_header_container);
        this.sidebarFooterContainer = this.rootView.findViewById(R.id.sidebar_footer_container);
        this.title = this.rootView.findViewById(R.id.title);
        this.sidebarContentContainer = this.rootView.findViewById(R.id.sidebar_content_container);
        if (this.parent != null) {
            this.sidebarBackdrop.setVisibility(View.GONE);
            this.sidebarContainer.setBackgroundColor(0);
        }
        this.sidebarBackdrop.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View view) {
                Sidebar.this.dismiss();
                if (Sidebar.this.parent != null) {
                    Sidebar.this.parent.instantDismiss();
                }
            }
        });
    }

    private Animation inAnimation() {
        Animation sidebarContainerAnimation = AnimationUtils.loadAnimation(this.context, this.params.gravity == 3 ? R.anim.slide_in_left : R.anim.slide_in_right);
        Animation sidebarBackdropAnimation = AnimationUtils.loadAnimation(this.context, R.anim.alpha_in);
        this.sidebarContainer.startAnimation(sidebarContainerAnimation);
        this.sidebarBackdrop.startAnimation(sidebarBackdropAnimation);
        return sidebarContainerAnimation;
    }

    private Animation inChildAnimation() {
        Animation sidebarHeaderContainerAnimation = AnimationUtils.loadAnimation(this.context, R.anim.alpha_in);
        Animation sidebarFooterContainerAnimation = AnimationUtils.loadAnimation(this.context, R.anim.alpha_in);
        Animation sidebarContentContainerAnimation = AnimationUtils.loadAnimation(this.context, this.params.gravity == 3 ? R.anim.child_slide_in_left : R.anim.child_slide_in_right);
        Animation parentSidebarContentContainerAnimation = AnimationUtils.loadAnimation(this.context, this.params.gravity == 3 ? R.anim.parent_slide_out_left : R.anim.parent_slide_out_right);
        this.sidebarHeaderContainer.startAnimation(sidebarHeaderContainerAnimation);
        this.sidebarFooterContainer.startAnimation(sidebarFooterContainerAnimation);
        this.sidebarContentContainer.startAnimation(sidebarContentContainerAnimation);
        this.parent.getSidebarContentContainer().startAnimation(parentSidebarContentContainerAnimation);
        return sidebarContentContainerAnimation;
    }

    private Animation outAnimation() {
        Animation sidebarContainerAnimation = AnimationUtils.loadAnimation(this.context, this.params.gravity == 3 ? R.anim.slide_out_left : R.anim.slide_out_right);
        Animation sidebarBackdropAnimation = AnimationUtils.loadAnimation(this.context, R.anim.alpha_out);
        this.sidebarContainer.startAnimation(sidebarContainerAnimation);
        this.sidebarBackdrop.startAnimation(sidebarBackdropAnimation);
        return sidebarContainerAnimation;
    }

    private Animation outChildAnimation() {
        Animation sidebarHeaderContainerAnimation = AnimationUtils.loadAnimation(this.context, R.anim.alpha_out);
        Animation sidebarFooterContainerAnimation = AnimationUtils.loadAnimation(this.context, R.anim.alpha_out);
        Animation sidebarContentContainerAnimation = AnimationUtils.loadAnimation(this.context, this.params.gravity == 3 ? R.anim.child_slide_out_left : R.anim.child_slide_out_right);
        Animation parentSidebarContentContainerAnimation = AnimationUtils.loadAnimation(this.context, this.params.gravity == 3 ? R.anim.parent_slide_in_left : R.anim.parent_slide_in_right);
        this.sidebarHeaderContainer.startAnimation(sidebarHeaderContainerAnimation);
        this.sidebarFooterContainer.startAnimation(sidebarFooterContainerAnimation);
        this.sidebarContentContainer.startAnimation(sidebarContentContainerAnimation);
        this.parent.getSidebarContentContainer().startAnimation(parentSidebarContentContainerAnimation);
        return sidebarContentContainerAnimation;
    }
}
