package com.dipcore.sidebar;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

public class Utils {
    private static final int INVALID = -1;

    public static void setView(Context context, ViewGroup parent, View view) {
        if (view != null) {
            parent.removeAllViews();
            parent.addView(view);
        }
    }

    public static void setView(Context context, ViewGroup parent, int resourceId) {
        if (resourceId != -1) {
            LayoutInflater inflater = LayoutInflater.from(context);
            parent.removeAllViews();
            parent.addView(inflater.inflate(resourceId, parent, false));
        }
    }

    public static int dp2px(Context context, float dipValue) {
        return (int) ((dipValue * context.getResources().getDisplayMetrics().density) + 0.5f);
    }
}
