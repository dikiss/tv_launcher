package com.dipcore.sidebar;

import android.view.KeyEvent;

public interface OnKeyListener {
    void onKey(Sidebar sidebar, KeyEvent keyEvent);
}
